# Dynamite #2i2 (ダイナマイト#2i2)

A live broadcast in which the members of the idol group #2i2 do their best to challenge various projects to bring out the true face of #2i2! The program title "Dynamite #2i2" was devised by the members. The title "Dynamite #2i2" was invented by the members to mean that they wanted to make the show a "live" broadcast in which they would "run wild" just like the group's live concept. Through the program, the group will maximize the appeal of #2i2, which has yet to be seen!

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: NicoNico, YouTube
* Air On: Once a month
* Started: 2023-11-22
* Status: Continuing
* Genres: Variety, Music
* Links: [NicoNico](https://nicochannel.jp/2i2/), [YouTube](https://www.youtube.com/playlist?list=PL2-indbWO52jq6ho9ciBgTIkJY9F5qrtJ)