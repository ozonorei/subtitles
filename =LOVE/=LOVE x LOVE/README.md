# =LOVE x LOVE (＝ＬＯＶＥ×ＬＯＶE)

A music variety show featuring =LOVE and a new guest every week.

![Sakura Meets Cast](fanart.jpg)

# MC's

* Satoshi "Panther" Mukai. `(Comedian)`. 

# Program Information

* Broadcast on: NTV, TVer, Hulu
* Air On: Monday, ?? (JST)
* Started: 2024-03-18
* Status: Ended
* Genres: Music, Variety.
* Links: [NTV](https://www.ntv.co.jp/xLOVE/), [TVer](https://tver.jp/series/srhlsqgvfd), [Hulu](https://www.hulu.jp/equallove-x-love),

# Notes

## 2024-04-22

Episode number 6 will be the last episode of the show.