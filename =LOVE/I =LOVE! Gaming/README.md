# I =LOVE! Gaming (アイ＝ラブ！げーみんぐ)

A regular gaming show hosted by comedy duo Einstein and Sasaki Maika and Saito Kiara of =LOVE. Other =LOVE members appear as guests. They gather with gamers for a late Saturday night gaming session! eSports, live gaming, smartphone games...all kinds of games will be introduced! A "new type of game talk variety show" is born to deepen the love of games together with the viewers!

![Show Poster](fanart.jpg)

# MC

* Saito "=LOVE" Kiara.
* Sasaki "=LOVE" Maika.
* Inada "Einstein" Naoki (Comedian).
* Kawai "Einstein" Yuzuru (Comedian).
 
# Airing Status

* Channel: TV Asahi, TVer, Telasa
* Air On: Saturday, 03:40 AM (JST)
* Started: 2022-10-08
* Status: Continuing
* Genre: Variety, Gaming
* Links: [TV Asahi](https://www.tv-asahi.co.jp/ilovegaming/), [TVer](https://tver.jp/series/sri4e6xmg4), [Telasa](https://www.telasa.jp/series/12928)

