# Kimi wa =LOVE wo Aiseru ka!!! (キミは=LOVEを愛せるか!!!)

The variety program of "=LOVE" idol group produced by Sashihara Rino, in celebrations of their 3rd year anniversary. They will not be given a mission, they will not put their bodies on the line to challenge themselves, and the concept of pursuing "kawaii" just like an idol and the appearance of MC Sissonne will remain the same, and new corner projects produced by each member will also appear. And we will deliver more and more powerful content.

![Show Poster](fanart.jpg)

# MC

* Jiro "Sissonne". (Comedian).
 
# Program Information

* Broadcast on: Fuji TV, SkyPerfectv, TVer
* Air On: Last Saturday of each month, 22:00~ (JST)
* Started: 2020-09-13
* Status: Continuing
* Genres: Variety
* Links : [Fuji TV](https://otn.fujitv.co.jp/equal-love/), [SkyPerfectv](https://www.skyperfectv.co.jp/program/st/idol/equal-love/),[TVer](https://tver.jp/series/srcjf4gfns)

# Notes

## 2024-02-25

The previous seasons has bene merged into a single directory all the subs for the previous seasons and current season are available in the same place.