# MUSIC VERSE

Modern idols who are making waves in the real and virtual worlds will dig deep into the behind-the-scenes of popular creators! Today, a "modern idol" is a gathering of a wide range of creative talents, including voice, singing, talking, and cutting-edge technology. So we invite guests who are also active at the forefront of the creative field to learn the "secrets of creativity" from various angles!

![MUSIC VERSE Fanart](fanart.jpg)

# MC's
* Subaru Ozora. `(VTuber)`.
  
# Regular Guests

* Iori Noguchi. `(Idol =LOVE)`.
* Hitomi Takamatsu. `(Idol =LOVE)`.
* Emiri Otani. `(Idol =LOVE)`.
* Saito Kirara. `(Idol =LOVE)`.
* Ooba Hana. `(Idol =LOVE)`.

# Program Information

* Broadcast on: NTV, TVer
* Air On: Last Thursday of each month, ?? (JST)
* Started: 2023-04-28
* Status: Continuing
* Genres: Music, Variety.
* Links: [NTV](https://www.ntv.co.jp/musicverse/), [TVer](https://tver.jp/series/srmsysqcfh)



