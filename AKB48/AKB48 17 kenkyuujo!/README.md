# AKB48 17 Kenkyuujo! (AKB48 17研究所！)

This program is the first show for AKB48 17th gen members. They will "study" various things in order to become first-class idols.

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: NicoNico
* Air on : 13:00~ (JST), `No fixed day`.
* Started: 2022-07-10
* Status : Continuing
* Genres : Live Show
* Links  : [NicoNico](https://nicochannel.jp/akb48-17ken/)
