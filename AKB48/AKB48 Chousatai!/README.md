# AKB48 Chousatai! (ＡＫＢ４８調査隊！)

"AKB48 Chousatai!" is a variety show broadcast on Tokyo MX, where AKB48 members visit various restaurants and businesses that were affected by Covid-19 to show their support.

![AKB48 Chousatai! Poster](poster.jpg)

# Airing Status

* Channel: Tokyo MX
* Started: 2023-10-15
* Status: Continuing
* Genre : Variety
