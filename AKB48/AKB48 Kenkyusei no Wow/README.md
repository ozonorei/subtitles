# AKB48 Kenkyusei no Wow (AKB48研究生のWOW)
##### Also known as: AKB48 no WOW!!! ~THE WALK OF THE WORLD~

A training location show for the Kenkyuusei to develop their variety skills while challenging location shoots and food reviews! The name of the program "WOW" is an abbreviation of "THE WALK OF THE WORLD," and the title represents the Kenkyuusei's wish to "become idols that will fly out into the world in the future." A handful of members from the two generations will be chosen each episode to explore different locations. 

![Content Poster/Fanart](fanart.jpg)

# Program Information

* Broadcast on: TV-Kanagawa, TVer.
* Air On : Friday, 25:30~ (JST), 
* Started: 2024-07-05
* Status : Continuing
* Genres : Comedy, Variety, Travel
* Links  : [TVer](https://tver.jp/series/srnmo7r8q6)

# Notes

## 2024-07-05

The show is quite new, there is not lot of information about it. 