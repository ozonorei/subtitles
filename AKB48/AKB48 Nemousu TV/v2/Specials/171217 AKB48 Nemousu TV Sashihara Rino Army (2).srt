1
00:00:05,786 --> 00:00:08,786
Family Theater

2
00:00:14,786 --> 00:00:18,786
Please be careful! The staff of this program are
crazy!

3
00:00:20,786 --> 00:00:26,786
This time on Nemo TV, Nobore, Sashipi, Chirudanen,
Chuuhen

4
00:00:26,786 --> 00:00:35,786
This program is produced by Sashihara Nemo and
Yoyogi Animation Academy

5
00:00:36,048 --> 00:00:39,048
Akadake Mountain Climbing

6
00:00:40,048 --> 00:00:44,048
And carrying a flag on the top of the mountain

7
00:00:44,048 --> 00:00:53,306
But Fumi-yama mountain climbing is not easy

8
00:00:53,306 --> 00:00:57,306
Noboredomo, Noboredomo, the mountain path
continues.

9
00:00:57,306 --> 00:01:01,562
Where does this lead to?

10
00:01:01,562 --> 00:01:10,562
Saito Nagisa and Oto-shima, who started to be
physically behind, had to make a tough choice.

11
00:01:10,562 --> 00:01:15,562
The decision they made was...

12
00:01:15,562 --> 00:01:17,562
Being reckless equals to what?

13
00:01:17,562 --> 00:01:20,562
Being reckless equals to love?

14
00:01:20,562 --> 00:01:26,562
But, what will happen?

15
00:01:26,562 --> 00:01:31,562
Will they continue to climb or give up?

16
00:01:31,562 --> 00:01:34,562
It's tough.

17
00:01:34,562 --> 00:01:36,410
It's impossible.

18
00:01:36,410 --> 00:01:40,410
I was 12 years old when I climbed the mountain

19
00:01:40,410 --> 00:01:42,410
And then, Oto-shima

20
00:01:42,410 --> 00:01:50,410
I've been aiming to go to the top with everyone

21
00:01:50,410 --> 00:01:57,410
I can't give up here

22
00:01:59,410 --> 00:02:03,410
Oto-shima continues to climb with tears

23
00:02:03,410 --> 00:02:06,410
Where is the rabbit?

24
00:02:06,624 --> 00:02:27,624
I also want to go. I want to go to the top with
everyone, but my physical strength is already
pulling my legs, so I don't know what to do.

25
00:02:27,624 --> 00:02:37,082
A site where I want to aim for the top with
everyone and a site where I can be between anxiety
that I can't get rid of

26
00:02:37,082 --> 00:02:43,082
After about 20 minutes or 30 minutes, there is a
place below where it is a little frozen.

27
00:02:43,082 --> 00:02:50,082
I think it's okay to go there and judge again at
that point.

28
00:02:50,082 --> 00:02:52,082
Then, Otoshima-san too.

29
00:02:52,082 --> 00:02:55,082
First of all, is everyone okay with that?

30
00:02:55,082 --> 00:02:56,082
Yes.

31
00:02:56,082 --> 00:03:00,082
It's hard, but I want to climb.

32
00:03:00,082 --> 00:03:03,082
I can't do my best.

33
00:03:03,082 --> 00:03:04,082
It's okay.

34
00:03:04,082 --> 00:03:05,882
It's really hard.

35
00:03:05,882 --> 00:03:07,882
I'm so scared.

36
00:03:08,882 --> 00:03:11,882
But you have the best body.

37
00:03:11,882 --> 00:03:15,882
If you think you can't do it, you'll be relieved.

38
00:03:16,882 --> 00:03:18,882
Let's start again with the engine.

39
00:03:18,882 --> 00:03:19,882
Yes.

40
00:03:19,882 --> 00:03:21,882
Let's cheer up again.

41
00:03:21,882 --> 00:03:27,882
Let's aim for the top together.

42
00:03:27,882 --> 00:03:28,882
Yes.

43
00:03:28,882 --> 00:03:29,882
One, two.

44
00:03:29,882 --> 00:03:32,882
Let's go!

45
00:03:32,882 --> 00:03:35,882
Let's do our best!

46
00:03:35,882 --> 00:03:40,634
Let's do our best!

47
00:03:40,634 --> 00:03:43,634
Yes, that's the spirit.

48
00:03:43,634 --> 00:03:48,634
But the guide says this is where the real mountain
climbing begins.

49
00:03:50,634 --> 00:03:56,634
Up to the summit, there will be steep long stairs
and mountains.

50
00:03:57,634 --> 00:04:02,634
What do you think you are aiming for?

51
00:04:02,634 --> 00:04:10,032
To see the same scenery with everyone.

52
00:04:10,032 --> 00:04:17,032
I think it is to see the wonderful scenery.

53
00:04:18,032 --> 00:04:21,032
I like ice cream very much.

54
00:04:21,032 --> 00:04:24,032
I love ice cream very much.

55
00:04:24,032 --> 00:04:26,032
I understood the feeling of ice cream.

56
00:04:26,032 --> 00:04:27,032
Did you understand?

57
00:04:27,032 --> 00:04:28,032
I understood.

58
00:04:28,032 --> 00:04:30,000
Thank you very much.

59
00:04:30,000 --> 00:04:32,800
I'm really sorry for what I've done so far.

60
00:04:32,800 --> 00:04:36,400
I'm full now. I'm really sorry, ice cream.

61
00:04:36,400 --> 00:04:40,000
I don't rely on people as much as I can.

62
00:04:40,000 --> 00:04:46,700
I want to climb on my own and not make too much
trouble.

63
00:04:46,700 --> 00:04:52,500
If there is a child in trouble, I want to be able
to support him.

64
00:04:52,500 --> 00:04:54,600
It's just a little more to the top.

65
00:04:54,600 --> 00:04:59,280
It's just a little more.

66
00:04:59,280 --> 00:05:02,280
Why do you say it's just a little more?

67
00:05:02,280 --> 00:05:06,280
My nose is too cold and it hurts.

68
00:05:06,280 --> 00:05:11,280
I can't breathe from my nose, so I'm trying to
breathe from my mouth.

69
00:05:11,280 --> 00:05:13,280
It hurts.

70
00:05:14,280 --> 00:05:17,280
But if I do this, I can't breathe, so it's
painful.

71
00:05:17,280 --> 00:05:19,280
That's true.

72
00:05:19,280 --> 00:05:28,280
The freezing cold takes away the strength of the
members.

73
00:05:28,346 --> 00:05:33,346
The pace is starting to slow down again, and this
island is at its peak.

74
00:05:36,346 --> 00:05:40,346
I thought, this is what a real mountain climbing
is all about.

75
00:05:41,346 --> 00:05:43,346
What is mountain climbing?

76
00:05:45,346 --> 00:05:46,346
What is it?

77
00:05:49,346 --> 00:05:50,346
The meaning of doing it?

78
00:05:51,346 --> 00:05:52,346
Probably...

79
00:05:53,346 --> 00:05:55,346
I don't know if it's true, but...

80
00:05:55,346 --> 00:05:57,346
It's one of those...

81
00:05:58,080 --> 00:06:01,840
If you get over this,

82
00:06:01,840 --> 00:06:05,580
you'll find something beyond that, right?

83
00:06:05,580 --> 00:06:07,840
Isn't that what it is?

84
00:06:07,840 --> 00:06:10,880
I'm worried that I won't be able to hold on.

85
00:06:10,880 --> 00:06:12,180
Oh, no!

86
00:06:15,540 --> 00:06:16,680
Ouch!

87
00:06:17,840 --> 00:06:23,840
I want to climb over the 12 of us.

88
00:06:23,840 --> 00:06:25,584
I'm the only one left.

89
00:06:25,584 --> 00:06:33,584
I don't think I can do it, so I'll do my best,
too.

90
00:06:34,584 --> 00:06:39,584
30 minutes after leaving the hut, they arrive at
the final decision point.

91
00:06:40,584 --> 00:06:41,584
It's snowing a lot.

92
00:06:42,584 --> 00:06:46,584
This is where we went to the Gyoja River and
climbed up.

93
00:06:46,584 --> 00:06:49,584
The ground is white now, right?

94
00:06:49,584 --> 00:06:50,584
Yes.

95
00:06:50,584 --> 00:06:52,752
From here on, it's the forest limit.

96
00:06:52,752 --> 00:06:57,752
We're going to pass through the rocks and go to
the rocks where there is nothing.

97
00:06:57,752 --> 00:07:01,752
The members who understood the roughness of the
road ahead.

98
00:07:01,752 --> 00:07:04,752
Should we all move forward?

99
00:07:04,752 --> 00:07:06,752
Or...

100
00:07:09,752 --> 00:07:13,752
What are the two people's decisions about their
physical strength?

101
00:07:14,752 --> 00:07:17,752
It's okay. I'm glad I've climbed this far.

102
00:07:17,752 --> 00:07:19,752
Let's go.

103
00:07:19,752 --> 00:07:23,280
But...

104
00:07:23,280 --> 00:07:27,280
I don't think this is the only chance we have.

105
00:07:28,280 --> 00:07:31,280
ーShe's worried about them. ー

106
00:07:31,280 --> 00:07:36,720
ーI don't want to say sorry. ー

107
00:07:36,720 --> 00:07:39,720
ーI don't want to say sorry. ー

108
00:07:40,720 --> 00:07:43,720
ーI want to talk with my body. ー

109
00:07:44,720 --> 00:07:46,720
ーDon't climb. ー

110
00:07:47,720 --> 00:07:49,720
ーI don't mean that. ー

111
00:07:50,720 --> 00:07:52,720
ーI'm worried about my body. ー

112
00:07:53,720 --> 00:07:56,720
ーIf you miss one step, you may fall down. ー

113
00:07:57,720 --> 00:07:59,720
ーI want to go home alone. ー

114
00:08:01,296 --> 00:08:03,296
It's not like that, Natan.

115
00:08:03,296 --> 00:08:04,296
Natan?

116
00:08:04,296 --> 00:08:09,296
I'm worried about you, Natan.

117
00:08:10,296 --> 00:08:13,296
I want to go home alone.

118
00:08:18,296 --> 00:08:21,296
I'm not blaming you.

119
00:08:21,296 --> 00:08:24,296
I'm worried about you, Natan.

120
00:08:24,296 --> 00:08:30,296
But it's hard to be blamed by others.

121
00:08:31,296 --> 00:08:34,296
Why don't you understand me?

122
00:08:35,296 --> 00:08:39,296
Everyone is blaming you, Natan.

123
00:08:39,296 --> 00:08:42,768
Everyone is blaming Natan.

124
00:08:42,768 --> 00:08:46,768
Natan, if this is how you've always felt...

125
00:08:47,664 --> 00:08:50,664
I don't think you can climb it.

126
00:08:50,664 --> 00:08:52,664
Why are you blaming me?

127
00:08:52,664 --> 00:08:56,664
Because Natan doesn't believe in us.

128
00:08:56,664 --> 00:09:02,664
Natan doesn't believe in us, so he can't climb it.

129
00:09:02,664 --> 00:09:06,664
I want to believe you, but...

130
00:09:06,664 --> 00:09:11,664
If everyone says it together, we won't understand
it either.

131
00:09:11,664 --> 00:09:15,664
I'm talking to you because I'm very worried.

132
00:09:15,664 --> 00:09:22,664
But if everyone says it together, we won't
understand it either.

133
00:09:22,664 --> 00:09:25,664
But you want to climb it, don't you?

134
00:09:25,664 --> 00:09:26,664
I want to climb it.

135
00:09:26,664 --> 00:09:29,808
You want to climb it, don't you?

136
00:09:29,808 --> 00:09:35,808
Do you really trust us?

137
00:09:36,808 --> 00:09:38,808
I trust you.

138
00:09:41,808 --> 00:09:43,808
I love you all.

139
00:09:44,808 --> 00:09:46,808
How do you feel? Do you want to go?

140
00:09:46,808 --> 00:09:47,808
I want to go.

141
00:09:48,808 --> 00:09:51,808
From now on, it's really dangerous, so you'll have
to climb this.

142
00:09:51,808 --> 00:09:58,808
I'm not going to be busy, but I want to go now.

143
00:10:00,192 --> 00:10:14,192
I want to climb up with everyone and raise the
flag of Equal Love.

144
00:10:17,192 --> 00:10:19,192
What do you think, Nagisa?

145
00:10:19,192 --> 00:10:25,008
Do you want to go?

146
00:10:25,008 --> 00:10:27,008
Let's do our best.

147
00:10:27,008 --> 00:10:29,008
OK, let's go.

148
00:10:29,008 --> 00:10:35,008
But from now on, if I say I'm sorry, you'll
definitely get off.

149
00:10:35,008 --> 00:10:37,008
Let's hurry up and get ready.

150
00:10:37,008 --> 00:10:39,008
Let's do our best.

151
00:10:43,008 --> 00:10:47,008
Ten people who revealed everything and hit each
other with honest feelings.

152
00:10:47,008 --> 00:10:52,224
With this, their feelings became one.

153
00:10:52,224 --> 00:10:57,224
However, in the future, the real windmills are
waiting for them.

154
00:11:00,224 --> 00:11:05,224
Will they be able to climb to the top of the
mountain?

155
00:11:05,224 --> 00:11:19,224
This is the most difficult part.

156
00:11:19,226 --> 00:11:29,226
I don't know the weight of the flag. This is good.

157
00:11:29,226 --> 00:11:36,576
The preparation of the mountain climbing is
perfect.

158
00:11:36,576 --> 00:11:43,576
Today, I'm going to wear this Nemoos T-shirt that
I won by lottery and do some physical training and
soothe my feet.

159
00:11:45,576 --> 00:11:49,576
I'm going to walk with this 10kg of rice in my
backpack.

160
00:11:52,576 --> 00:11:54,576
The basics of the mountain are greetings.

161
00:11:55,576 --> 00:11:58,576
Greetings are hello from the person who climbs
anytime.

162
00:11:58,576 --> 00:12:03,576
I'm going to make beans for water supply.

163
00:12:05,520 --> 00:12:08,520
Climbing with abdominal breathing.

164
00:12:10,520 --> 00:12:13,520
Do not match the pace of the person who is fast.

165
00:12:15,520 --> 00:12:17,520
It is better to bring chocolate.

166
00:12:22,520 --> 00:12:25,520
I walked about 3.5 kilometers.

167
00:12:25,520 --> 00:12:28,560
I want to do my best.

168
00:12:28,560 --> 00:12:33,560
Let's do our best. Let's have fun.

169
00:12:33,560 --> 00:12:39,560
I don't have strength in my hands and feet, but I
have extra strength in my body.

170
00:12:39,560 --> 00:12:45,560
I'm going to use my useless physical strength.

171
00:12:45,560 --> 00:12:50,560
I'll do my best on the stairs.

172
00:12:50,560 --> 00:12:55,560
I'm going up the stairs.

173
00:13:03,674 --> 00:13:05,674
You can do it!

174
00:13:05,674 --> 00:13:09,674
It takes 30 minutes to continue climbing.

175
00:13:09,674 --> 00:13:13,674
Looking back,

176
00:13:13,674 --> 00:13:19,674
I was surrounded by such a beautiful place.

177
00:13:19,674 --> 00:13:23,674
I walked in such a place.

178
00:13:23,674 --> 00:13:30,624
I'll do my best!

179
00:13:30,624 --> 00:13:41,624
However, what awaits the members in front of them
is a steep and long staircase that is longer than
ever.

180
00:13:41,624 --> 00:13:59,624
However, if they do not overcome this staircase,
they will not reach the top they are aiming for.

181
00:14:01,082 --> 00:14:06,082
I don't want to see it, but I can't see it at all.

182
00:14:06,082 --> 00:14:09,082
I'm scared of high places.

183
00:14:09,082 --> 00:14:11,082
Oh, it's hard!

184
00:14:11,082 --> 00:14:13,082
My thighs!

185
00:14:13,082 --> 00:14:17,082
I have thighs!

186
00:14:17,082 --> 00:14:21,082
It takes about 20 minutes to cover your thighs.

187
00:14:21,082 --> 00:14:25,082
Wow, the light is amazing.

188
00:14:25,082 --> 00:14:28,082
We finally arrived at the open place.

189
00:14:28,082 --> 00:14:30,960
It's so hot!

190
00:14:30,960 --> 00:14:32,960
It hurts!

191
00:14:34,960 --> 00:14:36,960
Everyone is lost for words

192
00:14:38,960 --> 00:14:40,960
This is the warmest place

193
00:14:41,960 --> 00:14:43,960
It's fun

194
00:14:43,960 --> 00:14:44,960
Still?

195
00:14:44,960 --> 00:14:45,960
Not yet

196
00:14:45,960 --> 00:14:47,960
But I can breathe

197
00:14:47,960 --> 00:14:50,448
I didn't breathe

198
00:14:50,448 --> 00:14:53,448
I didn't breathe

199
00:14:53,448 --> 00:14:56,448
You are relaxed

200
00:14:56,448 --> 00:15:00,448
I'm a little tired now, but I'm healed by this
view

201
00:15:00,448 --> 00:15:03,448
My fingers are about to fall

202
00:15:03,448 --> 00:15:05,760
My fingers are frozen

203
00:15:05,760 --> 00:15:07,760
Thank you very much

204
00:15:07,760 --> 00:15:09,760
Kiyono seems to be in pain

205
00:15:09,760 --> 00:15:11,760
It's hard for me to watch

206
00:15:15,760 --> 00:15:17,760
I will do my best

207
00:15:19,760 --> 00:15:21,760
Don't you want to see the back?

208
00:15:21,760 --> 00:15:23,760
I don't see it now

209
00:15:23,760 --> 00:15:25,760
I only look at the floor

210
00:15:29,760 --> 00:15:31,760
There are too many stairs

211
00:15:31,760 --> 00:15:33,760
I know

212
00:15:37,760 --> 00:15:39,760
I didn't give up

213
00:15:39,760 --> 00:15:41,760
I thought I could see such a view

214
00:15:41,760 --> 00:15:43,760
I'm glad I did my best with everyone

215
00:15:43,760 --> 00:15:45,760
I'm not at my peak yet

216
00:15:47,760 --> 00:15:49,760
Saito decided to do her best with everyone

217
00:15:49,760 --> 00:15:51,744
I feel a great sense of achievement

218
00:15:51,744 --> 00:15:54,744
I'm glad

219
00:15:55,744 --> 00:15:58,744
Are you all right?

220
00:15:58,744 --> 00:16:02,744
This is the 8th stage

221
00:16:02,744 --> 00:16:06,744
We climbed about 80%

222
00:16:06,744 --> 00:16:11,744
That is the summit of Akadake

223
00:16:11,744 --> 00:16:13,744
It's so far

224
00:16:13,744 --> 00:16:17,744
We still have a long way to go

225
00:16:17,744 --> 00:16:18,912
See you!

226
00:16:18,912 --> 00:16:20,912
Let's go!

227
00:16:20,912 --> 00:16:22,912
Something came!

228
00:16:22,912 --> 00:16:24,912
Wait!

229
00:16:24,912 --> 00:16:26,912
Something came again!

230
00:16:26,912 --> 00:16:32,912
The title of the second single of the album is?

231
00:16:32,912 --> 00:16:35,808
Our uniform Christmas

232
00:16:35,808 --> 00:16:43,808
That's right. So, please do the Christmas
confession competition.

233
00:16:43,808 --> 00:16:46,808
There is nothing to do with the face

234
00:16:46,808 --> 00:16:52,808
I'll have you do it in this harsh situation

235
00:16:52,808 --> 00:16:55,808
Confession of love toward the camera

236
00:16:55,808 --> 00:16:58,808
Let's go

237
00:16:58,808 --> 00:17:02,400
Entry No. 1 Takiwaki Shoko

238
00:17:02,400 --> 00:17:07,400
It's Christmas today. Let's have fun together. I
love you.

239
00:17:07,400 --> 00:17:17,424
Entry No. 2 Hitomi Takamatsu

240
00:17:17,424 --> 00:17:23,424
Thank you for today. This year's Christmas present
is me. I love you.

241
00:17:29,424 --> 00:17:32,424
Entry No. 3 Oobahana

242
00:17:32,424 --> 00:17:43,680
You and I are connected by a red thread.

243
00:17:43,680 --> 00:17:47,680
Entry No. 4, Kiyara Saito

244
00:17:48,680 --> 00:17:52,680
I'm happy to spend Christmas with you.

245
00:17:52,680 --> 00:18:03,600
I thought it would suit you.

246
00:18:03,600 --> 00:18:08,600
Thank you. Can I spend the rest of my life with
you?

247
00:18:14,600 --> 00:18:18,600
Entry No.6 Mai Sasaki

248
00:18:19,600 --> 00:18:24,600
I asked Santa to be my girlfriend.

249
00:18:24,600 --> 00:18:29,600
I thought it would suit you.

250
00:18:32,208 --> 00:18:35,608
HENTORI NO.7 OTO SHIMA RISA

251
00:18:35,708 --> 00:18:39,608
Who will I spend Christmas with this year?

252
00:18:39,708 --> 00:18:44,608
I thought it would suit you.

253
00:18:45,208 --> 00:18:49,608
HENTORI NO.8 NOGUCHI IORI

254
00:18:49,608 --> 00:18:54,720
Even though I'm like this, please continue to love
me.

255
00:18:54,720 --> 00:18:59,720
Entry No.9 SANA MOROHASHI

256
00:19:00,720 --> 00:19:03,720
Entry No.9 SANA MOROHASHI

257
00:19:05,720 --> 00:19:11,720
When I climb this mountain, I have something to
say at the top, so wait until then. I won't say it
here.

258
00:19:11,720 --> 00:19:20,720
I thought it would suit you. Entry No.10 SATAKE
NONO

259
00:19:22,224 --> 00:19:28,224
I've always liked you. I want to spend Christmas
with you next year.

260
00:19:33,224 --> 00:19:37,224
Entry No.11 SAITO NAGISA

261
00:19:38,224 --> 00:19:41,224
What should I do? I love you.

262
00:19:41,224 --> 00:19:47,224
I thought it would suit you.

263
00:19:51,312 --> 00:19:55,312
If you climb to the top of the mountain, please
hold tight.

264
00:20:01,312 --> 00:20:06,312
I heard that there are other customers, but on the
way to such a mountain,

265
00:20:06,312 --> 00:20:08,312
such a Christmas,

266
00:20:08,312 --> 00:20:14,312
It's a difficult thing to say that it's a place of
love.

267
00:20:14,312 --> 00:20:16,312
It's a little dangerous.

268
00:20:16,312 --> 00:20:18,672
I was laughed at.

269
00:20:18,672 --> 00:20:20,672
What did you say?

270
00:20:20,672 --> 00:20:22,672
I don't want to say it.

271
00:20:22,672 --> 00:20:24,672
Confession?

272
00:20:24,672 --> 00:20:26,672
In this situation?

273
00:20:26,672 --> 00:20:28,672
It's terrible.

274
00:20:30,672 --> 00:20:32,672
Don't mess with me.

275
00:20:32,672 --> 00:20:34,672
I'm sorry.

276
00:20:34,672 --> 00:20:36,672
Viewers.

277
00:20:36,672 --> 00:20:40,672
Whose confession was the most shocking?

278
00:20:42,672 --> 00:20:46,672
After a hot confession, let's go back to the cold
mountain.

279
00:20:46,672 --> 00:20:47,664
Let's go.

280
00:20:47,664 --> 00:21:16,538
Oh, it's hard to walk on one foot of the mountain,
so it's easier to walk here little by little, even
if it's a little, so I'm climbing with the
Chocomaka strategy Chocomaka, Chocomaka,
Chocomaka, I'm doing it properly, but I think it's
better to be told that it's okay to be the oldest,
Chocomaka, Chocomaka,

281
00:21:16,538 --> 00:21:18,538
Oh, I missed one shot.

282
00:21:20,538 --> 00:21:22,538
I need to calm down.

283
00:21:28,538 --> 00:21:29,538
I'm going.

284
00:21:29,538 --> 00:21:30,538
Are you going?

285
00:21:30,538 --> 00:21:31,538
Yes.

286
00:21:31,538 --> 00:21:34,538
The hell of stairs is not over yet.

287
00:21:34,538 --> 00:21:46,538
I'm going.

288
00:21:46,586 --> 00:21:51,586
I have a lot of guts. It's okay. I can definitely
climb.

289
00:21:51,586 --> 00:21:54,586
I don't like the stairs.

290
00:21:54,586 --> 00:21:57,586
I don't like the stairs.

291
00:21:57,586 --> 00:22:00,586
It's the most tiring.

292
00:22:00,586 --> 00:22:03,586
I don't like it.

293
00:22:03,586 --> 00:22:11,568
No matter how much I hate it, the stairs won't
disappear.

294
00:22:11,568 --> 00:22:13,568
Just a little bit more!

295
00:22:13,568 --> 00:22:15,568
You can see Bunki now.

296
00:22:15,568 --> 00:22:19,568
If you go there, you can see the mountain on the
other side of the mountain.

297
00:22:21,568 --> 00:22:23,568
Just a little bit more!

298
00:22:23,568 --> 00:22:27,568
While the members are talking, the waterfall is
getting closer.

299
00:22:28,568 --> 00:22:30,568
Let's do our best!

300
00:22:30,568 --> 00:22:33,568
Just a little bit more to the top of the mountain.

301
00:22:33,568 --> 00:22:38,256
It's difficult.

302
00:22:38,256 --> 00:22:46,256
Kiyara Saito, who is the youngest, is already on
the back of the mountain.

303
00:22:50,256 --> 00:22:52,256
It's hard.

304
00:22:57,256 --> 00:22:59,256
I'll do my best.

305
00:23:01,256 --> 00:23:04,256
However, there are no steps that do not end.

306
00:23:04,256 --> 00:23:08,256
It is important to continue climbing.

307
00:23:10,298 --> 00:23:12,298
It's not the goal yet.

308
00:23:12,298 --> 00:23:14,298
It's not the goal yet.

309
00:23:14,298 --> 00:23:16,298
The wind is cold.

310
00:23:19,298 --> 00:23:21,298
I can see the signboard.

311
00:23:21,298 --> 00:23:23,298
Just a little more.

312
00:23:24,298 --> 00:23:26,298
Hang in there.

313
00:23:26,298 --> 00:23:29,184
This is the top.

314
00:23:29,184 --> 00:23:31,184
No, it's not.

315
00:23:38,784 --> 00:23:41,184
The top of the mountain is already in front of us.

316
00:23:41,184 --> 00:23:43,184
Now, it's the last spurt.

317
00:23:45,184 --> 00:23:47,184
What should we do?

318
00:23:47,184 --> 00:23:49,184
Next time, it's finally over.

319
00:23:49,184 --> 00:23:51,184
GORABU BANGAI HEN

320
00:23:51,184 --> 00:23:56,448
The biggest and last challenge waiting for you at
the top of the mountain.

321
00:23:56,448 --> 00:23:58,448
Juri's heart is one.

322
00:23:58,448 --> 00:24:00,448
Eat the mountain with all your might.

323
00:24:00,448 --> 00:24:02,448
I'm scared.

324
00:24:02,448 --> 00:24:04,448
I'm scared.

325
00:24:04,448 --> 00:24:09,448
Will Tashipi Children be able to climb all of
them?

326
00:24:09,448 --> 00:24:11,448
I'm sleepy.

327
00:24:11,448 --> 00:24:15,168
I'm sleepy.

328
00:24:15,168 --> 00:24:21,168
It's time for the present quiz. This week, a total
of four people from the correct answer.

329
00:24:21,168 --> 00:24:29,168
Saito Nagisa, Sasaki Maika, Sasaken Nonno, and
Takamatsu Hitomi will wear the autographed
headlights as a gift.

330
00:24:29,168 --> 00:24:35,168
Here's the quiz. Who is going to fall out with
Saito Nagisa?

331
00:24:35,168 --> 00:24:39,168
He was out of the Christmas confession contest.

332
00:24:39,168 --> 00:24:44,570
Answer the quiz and choose one of the four members
you like.

333
00:24:44,570 --> 00:24:46,570
Family Theater

334
00:24:46,570 --> 00:24:49,570
Why do they aim for the top?

335
00:24:49,570 --> 00:24:51,570
It's impossible

336
00:24:51,570 --> 00:24:52,570
It's tough

337
00:24:52,570 --> 00:24:53,570
It hurts

338
00:24:53,570 --> 00:24:54,570
Are you okay?

339
00:24:54,570 --> 00:24:58,570
What awaits them at the end of their suffering?

340
00:24:58,570 --> 00:24:59,570
It's really scary

341
00:24:59,570 --> 00:25:00,570
It's cold

342
00:25:00,570 --> 00:25:03,570
The Menosu TV series is finally over

343
00:25:07,570 --> 00:25:08,570
Thank you

344
00:25:08,570 --> 00:25:14,570
Sunday, December 24 at 6 p.m. All the answers are
on the screen

