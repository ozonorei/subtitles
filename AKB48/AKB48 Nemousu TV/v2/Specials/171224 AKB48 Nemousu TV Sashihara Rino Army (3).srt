1
00:00:05,786 --> 00:00:08,786
Family Theater

2
00:00:15,786 --> 00:00:19,786
This year's Christmas present is me. I love you

3
00:00:20,786 --> 00:00:24,786
Nemos TV's episode is over

4
00:00:24,786 --> 00:00:27,786
I should go home

5
00:00:27,786 --> 00:00:32,786
Love is aiming to capture Fuyama with all members

6
00:00:32,786 --> 00:00:37,786
If everyone says the same thing, I won't
understand

7
00:00:37,786 --> 00:00:39,962
Who are you?

8
00:00:39,962 --> 00:00:46,962
I wanted to carry the torch of the IKON-REM

9
00:00:47,962 --> 00:00:50,962
The bond was born by bumping into each other

10
00:00:50,962 --> 00:00:53,962
The feelings of the 8 people became one

11
00:00:56,962 --> 00:01:00,962
However, the members, Uiyama, raised their fangs

12
00:01:00,962 --> 00:01:02,962
Ah, it's hot!

13
00:01:02,962 --> 00:01:05,402
Oh my god!

14
00:01:05,402 --> 00:01:11,402
Will they be able to climb all the way to the top
of Sashipi-Chirion-Equal-Ranma?

15
00:01:14,402 --> 00:01:16,402
Muchaburi-Equal-Narita

16
00:01:17,402 --> 00:01:19,402
Muchaburi-Equal-Ranma

17
00:01:20,402 --> 00:01:22,402
Nemo-Stenri-Bangai-Hen

18
00:01:26,402 --> 00:01:28,402
The summit of the goal is right in front of them

19
00:01:28,402 --> 00:01:30,402
Now, it's the last spurt

20
00:01:30,402 --> 00:01:32,402
Will they be able to climb all the way to the top
of Sashipi-Chirion-Equal-Ranma?

21
00:01:32,402 --> 00:01:35,088
This is Ryo-Sen

22
00:01:35,088 --> 00:01:38,088
It will be on the line connecting the mountains. And this is the line that
connects the mountain to the mountain.

23
00:01:38,088 --> 00:01:41,088
So, the peak that stands out from here is already close. And the top of Sashipi-
Chirion-Equal-Ranma is close from here.

24
00:01:41,088 --> 00:01:44,088
From here, yes, about an hour or so. It's about an hour's drive
from here.

25
00:01:44,088 --> 00:01:48,088
This time, it will be a steep cliff-like chain. This time, it will be a steep
cliff-like slope.

26
00:01:49,088 --> 00:01:53,088
The biggest and final trial to stand up in front of ten people. This is the last attempt to
stand in front of ten people.

27
00:01:53,088 --> 00:01:57,434
As it is. That's it.

28
00:01:57,434 --> 00:02:01,434
It's called Kusariba, a steep rocky slope.

29
00:02:05,434 --> 00:02:07,434
We've made 12 people so far.

30
00:02:07,434 --> 00:02:09,434
We're almost there.

31
00:02:09,434 --> 00:02:15,674
Let's do our best!

32
00:02:15,674 --> 00:02:21,674
It takes 30 minutes to get to the entrance of the
chain yard.

33
00:02:24,674 --> 00:02:26,674
The temperature is getting lower.

34
00:02:26,674 --> 00:02:29,674
The cold wind blows mercilessly.

35
00:02:32,674 --> 00:02:34,674
The rocks are coming out of the ground.

36
00:02:34,674 --> 00:02:38,662
The ground is more unstable than ever before.

37
00:02:38,662 --> 00:02:40,662
Good luck!

38
00:02:43,662 --> 00:02:46,662
It's harder than climbing the stairs.

39
00:02:49,662 --> 00:02:52,662
My feet are heavy.

40
00:02:53,662 --> 00:02:55,662
I can't keep my balance.

41
00:02:58,662 --> 00:02:59,662
It's hard.

42
00:03:00,662 --> 00:03:01,662
I'll do my best.

43
00:03:02,662 --> 00:03:03,662
I have 2 seconds left.

44
00:03:04,662 --> 00:03:05,662
I'll do my best.

45
00:03:05,662 --> 00:03:06,662
Just a little more.

46
00:03:06,662 --> 00:03:08,662
Just a little more.

47
00:03:14,042 --> 00:03:17,042
It's dangerous. Just a little more.

48
00:03:18,042 --> 00:03:20,042
I'm glad it's not raining.

49
00:03:24,042 --> 00:03:25,042
What's wrong?

50
00:03:26,042 --> 00:03:32,042
My right thumb hurts when I put weight on it.

51
00:03:33,042 --> 00:03:34,042
It's hard now.

52
00:03:37,042 --> 00:03:38,042
It hurts.

53
00:03:38,042 --> 00:03:43,042
They've climbed up to 2700m.

54
00:03:43,680 --> 00:03:45,680
My legs hurt.

55
00:03:45,680 --> 00:03:47,680
They can't hide their fatigue.

56
00:03:47,680 --> 00:03:49,680
It hurts.

57
00:03:49,680 --> 00:03:51,680
I'll do my best.

58
00:03:51,680 --> 00:03:53,680
It's the last obstacle.

59
00:03:53,680 --> 00:03:55,680
Just a little more to the finish line.

60
00:03:57,680 --> 00:03:59,680
However, the youngest here,

61
00:03:59,680 --> 00:04:01,680
Kiyara Saito's foothold is in disarray.

62
00:04:05,680 --> 00:04:07,680
That's how it should be.

63
00:04:07,680 --> 00:04:11,232
Strength and endurance are the most important
things.

64
00:04:11,232 --> 00:04:15,232
The burden of climbing at the same pace as
Toshihide is big.

65
00:04:18,650 --> 00:04:19,650
It's 100%

66
00:04:19,650 --> 00:04:20,650
It's 100%?

67
00:04:20,650 --> 00:04:23,650
Saito was very energetic at the beginning

68
00:04:25,650 --> 00:04:29,650
But the burden gradually made her suffer

69
00:04:31,650 --> 00:04:32,650
She's very strange

70
00:04:32,650 --> 00:04:33,650
She's a strange girl

71
00:04:33,650 --> 00:04:36,650
We can't communicate with her

72
00:04:36,650 --> 00:04:39,650
I'm worried

73
00:04:39,650 --> 00:04:41,650
I'm worried

74
00:04:41,650 --> 00:04:47,354
She's worried about Sashihara

75
00:04:47,354 --> 00:04:49,354
Can you go a little more to the right?

76
00:04:49,354 --> 00:04:49,854
Yes

77
00:04:49,854 --> 00:04:50,354
I'm scared

78
00:04:50,354 --> 00:04:53,354
Such a fluttering imagination is the best

79
00:04:54,354 --> 00:04:56,354
Just a little more

80
00:04:56,354 --> 00:04:57,354
Kiara

81
00:04:57,354 --> 00:04:59,354
Let's do our best

82
00:05:01,354 --> 00:05:04,354
Maika is excited

83
00:05:04,354 --> 00:05:06,354
She is like Sayaka Akimoto

84
00:05:06,354 --> 00:05:09,354
I think she will pull everyone

85
00:05:09,354 --> 00:05:10,354
Let's go

86
00:05:10,354 --> 00:05:14,234
Put your feet firmly on the ground

87
00:05:14,234 --> 00:05:19,234
Even though she said that she would be at the peak
if she got tired, Sasaki was great.

88
00:05:22,234 --> 00:05:23,234
Then...

89
00:05:24,234 --> 00:05:26,234
I don't have it.

90
00:05:26,234 --> 00:05:30,746
I don't have it.

91
00:05:30,746 --> 00:05:32,746
Can you hold it?

92
00:05:32,746 --> 00:05:34,746
I'll do my best

93
00:05:34,746 --> 00:05:36,746
I'll do my best

94
00:05:40,746 --> 00:05:42,746
I feel a little tired

95
00:05:42,746 --> 00:05:46,746
It seems that Sasaki's power has been injected

96
00:05:46,746 --> 00:05:48,746
But don't overdo it

97
00:05:52,746 --> 00:05:54,746
Yes, that's fine

98
00:05:54,746 --> 00:05:57,242
Thank you

99
00:05:57,242 --> 00:06:02,242
She was only a few years old, but she did her
best.

100
00:06:05,242 --> 00:06:08,242
But her movements stopped.

101
00:06:08,242 --> 00:06:10,242
It hurts when I step on it.

102
00:06:10,242 --> 00:06:13,242
It's better to put weight on it.

103
00:06:13,242 --> 00:06:15,242
Are you okay?

104
00:06:18,074 --> 00:06:23,074
Well, Iori is absolutely fine. She's everyone's
big sister.

105
00:06:23,074 --> 00:06:29,074
Even when I check the making video, she always
shows her support.

106
00:06:29,074 --> 00:06:33,074
I think she'll be a kind big sister to everyone.

107
00:06:33,074 --> 00:06:48,074
Well, I have to take a lot of protein when I get
home.

108
00:06:50,618 --> 00:06:56,618
At last, the biggest and last step has arrived at
the bottom of the mountain.

109
00:06:57,618 --> 00:07:00,618
It's right in front of the mountain.

110
00:07:02,618 --> 00:07:04,618
Wow, this is climbing.

111
00:07:04,618 --> 00:07:06,618
I've seen this before.

112
00:07:06,618 --> 00:07:09,618
This is the one that my sister climbs.

113
00:07:09,618 --> 00:07:11,618
My sister is amazing.

114
00:07:15,618 --> 00:07:17,618
Here is the water.

115
00:07:17,618 --> 00:07:19,618
Thank you.

116
00:07:19,898 --> 00:07:29,898
Anna is the type of person who thinks about the
group first, and I think she can get leadership,
so I think she'll be fine.

117
00:07:37,898 --> 00:07:39,898
What are you thinking about now?

118
00:07:40,898 --> 00:07:43,898
I believe in her. I'm telling her to do her best.

119
00:07:43,898 --> 00:07:47,930
Do your best.

120
00:07:47,930 --> 00:08:02,930
I think I'm a thick type, but when I was
auditioning, I was shaking so much that I couldn't
believe it, and I was singing Nakabuchi while
shaking.

121
00:08:08,378 --> 00:08:11,378
Wait! Wait! It's about to fall!

122
00:08:11,378 --> 00:08:13,378
I think I'm going to fall before I can take a
picture.

123
00:08:23,546 --> 00:08:25,546
I climbed up.

124
00:08:25,546 --> 00:08:27,546
Mirina, hold this.

125
00:08:27,546 --> 00:08:29,546
OK. Thank you.

126
00:08:29,546 --> 00:08:35,834
I did it.

127
00:08:35,834 --> 00:08:37,834
Sana seems to use this rock.

128
00:08:38,834 --> 00:08:40,834
I think I'll be fine.

129
00:08:40,834 --> 00:08:43,834
I'm an adult, so I don't think I have much
stamina.

130
00:08:43,834 --> 00:08:48,834
I can't do it, but I think I have the most guts.

131
00:08:48,834 --> 00:08:50,834
I did it!

132
00:08:52,834 --> 00:08:55,834
Where? Here?

133
00:08:55,834 --> 00:08:57,834
I'm going to fall.

134
00:09:03,834 --> 00:09:05,834
I'm going to fall.

135
00:09:05,834 --> 00:09:10,298
I'm going to fall.

136
00:09:10,298 --> 00:09:13,298
What's in Sazaki's heart is...

137
00:09:13,298 --> 00:09:15,298
You can do it! You can do it!

138
00:09:15,298 --> 00:09:17,298
Good luck, Risa!

139
00:09:17,298 --> 00:09:19,298
Good luck!

140
00:09:19,298 --> 00:09:20,474
As expected of Risa!

141
00:09:20,474 --> 00:09:26,474
I think the good thing about Risa is that she is
the kind of girl that she thinks she is.

142
00:09:26,474 --> 00:09:34,918
The youngest, Kiyaramo Saito challenges.

143
00:09:34,918 --> 00:09:51,358
I'm looking forward to seeing Kiyaramo Saito
Kiyраo.

144
00:09:51,538 --> 00:10:00,578
It's been five years June of the biggest

145
00:10:00,578 --> 00:10:02,578
I see.

146
00:10:17,690 --> 00:10:19,690
They're cheering for us!

147
00:10:19,690 --> 00:10:24,506
Do your best!

148
00:10:24,506 --> 00:10:31,506
After climbing the first mountain, she goes down
the slope with a big rock in her right hand

149
00:10:31,506 --> 00:10:34,506
I don't know where my feet are

150
00:10:34,506 --> 00:10:37,506
Look at my feet

151
00:10:37,506 --> 00:10:44,570
It's dangerous here

152
00:10:44,570 --> 00:10:46,570
I'm scared.

153
00:10:55,514 --> 00:10:57,514
Where am I?

154
00:11:02,714 --> 00:11:04,514
It's hard to walk.

155
00:11:05,714 --> 00:11:10,714
Nagi-sama is watching over her.

156
00:11:15,714 --> 00:11:17,714
Nata!

157
00:11:17,714 --> 00:11:19,714
Wait for me!

158
00:11:19,714 --> 00:11:20,714
I'll do my best!

159
00:11:24,714 --> 00:11:26,714
Don't fall.

160
00:11:26,714 --> 00:11:30,714
Thank you.

161
00:11:30,938 --> 00:11:34,938
They help each other and go to the top step by
step

162
00:11:35,938 --> 00:11:36,938
Dangerous

163
00:11:37,938 --> 00:11:38,938
Are you OK?

164
00:11:38,938 --> 00:11:39,938
I'm OK

165
00:11:40,938 --> 00:11:43,938
The voice of friends is the best power

166
00:11:45,938 --> 00:11:49,938
When Kiyara is in front of me, I have to do my
best

167
00:11:50,938 --> 00:11:51,938
I think

168
00:11:51,938 --> 00:11:56,448
I'll do my best too

169
00:11:56,448 --> 00:11:58,448
Well, everyone!

170
00:11:58,448 --> 00:11:58,948
Yes!

171
00:11:58,948 --> 00:12:01,448
It's really there!

172
00:12:01,448 --> 00:12:01,948
Yes!

173
00:12:01,948 --> 00:12:04,448
A little bit more, 12 people have come so far!

174
00:12:04,448 --> 00:12:05,448
Yes!

175
00:12:05,448 --> 00:12:08,448
Well, let's do our best at the end!

176
00:12:08,448 --> 00:12:09,448
Yes!

177
00:12:09,448 --> 00:12:10,448
Let's go!

178
00:12:10,448 --> 00:12:11,448
Let's do our best!

179
00:12:11,448 --> 00:12:13,448
Yeah!

180
00:12:13,448 --> 00:12:15,448
Let's do our best!

181
00:12:15,448 --> 00:12:16,730
Let's do our best!

182
00:12:16,730 --> 00:12:19,730
I'm going to climb it with a smile at the end.

183
00:12:20,730 --> 00:12:21,730
I'll do my best.

184
00:12:21,730 --> 00:12:30,938
What is this?

185
00:12:30,938 --> 00:12:35,938
Akadake Climbing at Recall Love. It's finally the
climax.

186
00:12:38,938 --> 00:12:42,938
The wind is getting colder.

187
00:12:44,938 --> 00:12:46,938
What is this circle?

188
00:12:47,938 --> 00:12:50,938
Don't let your guard down.

189
00:12:50,938 --> 00:12:51,938
There are many circles.

190
00:12:51,938 --> 00:12:52,938
Why are there circles?

191
00:12:52,938 --> 00:12:55,938
This is the passageway.

192
00:12:55,938 --> 00:12:58,938
Please come this way.

193
00:12:58,938 --> 00:13:00,938
I see.

194
00:13:02,938 --> 00:13:03,938
I'm scared.

195
00:13:06,938 --> 00:13:07,938
What is this?

196
00:13:07,938 --> 00:13:09,938
I'm scared.

197
00:13:11,938 --> 00:13:12,938
I'm scared.

198
00:13:13,938 --> 00:13:14,938
Wait a minute.

199
00:13:14,938 --> 00:13:15,938
I'm scared.

200
00:13:22,106 --> 00:13:24,106
Natasha, are you okay?

201
00:13:24,106 --> 00:13:43,514
Let's take a break for a while.

202
00:13:43,514 --> 00:13:45,514
You can do it!

203
00:13:45,514 --> 00:13:47,514
You can do it!

204
00:13:47,514 --> 00:13:49,514
You can do it!

205
00:13:49,514 --> 00:13:52,826
You can do it!

206
00:13:52,826 --> 00:13:55,826
Now, the summit is right in front of her.

207
00:13:58,826 --> 00:14:01,826
I've always wanted to climb a mountain.

208
00:14:01,826 --> 00:14:06,826
So I thought I could do it on a TV show.

209
00:14:06,826 --> 00:14:11,834
I'm really looking forward to it.

210
00:14:11,834 --> 00:14:17,834
I want to climb with my own power, without
borrowing anyone's help.

211
00:14:17,834 --> 00:14:21,914
I'll do my best.

212
00:14:21,914 --> 00:14:26,914
I would like to climb with everyone and make a
good report to Mr. Sashihara.

213
00:14:29,914 --> 00:14:30,914
I'm scared.

214
00:14:30,914 --> 00:14:32,378
There are 12 people.

215
00:14:32,378 --> 00:14:38,378
I want to climb up, put up the flag, and see the
same scenery as everyone else.

216
00:14:38,378 --> 00:14:40,378
This is a little scary.

217
00:14:40,378 --> 00:14:44,378
I never thought we'd be able to perform on stage.

218
00:14:44,378 --> 00:14:50,378
I'm very happy, but I'm also nervous about what's
going to happen.

219
00:14:50,378 --> 00:14:57,242
I want to climb up, put up the flag, and see the
same scenery as everyone else.

220
00:14:57,242 --> 00:15:03,242
We all want to see the good things that Sashihara-
san said at the end.

221
00:15:05,242 --> 00:15:07,242
I don't want to give up.

222
00:15:08,242 --> 00:15:12,242
My goal is to aim for 12 people.

223
00:15:13,242 --> 00:15:15,242
That's all in my head.

224
00:15:16,242 --> 00:15:17,242
I'm trying my best.

225
00:15:18,242 --> 00:15:19,242
Just a little more.

226
00:15:20,242 --> 00:15:21,242
Really, just a little more.

227
00:15:21,242 --> 00:15:25,242
Even if it's hard, don't give up.

228
00:15:25,242 --> 00:15:31,610
Don't give up until you reach the top.

229
00:15:31,610 --> 00:15:33,610
I want to experience something new.

230
00:15:36,610 --> 00:15:43,610
It's still a bit early for the formation, so I
hope we can deepen our bond here.

231
00:15:47,610 --> 00:15:56,610
The goal of this mountain climbing is to aim for
the top and descend together.

232
00:15:56,610 --> 00:16:00,610
I want to find what I'm missing this time.

233
00:16:01,440 --> 00:16:03,440
Yes, I'll do my best.

234
00:16:08,954 --> 00:16:11,954
Even if I lose consciousness, I can climb.

235
00:16:11,954 --> 00:16:18,170
Even if I lose consciousness, I can't climb.

236
00:16:18,170 --> 00:16:21,170
There is only one thought in the twelve of them.

237
00:16:21,170 --> 00:16:26,182
The moment has finally arrived.

238
00:16:26,182 --> 00:16:28,182
The moment has finally arrived.

239
00:16:28,182 --> 00:16:30,182
Hey, Akadake has arrived!

240
00:16:30,182 --> 00:16:32,182
We made it!

241
00:16:36,182 --> 00:16:38,182
We made it!

242
00:16:38,182 --> 00:16:40,182
I'm so happy.

243
00:16:40,182 --> 00:16:42,182
We finally made it.

244
00:16:42,182 --> 00:16:44,182
We made it!

245
00:16:44,182 --> 00:16:46,182
We made it!

246
00:16:46,182 --> 00:16:48,182
I'm so happy.

247
00:16:48,182 --> 00:16:50,182
I'm so happy.

248
00:16:50,182 --> 00:16:52,182
We made it!

249
00:16:52,182 --> 00:16:54,182
We made it!

250
00:16:54,182 --> 00:16:56,182
Good job!

251
00:16:56,182 --> 00:16:58,182
You made it!

252
00:16:58,182 --> 00:17:00,182
You made it!

253
00:17:00,182 --> 00:17:02,182
We made it with 12 people!

254
00:17:02,182 --> 00:17:04,182
We made it with 12 people!

255
00:17:04,182 --> 00:17:06,182
Akadake has arrived!

256
00:17:06,182 --> 00:17:08,182
Akadake has arrived!

257
00:17:08,182 --> 00:17:10,182
Yay!

258
00:17:10,182 --> 00:17:12,182
Yay!

259
00:17:12,182 --> 00:17:14,182
Good job, everyone!

260
00:17:14,182 --> 00:17:16,182
Good job!

261
00:17:19,610 --> 00:17:21,610
We did it!

262
00:17:21,610 --> 00:17:22,610
It was good!

263
00:17:22,610 --> 00:17:26,610
12 members of RICOAD LOVE have arrived in Akazuki!

264
00:17:28,610 --> 00:17:30,610
We did it!

265
00:17:30,610 --> 00:17:31,610
We did it!

266
00:17:31,610 --> 00:17:34,778
We did it!

267
00:17:34,778 --> 00:17:39,778
I'm really happy to be able to climb with 12
people.

268
00:17:39,778 --> 00:17:41,778
Then, please pray for us.

269
00:17:41,778 --> 00:17:45,778
Check out single, our uniform Christmas.

270
00:17:45,778 --> 00:17:48,778
I hope it will be a hit!

271
00:17:48,778 --> 00:17:52,730
Please pray for us!

272
00:17:52,730 --> 00:17:54,730
What is it?

273
00:17:54,730 --> 00:17:56,730
What is it?

274
00:17:56,730 --> 00:17:58,730
You know who it is, right?

275
00:17:58,730 --> 00:18:00,730
Oh, no!

276
00:18:00,730 --> 00:18:03,578
Everyone, thank you for your hard work.

277
00:18:03,578 --> 00:18:05,578
Wonderful, wonderful!

278
00:18:05,578 --> 00:18:08,578
I think it was hard, but you climbed all the way
up, didn't you?

279
00:18:08,578 --> 00:18:10,578
You really did a great job!

280
00:18:11,578 --> 00:18:15,578
Now, Miri-nyan, how do you feel standing on the
top?

281
00:18:15,578 --> 00:18:19,578
I'm really glad everyone did a great job!

282
00:18:20,578 --> 00:18:23,578
Nagisa, what is the view from the top like?

283
00:18:23,578 --> 00:18:26,578
It's very beautiful!

284
00:18:26,578 --> 00:18:29,578
I'm glad I was able to climb with everyone!

285
00:18:29,578 --> 00:18:32,578
Sana, has everyone's unity increased?

286
00:18:32,578 --> 00:18:33,312
That's right!

287
00:18:33,312 --> 00:18:37,312
It's 100%! Thank you very much!

288
00:18:37,312 --> 00:18:41,312
The flag was given when something important was
given.

289
00:18:41,312 --> 00:18:47,312
The answer is the one that strengthens the bonds
of the 12 people who go up.

290
00:18:49,312 --> 00:18:51,312
Do you remember, Hitomi?

291
00:18:51,312 --> 00:18:55,312
Do you remember that if you reach the summit, you
will know the reason to climb the mountain?

292
00:18:55,312 --> 00:18:57,312
I remember! Of course I remember!

293
00:18:57,312 --> 00:18:59,312
I wonder what it is.

294
00:18:59,312 --> 00:19:01,008
What is it?

295
00:19:01,008 --> 00:19:05,008
From there, I wanted you to see something that
everyone has.

296
00:19:05,008 --> 00:19:10,008
Everyone, I think you can see a mountain in the
direction of the southeast.

297
00:19:10,008 --> 00:19:11,008
Can you see it?

298
00:19:11,008 --> 00:19:16,730
It's so beautiful!

299
00:19:16,730 --> 00:19:19,730
What is the name of that mountain?

300
00:19:19,730 --> 00:19:21,730
Mt.Fuji!

301
00:19:21,730 --> 00:19:26,730
That's right! It's Mt.Fuji! That's the highest
mountain in Japan.

302
00:19:26,730 --> 00:19:31,730
I wanted everyone to see Mt.Fuji from there.

303
00:19:31,730 --> 00:19:38,730
The reason why I wanted to show Mt.Fuji to
everyone is because I want to aim for the highest
mountain in Japan with everyone.

304
00:19:38,730 --> 00:19:46,730
I think it's too big a dream to say that it's the
highest mountain in Japan, but I'm seriously
aiming for the top with everyone.

305
00:19:46,992 --> 00:19:57,672
I think you can understand it from this climb, but
when you hear a big mountain, you may give up a
little, but the important thing is to believe in
yourself and take the first step.

306
00:19:57,672 --> 00:20:09,912
That one step is, no matter how big the goal is,
there is always that first step, so I thought that
I would like you to do your best without giving up
on it, and I wanted you to climb the mountain.

307
00:20:09,912 --> 00:20:15,866
I'll do my best, so let's all do our best so that
we can be the best in Japan someday.

308
00:20:15,866 --> 00:20:20,866
Well then, please go to Mt. Fuji and scream your
dreams and come back!

309
00:20:27,578 --> 00:20:33,578
From Nemos to Sashihara, the night has begun as a
journey to Sashihara.

310
00:20:33,578 --> 00:20:36,578
A heart that does not give up on unity.

311
00:20:36,578 --> 00:20:40,578
And the destination of the dream that everyone
aims for.

312
00:20:42,578 --> 00:20:45,578
Through the harsh mountain climbing,

313
00:20:45,578 --> 00:20:50,578
I want to convey the feelings that Sashihara wants
to convey.

314
00:20:50,578 --> 00:20:56,578
And finally, the dream of everyone towards the
best mountain in Japan, Nouchi.

315
00:20:56,688 --> 00:20:58,688
Please represent us.

316
00:20:59,688 --> 00:21:07,688
I hope that all 12 of us will shine at the top of
the idol without losing to anyone!

317
00:21:07,688 --> 00:21:14,906
I hope that I can shine as a voice actor too!

318
00:21:14,906 --> 00:21:20,906
Oh, that reminds me. I had another VTR.

319
00:21:20,906 --> 00:21:26,042
Oh, that reminds me. I had another VTR.

320
00:21:26,042 --> 00:21:32,042
I forgot to say this, but going down the mountain
is really hard, so do your best! Bye-bye!

321
00:21:33,042 --> 00:21:35,042
That's all?

322
00:21:36,042 --> 00:21:37,042
That's all?

323
00:21:38,042 --> 00:21:40,042
Sashihara-san!

324
00:21:40,042 --> 00:21:42,042
Sashihara-san!

325
00:21:42,042 --> 00:21:44,042
Let's do our best!

326
00:21:45,042 --> 00:21:46,042
With 12 people!

327
00:21:46,042 --> 00:21:47,042
Let's do our best!

328
00:21:47,042 --> 00:21:51,408
The girl in the back supports me a lot.

329
00:21:51,408 --> 00:21:57,408
Thanks to her support, I was able to climb to the
top, so I think I gained a lot of unity.

330
00:21:58,408 --> 00:22:08,408
I didn't give up, and I think I was able to grow a
lot physically and mentally.

331
00:22:09,408 --> 00:22:18,408
Even if I have a hard time, I think I can overcome
anything if there are 12 of us.

332
00:22:18,408 --> 00:22:21,408
Because there are children under my age.

333
00:22:21,600 --> 00:22:25,600
I have to push them back.

334
00:22:25,600 --> 00:22:28,600
I think I did my best.

335
00:22:28,600 --> 00:22:33,600
I think that's different from before.

336
00:22:33,600 --> 00:22:37,600
I thought it was impossible.

337
00:22:37,600 --> 00:22:41,600
But if I go one step at a time,

338
00:22:41,600 --> 00:22:46,600
I think I will reach my goal.

339
00:22:46,600 --> 00:22:51,456
I had a vague dream of doing it at the Tokyo Dome.

340
00:22:51,456 --> 00:22:56,456
But I wondered if such a dream could come true. I think my dream will come
true.

341
00:22:56,456 --> 00:23:03,984
When I think about climbing together, I once again feel the importance of that member. When I think
of climbing together, I feel the importance of the
members again.

342
00:23:03,984 --> 00:23:11,984
From now on, no matter how hard or painful things may be,
 I want to do my best without giving up until the end. Even if there is something painful or
painful in the future, I want to do my best
without giving up until the end.

343
00:23:11,984 --> 00:23:16,984
I was looking forward to it from the beginning, but I'm glad I really enjoyed it. I was looking
forward to it from the beginning, but I'm really
glad I was able to enjoy it.

344
00:23:16,984 --> 00:23:23,984
Once again, I was born to want to be more active in this Ikolab. I've come to
realize that I want to be more active with this
Ikolab once again.

345
00:23:23,984 --> 00:23:32,544
I'm the
type of person who prioritizes myself, but I climb
this mountain and everyone…

346
00:23:32,544 --> 00:23:41,544
Looking at everyone, I really realized that we have to support each other as we go. I
realized that I had to go with them to support
them.

347
00:23:41,544 --> 00:23:51,544
I really wanted to become Ikurabu 12 and aim to be the number one idol in Japan with everyone who is involved with Ikurabu. I really wanted to aim for the best
idol in Japan with everyone involved in the IKOLAB
12.

348
00:23:51,544 --> 00:23:59,040
I was glad that I climbed the mountain, and it was tough, but somehow, I think I was able to hold on to our solidarity. I'm
glad I climbed it. It was tough, but I think I was
able to strengthen my unity.

349
00:23:59,040 --> 00:24:07,040
I'm grateful because I had nothing but good things to say. After all, I thought that we shouldn't set our own limits. 
It was a good thing, so I'm grateful.

350
00:24:08,040 --> 00:24:14,040
Goodbye until next season. Everyone, don't catch a cold. Everyone, don't catch
a cold until the next season.

351
00:24:16,040 --> 00:24:21,040
It's time for a present quiz. This week, a total of 4 people will be selected from among the correct answers. It's time for the
gift quiz. This week, 4 people from the correct
answer sheet.

352
00:24:21,040 --> 00:24:28,656
We will be giving away signature headlights worn by Shogo Takigaki, Yurin Noguchi, Sana Morihashi, and Anna Yamamoto. Shoko
Takiwaki, Nobuchi Iwari, Sana Morohashi, and Anna
Yamamoto's signed headlights.

353
00:24:28,656 --> 00:24:29,656
Here's the quiz.

354
00:24:30,656 --> 00:24:33,656
Who shouted out everyone's dreams towards Mt.
Fuji?

355
00:24:33,656 --> 00:24:48,000
The answer is...the guy in the short cut.

