# AKB48, Saikin Kiita yo ne... (ＡＫＢ４８、最近聞いたよね…)

The latest installment of the `AKB48, Saikin Kiita?` series. And One of the last active AKB48 variety show. The show is hosted by Hiroyuki and features AKB48 members as guests. 

What is the appeal of AKB48? One of them is the unique music of AKB48! That's why AKB48 has teamed up with a variety of people, including general companies, local governments, and students, to create new content together through AKB48's music!. The show aims to have AKB48 and its members collaborate with local businesses and governments to promote the group in smaller regions.

![Show Poster](fanart.jpg)

# MC

* Hiroyuki
  
# Program Information

* Broadcast on: TV Tokyo, TVer
* Air On : Tuesday, 25:30~ (01:30 AM) (JST)
* Started: 2022-10-04
* Status :  Ended
* Genre  : Variety, Documentary, Music, Travel.
* Links  : [TV Tokyo](https://www.tv-tokyo.co.jp/akb48saikinkiitayone/), [TVer](https://tver.jp/series/srbp1t6umv)

# Notes

## 2024-04-03

A New version of the show was released under the name of `AKB48, Saikin Kiita！～ issho ni KYOUSOU shimasen ka ？～`.

## 2024-02-15

Starting with episode episode `70`, we will use `TVer` as the source for the raws and AISubs.

## 2024-01-23

The regular uploader of the show is no longer active. The show is still airing, so the status of this show subbing is in limbo. If you want to help out, please contact me on on one of the following channels listed in [this page](https://subs.j-idols.net/subtitles/).