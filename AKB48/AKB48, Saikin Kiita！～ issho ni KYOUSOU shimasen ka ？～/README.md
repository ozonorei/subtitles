# AKB48, Saikin Kiita！～ issho ni KYOUSOU shimasen ka ？～ (AKB48、最近聞いた！～一緒にKYOUSOUしませんか？～)

The 4th installment of the `AKB48, Saikin Kiita?` series. 

And One of the last active AKB48 variety show. The show is hosted by Hiroyuki and features AKB48 members as guests. 

![Show Poster](fanart.jpg)

# MC

* Hiroyuki
  
# Program Information

* Broadcast on: TV Tokyo, TVer
* Air On : Tuesday, 25:30~ (01:30 AM) (JST)
* Started: 2024-04-02
* Status :  Continuing
* Genre  : Variety, Documentary, Music, Travel.
* Links  : [TV Tokyo](https://www.tv-tokyo.co.jp/akb48saikinkiitayone/), [TVer](https://tver.jp/series/srbp1t6umv)

# Notes

## 2024-04-03

The continuation of the show was announced under the name of `AKB48, Saikin Kiita！～ issho ni KYOUSOU shimasen ka ？～`. replacing the previous show named `AKB48, Saikin Kiita yo ne...`.
