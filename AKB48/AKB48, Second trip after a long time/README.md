# AKB48, Second trip after a long time (AKB48、久しぶりの二度目旅)

This program is a program that proposes a new form of travel that says, "Traveling is fun the second time.".

![Show Poster](fanart.jpg)
  
# Program Information

* Broadcast on: TVer
* Air On : Saturday, ??:??~ (JST)
* Started: 2024-08-03
* Status : Continuing
* Genre  : Documentary, Travel.
* Links  : [TVer](https://tver.jp/series/sruh5975ky)

# Notes

## 2024-08-03

A new AKB48 show, there is still little information about it.
