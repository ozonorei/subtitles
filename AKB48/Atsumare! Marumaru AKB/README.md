# Atsumare! Marumaru AKB (集まれ！まるまるAKB)

A large gathering of AKB48 members who are grouped together in a 〇〇 genre! Members belonging to a similar genre will gather together to carry on niche projects and conversations that can only be understood by other 〇〇 members.

![Poster](fanart.jpg)

# Airing Status

* Channel: TV Asahi
* Started: 2023-05-24
* Status: Continuing