# The Person I Want to Meet the Most (いちばん逢いたいひと) (2023)

Based on the true story of producer Tomoko Hori, whose family survived her daughter's leukemia, the film tells the story of Kaede ( Narumi Kuranoo ), a girl who overcame acute myeloid leukemia , and the man who became her donor . Draws on the theme of ``Bone Marrow Transplant''. A road movie in which Kaede travels

![Poster](fanart.jpg)

# Program Information

* Broadcast on: -
* Air On : -
* Started: 2023-02-17
* Status : Ended
* Genres : Drama, Movie
* Links  : [Movie page](https://www.ichi-ai.com/), [tmdb](https://www.themoviedb.org/movie/1033910).