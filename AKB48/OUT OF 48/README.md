# OUT OF 48

OUT OF 48 is the latest AKB48 tv show, The show concept is audition based where a new idol group will be created from both young AKB48 members and completely new applicants. This show replaced AKB48 Sayonara Mouri-san, which aired it's final episode on 23/03/2023.

![UNLAME Members](fanart.jpg)

# MCs

* Maeda Gouki (Actor) - `First half of the show` 
* Machida "NTV" Hironori (Announcer) - `First half of the show`
* Kashiwagi "AKB48" Yuki. (Idol) - `First half of the show`

# Program Information

* Broadcast on: NTV, Tver, Hulu
* Air On : Thursday, 24:59~ (JST), 
* Started: 2023-04-20
* Status : Continuing
* Genres : Music, Documentary
* Links  : [NTV](https://www.ntv.co.jp/outof48), [TVer](https://tver.jp/series/srn72cwlv5), [Hulu](https://www.hulu.jp/out-of-48)

# Notes

## 2024-02-15

Starting with episode episode `41`, we will use TVer as the source for the raws and AISubs.

## 2024-01-23

The original uploader of the source material is no longer interested in this show, so the status of this show subbing is in limbo. If you want to help out, please contact me on on one of the following channels listed in [this page](https://subs.j-idols.net/subtitles/).