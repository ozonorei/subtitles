1
00:00:10,178 --> 00:00:15,178
I'm used to the shadows today, so I'll do my best.

2
00:00:15,178 --> 00:00:17,178
Thank you for your support.

3
00:00:18,178 --> 00:00:21,178
I'm first place again today, so I think I can do it.

4
00:00:27,970 --> 00:00:35,970
It's my first time to cut my hair because I'm a nurse today.

5
00:00:35,970 --> 00:00:37,970
I'm looking forward to it.

6
00:00:37,970 --> 00:00:41,970
I want to be a handsome guy.

7
00:00:47,234 --> 00:00:49,234
I'm sorry.

8
00:01:00,234 --> 00:01:05,234
Today is AKB48 middle school graduation celebration live.

9
00:01:05,234 --> 00:01:12,234
Before greeting the new members, we would like to thank you for coming to AKB48.

10
00:01:12,234 --> 00:01:16,234
First of all, I would like to give you a tour of the venue.

11
00:01:16,234 --> 00:01:22,234
This park is all one line, so please take care of your own safety.

12
00:01:22,234 --> 00:01:28,234
We will not let you step into or jump into the water.

13
00:01:28,234 --> 00:01:33,234
If you do not follow the instructions of the judge, you may have to leave.

14
00:01:33,234 --> 00:01:36,234
Please understand in advance.

15
00:01:36,234 --> 00:01:42,234
In addition to the photographer's seat, cameras, video cameras, mobile phones, etc.

16
00:01:42,234 --> 00:01:47,234
Recording devices, etc. will be used for recording and recording.

17
00:01:47,234 --> 00:01:50,234
Smartphones, mobile phones, etc.

18
00:01:50,234 --> 00:01:58,234
If you have them, please turn off all of them or put them in learning mode and use them during performance.

19
00:01:58,234 --> 00:02:04,234
In addition, during the performance, we will instruct you to evacuate.

20
00:02:04,234 --> 00:02:10,234
We will instruct you to evacuate when there is an emergency, but we ask you to dance well.

21
00:02:10,234 --> 00:02:17,234
If you need a guitar, please find a guitar and dance.

22
00:02:17,234 --> 00:02:22,234
Please pay attention to the behavior of the audience.

23
00:02:23,234 --> 00:02:24,234
It's enough.

24
00:02:24,234 --> 00:02:25,234
Non-chan, this way.

25
00:02:28,234 --> 00:02:30,234
What was it? Yes, the yellow one.

26
00:03:17,570 --> 00:03:19,570
I'm getting addicted to it.

27
00:03:20,570 --> 00:03:22,118
Should we stop it?

28
00:03:36,546 --> 00:03:43,546
The only thing left is the effect and the camera, so let's take care of each and every one of them!

29
00:03:43,546 --> 00:03:47,546
It will be on soon! Please wait a little longer!

30
00:03:47,546 --> 00:03:50,790
That's all for AKB48's 17th generation!

31
00:04:00,514 --> 00:04:02,514
All right, let's do our best!

32
00:04:22,514 --> 00:04:24,514
All right, let's do it on time!

33
00:04:29,514 --> 00:04:37,510
I thought it would be bright.

34
00:05:07,298 --> 00:05:10,298
Are you excited?

35
00:05:11,298 --> 00:05:13,298
Look forward to it!

36
00:05:17,530 --> 00:05:27,490
I'll scream so much that I'll get out of breath and I'll have a serious love increase.

37
00:05:27,490 --> 00:05:30,730
Oh no! Did I say it? Did I say it? Eh? I said it?

38
00:22:53,822 --> 00:22:58,222
With the hashtag #akb48... Ah, that's not it.

39
00:22:58,222 --> 00:23:02,154
With the hashtag #akb48, before you say goodbye, please spread the word more and more!

40
00:23:51,582 --> 00:23:55,178
Before I say goodbye, I'd like to say a few words of encouragement.

41
00:23:59,358 --> 00:24:03,358
Hi, I'm Kairi Komoto, a senior high school entrance examiner.

42
00:24:07,646 --> 00:24:09,646
Thank you very much.

43
00:24:09,646 --> 00:24:18,282
Today, I'd like to join hands with all of you and get fired up!

44
00:24:24,190 --> 00:24:26,190
I'm Mizuminko and Mizushima Miyu from AKB48!

45
00:24:33,310 --> 00:24:37,310
We'll do our best to make you like us!

46
00:24:37,310 --> 00:24:40,310
Please watch us! Thank you!

47
00:24:42,310 --> 00:24:46,310
I'm 19th grade research student Sari, also known as Shiratori Sari!

48
00:24:46,310 --> 00:24:48,310
Sari Sari Sari!

49
00:24:50,310 --> 00:24:53,310
We ate a lot of delicious food and became more energetic than ever,

50
00:24:53,310 --> 00:24:57,310
so we'd like to have more fun with you than usual! Thank you!

51
00:25:08,254 --> 00:25:12,426
I've been working out for a year and a half, so I'm in better shape than the president!

52
00:25:31,518 --> 00:25:39,518
Yesterday and today, I ate too much miso katsu and now I'm eating sweet potato sauce, but I'd like to eat that sweet potato sauce and katsu-sized sweet potato sauce with you all!

53
00:25:39,518 --> 00:25:39,946
Let's do our best together!

54
00:27:11,008 --> 00:27:18,828
I'm going to show you all of me that you can only see in Nagoya, so please look forward to it when it comes out!

55
00:27:18,828 --> 00:27:21,188
Yeah!

56
00:27:21,188 --> 00:27:25,428
Yes, I'm 17-year-old Eri-chan and Hashimoto Eriko! Nice to meet you!

57
00:27:25,428 --> 00:27:27,708
Eri-chan!

58
00:27:27,708 --> 00:27:36,348
Yes, I was already looking forward to Della today, so let's sweat a lot and get excited together! Thank you!

59
00:27:36,348 --> 00:27:40,188
Yeah!

60
00:27:40,188 --> 00:27:43,758
Yes, I'm 17-year-old Ai-chan, Sato Eri! Nice to meet you!

61
00:27:49,282 --> 00:28:01,282
Actually, my grandmother is from that area, so she watched my performance and was very happy to see me again after a long time.

62
00:28:01,282 --> 00:28:02,282
So...

63
00:28:02,282 --> 00:28:03,282
So...

64
00:28:03,282 --> 00:28:06,282
So, please show me more of your energy!

65
00:28:06,282 --> 00:28:07,534
Thank you for coming today!

66
00:28:15,170 --> 00:28:17,170
I'm Moka Moka. Nice to meet you!

67
00:28:25,506 --> 00:28:27,506
I want you to be sweet to me, but...

68
00:28:27,506 --> 00:28:27,822
I want you to be sweet to me, but...

69
00:28:49,474 --> 00:28:51,474
I can't say anything about you!

70
00:28:51,474 --> 00:28:52,046
I can't say anything about you!

71
00:28:59,810 --> 00:29:03,630
I will perform with a lot of smiles, so please look only at me!

72
00:29:20,386 --> 00:29:25,038
I ate a lot of delicious food at my parents' house yesterday and today, so I'm full of energy!

73
00:29:29,186 --> 00:29:31,566
I'm full of energy, so everyone...

74
00:30:16,362 --> 00:30:18,362
I'm full of happy feelings.

75
00:30:18,362 --> 00:30:23,022
I think it's the first time for everyone in Nagoya, so I hope you'll enjoy it with me!

76
00:30:52,290 --> 00:31:02,290
I hope you'll love me more and more, and I'll do my best to give you my best performance!

77
00:31:02,290 --> 00:31:03,662
Thank you!

78
00:31:14,466 --> 00:31:20,466
I'll do my best to make you love me more and more, and I hope you'll love me more and more, and I'll do my best to give you my best performance!

79
00:31:27,554 --> 00:31:30,634
OK! I'm Kasumin from Juhazu Research School, Okudo Kasumin!

80
00:31:30,954 --> 00:31:32,714
Kasumin!

81
00:31:34,594 --> 00:31:39,502
Today I'll do my best to make you love me more and more!

82
00:31:50,530 --> 00:31:56,462
Even if I'm alone, I'll do my best to make you love me more and more! Thank you!

83
00:32:05,090 --> 00:32:07,566
This is the limited unit corner of the park!

84
00:48:34,758 --> 00:48:36,758
Yes! I was happy!

85
00:48:36,758 --> 00:48:38,002
The energy was amazing!

86
00:49:00,678 --> 00:49:03,678
I was dancing between M's and M's. Thank you!

87
00:49:03,678 --> 00:49:05,678
You were in the middle at that time, weren't you?

88
00:49:05,678 --> 00:49:07,678
Yes, I was.

89
00:49:07,678 --> 00:49:09,234
You looked like you were having fun.

90
00:49:12,710 --> 00:49:13,710
This is how you cut it.

91
00:49:13,710 --> 00:49:15,026
Yes, you cut it!

92
00:49:40,742 --> 00:49:45,906
We are doing a live broadcast on the SCAPAR program delivery provided by FAMILY THEATRE!

93
00:49:55,398 --> 00:50:00,398
Please check it out if you come to the venue!

94
00:50:00,398 --> 00:50:03,398
Thank you!

95
00:50:03,398 --> 00:50:09,398
The goods for this concert are also very cute.

96
00:50:09,398 --> 00:50:13,398
We would like to do a project using such goods.

97
00:50:13,398 --> 00:50:14,834
It is called "Big Change"!

98
00:50:19,654 --> 00:50:24,654
We would like to draw one and talk about the members who drew it.

99
00:50:24,654 --> 00:50:26,654
Then I will draw it right away.

100
00:50:33,678 --> 00:50:34,678
What is it?

101
00:50:34,678 --> 00:50:36,678
Kohi-chan is...

102
00:50:36,678 --> 00:50:41,678
Actually, I am the one who is saying "Kohi-chan, Kohi-chan",

103
00:50:41,678 --> 00:50:51,678
but Kohi-chan contacted me and said "I am lonely" only a few hours after I moved.

104
00:50:51,678 --> 00:50:53,678
She is quite a spoiled girl.

105
00:50:53,678 --> 00:50:54,678
Is she?

106
00:50:54,678 --> 00:50:55,678
I didn't like it.

107
00:50:55,678 --> 00:50:57,678
So, please spoil her a lot.

108
00:50:57,678 --> 00:50:58,678
She is cute.

109
00:50:58,678 --> 00:50:59,678
She is cute.

110
00:50:59,678 --> 00:51:00,678
She is cute, isn't she?

111
00:51:00,678 --> 00:51:01,678
She is cute.

112
00:51:01,678 --> 00:51:02,678
She is cute.

113
00:51:02,678 --> 00:51:03,678
She is noisy.

114
00:51:03,678 --> 00:51:04,678
She is embarrassed.

115
00:51:04,678 --> 00:51:05,678
She is embarrassed and cute.

116
00:51:05,678 --> 00:51:07,678
Then, next is Rai-chan.

117
00:51:07,678 --> 00:51:08,678
I didn't hear it earlier.

118
00:51:08,678 --> 00:51:09,074
Oh, Rai-chan.

119
00:51:21,166 --> 00:51:26,166
Her hair was originally long, but I cut it about the same length as Kawaii-chan's.

120
00:51:26,166 --> 00:51:31,166
And Kawaii-chan has the same length of hair as me, and I'm happy about it.

121
00:51:31,166 --> 00:51:33,166
Eh, Kawaii!

122
00:51:33,166 --> 00:51:37,166
Kawaii! Kawaii! Kawaii! Kawaii!

123
00:51:37,166 --> 00:51:45,166
Kawaii-chan, when you first did the Sender Tour, you set a goal of taking photos with all of your seniors.

124
00:51:45,166 --> 00:51:48,166
If you haven't taken photos yet, I hope you will.

125
00:51:48,166 --> 00:51:49,166
Oh, no!

126
00:51:49,166 --> 00:51:50,166
You haven't?

127
00:51:50,166 --> 00:51:51,166
No, I haven't.

128
00:51:51,166 --> 00:51:52,166
No, I haven't.

129
00:51:52,166 --> 00:51:53,166
Ah, that's sad.

130
00:51:53,166 --> 00:51:54,166
Then, please take photos of me.

131
00:51:54,166 --> 00:51:55,166
Okay.

132
00:51:55,166 --> 00:51:57,166
Then, next is Fukuoka.

133
00:51:57,166 --> 00:51:58,166
Fukuoka, right?

134
00:51:58,166 --> 00:51:59,166
Yes.

135
00:51:59,166 --> 00:52:00,166
Eh, Kyoka! Kyoka!

136
00:52:04,774 --> 00:52:06,774
And Koshi-chan! Here you go.

137
00:52:15,310 --> 00:52:23,310
I've been saying from the beginning that I like you, but you've been cheating a lot lately and I don't know where you are.

138
00:52:23,310 --> 00:52:27,310
There's a part where you two dance as a pair at the summer club.

139
00:52:27,310 --> 00:52:33,310
You smiled a lot in the beginning, but you haven't looked at me much lately.

140
00:52:33,310 --> 00:52:35,310
I looked at you today.

141
00:52:35,310 --> 00:52:37,310
You looked at me today.

142
00:52:37,310 --> 00:52:39,310
Did you do it on purpose?

143
00:52:39,310 --> 00:52:41,310
I'm looking at you.

144
00:52:41,310 --> 00:52:46,310
You said you like me a lot, so I'm happy for you.

145
00:52:46,310 --> 00:52:50,310
You hugged me like Eriko-san in class.

146
00:52:50,310 --> 00:52:52,310
I think you're cute.

147
00:52:52,310 --> 00:52:55,310
Koshi-chan, who do you think Eriko-chan is cheating on you with lately?

148
00:52:55,310 --> 00:52:59,310
Eriko-san goes to a lot of places.

149
00:52:59,310 --> 00:53:04,310
But Kasumi is very active, so I don't think she can lose.

150
00:53:04,310 --> 00:53:10,310
Kasumin is very active and says nice things, but Koshi-chan is even more active and says nice things.

151
00:53:10,310 --> 00:53:11,310
Can I say it?

152
00:53:11,310 --> 00:53:12,310
You said it, didn't you?

153
00:53:12,310 --> 00:53:14,310
I'll say it from today.

154
00:53:14,310 --> 00:53:16,310
Please say it a lot.

155
00:53:16,310 --> 00:53:18,310
I'd like to say it.

156
00:53:18,310 --> 00:53:24,310
I'd like to go back to the performance because it was a good time.

157
00:53:43,910 --> 00:53:48,434
So please listen. "恋積んじゃった"

158
01:04:03,090 --> 01:04:04,090
"恋積んじゃった"

159
01:04:04,090 --> 01:04:04,406
"恋積んじゃった"

160
01:05:54,314 --> 01:05:56,314
"恋積んじゃった" "恋積んじゃった"

161
01:06:27,082 --> 01:06:38,082
As you can see, her eyes are big and her face is small, so I drew it like this to emphasize it.

162
01:06:38,082 --> 01:06:42,082
I'm happy with my sparkling eyes.

163
01:06:42,082 --> 01:06:45,910
It's cute and sparkling.

164
01:07:06,482 --> 01:07:08,482
I drew it while observing.

165
01:07:08,482 --> 01:07:10,482
So I'm confident that it looks like me.

166
01:07:10,482 --> 01:07:13,482
And Kawaii always has beautiful bangs.

167
01:07:50,410 --> 01:07:54,410
We will write it, so we would be happy if you could check it out.

168
01:07:54,410 --> 01:07:56,410
Thank you!

169
01:07:56,410 --> 01:07:59,862
Finally, please throw this red paper to everyone!

170
01:35:11,028 --> 01:35:13,028
Thank you for watching!

171
01:40:09,750 --> 01:40:11,750
Thank you for watching!

172
01:40:11,750 --> 01:40:13,282
See you next week!

173
01:40:21,846 --> 01:40:24,802
It was fun!

174
01:40:29,630 --> 01:40:37,630
I was happy to see the smiles and laughter of the fans.

175
01:40:44,630 --> 01:40:55,630
At first, I was nervous, but I think I'm getting better and better at enjoying and getting excited.

176
01:40:58,630 --> 01:41:00,546
Thank you for your hard work!

177
01:41:06,838 --> 01:41:18,338
Thank you for coming to AKB48's 17th grade elementary school commemorative live show "New member greeting tour, we are AKB48"!

178
01:41:18,338 --> 01:41:23,038
This has been AKB48's 19th grade research student, Ito Momoka!

179
01:41:31,478 --> 01:41:35,362
She was 17 years old at the time of the war.

180
01:42:52,862 --> 01:42:55,862
It was my first time to perform in this venue.

181
01:42:56,862 --> 01:43:00,862
Until now, NICOLE's venue had a different image unit.

182
01:43:01,862 --> 01:43:10,862
It's been a while since I've had an image-like adult-like unit that I like, so it was a lot of fun.

183
01:43:11,862 --> 01:43:13,862
Did you get used to this venue?

184
01:43:14,862 --> 01:43:16,862
I got used to it!

185
01:43:16,862 --> 01:43:23,862
I think that's what I can say about this venue.

186
01:43:24,862 --> 01:43:27,862
I'm excited every time, and I had a lot of fun today.

187
01:43:30,862 --> 01:43:35,862
I'm really looking forward to the second venue!

188
01:43:36,862 --> 01:43:38,862
I want to enjoy the rest of it!

189
01:43:39,862 --> 01:43:41,862
What about FIKUOKA?

190
01:43:42,862 --> 01:43:44,862
FIKUOKA...

191
01:43:49,862 --> 01:43:57,862
I've never done a live show in FIKUOKA, so I'd like to challenge it again with a fresh feeling.

192
01:43:58,862 --> 01:44:01,862
Of course I want to enjoy it too!

193
01:44:02,862 --> 01:44:05,862
I hope all the fans will enjoy it!

194
01:44:05,862 --> 01:44:08,862
It's really warm here.

195
01:44:09,862 --> 01:44:15,862
It's the first time we've done a song with AICHI.

196
01:44:16,862 --> 01:44:21,862
It's AICHI, so when that song comes out, it's really exciting!

197
01:44:22,862 --> 01:44:28,862
I really wanted to do a local song or a lot of songs.

198
01:44:29,862 --> 01:44:31,862
I've been to a lot of places.

199
01:44:31,862 --> 01:44:46,862
Now I'm doing it in a few places like this, but one day I'd like to go to more places and do a lot of local songs at a bigger venue.

200
01:44:48,228 --> 01:44:50,228
It was really fun.

201
01:44:50,228 --> 01:44:52,228
The distance is close.

202
01:44:52,228 --> 01:44:54,228
That's right. That's a really good thing.

203
01:44:54,228 --> 01:44:56,228
It's the same as the park.

204
01:44:56,228 --> 01:45:03,228
It's a tour, but the good thing is that even though the distance is close, we're right here.

205
01:45:03,228 --> 01:45:09,228
We can make eye contact with you and have a lot of fun.

206
01:45:09,228 --> 01:45:16,228
And we have photographers this time, so you can take pictures of us.

207
01:45:16,228 --> 01:45:18,228
What kind of atmosphere is it?

208
01:45:18,228 --> 01:45:22,228
I wonder if it will be transmitted to those who are not here.

209
01:45:22,228 --> 01:45:24,228
It was really good.

210
01:45:24,228 --> 01:45:26,228
Thank you for your hard work.

211
01:45:26,228 --> 01:45:28,228
Thank you for your hard work.

212
01:47:45,206 --> 01:47:50,206
I'm happy to be able to get excited with you.

213
01:48:33,238 --> 01:48:35,238
Thank you for your understanding.

