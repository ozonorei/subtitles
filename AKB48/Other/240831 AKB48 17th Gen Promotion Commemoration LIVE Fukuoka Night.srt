1
00:00:06,690 --> 00:00:07,690
Thank you.

2
00:00:09,690 --> 00:00:10,690
Thank you.

3
00:00:11,690 --> 00:00:12,690
I'm turning it.

4
00:00:15,690 --> 00:00:16,690
You somehow made it.

5
00:00:17,690 --> 00:00:19,690
Yes, the plane shook a lot.

6
00:00:21,690 --> 00:00:22,690
Was it okay?

7
00:00:22,690 --> 00:00:23,690
Were you scared?

8
00:00:23,690 --> 00:00:24,690
I was scared.

9
00:00:25,690 --> 00:00:27,690
The video was uploaded a lot, wasn't it?

10
00:00:28,690 --> 00:00:29,690
Yes, it was.

11
00:00:29,690 --> 00:00:30,690
It was completed at that time, wasn't it?

12
00:00:30,690 --> 00:00:31,690
Yes, it was.

13
00:00:32,690 --> 00:00:34,690
I'm glad it was completed.

14
00:00:35,690 --> 00:00:36,690
I'm really glad it was completed.

15
00:00:37,690 --> 00:00:38,690
Thank you.

16
00:00:38,690 --> 00:00:39,690
I'll do my best.

17
00:00:39,690 --> 00:00:40,690
Thank you.

18
00:00:49,978 --> 00:00:51,978
Do your best.

19
00:00:57,314 --> 00:00:59,314
I'm going to cry.

20
00:01:01,314 --> 00:01:02,314
What?

21
00:01:03,314 --> 00:01:05,314
Be quiet if you don't want to be caught.

22
00:01:10,314 --> 00:01:12,314
I want to go soon.

23
00:01:16,314 --> 00:01:17,314
I'll be there soon.

24
00:01:18,314 --> 00:01:19,314
I'm going to make a hole.

25
00:01:20,314 --> 00:01:21,314
I love you.

26
00:01:22,314 --> 00:01:34,314
Thank you for coming to AKB48's 17th anniversary live show "New members, greetings tour - we are AKB48".

27
00:01:35,314 --> 00:01:39,314
First of all, I would like to give you a briefing.

28
00:01:40,314 --> 00:01:45,314
This show is restricted to all seats, so please watch it at your own seat.

29
00:01:45,314 --> 00:01:50,314
Please do not cross the aisle or jump off the stage.

30
00:01:51,314 --> 00:01:58,314
If you do not follow the rules of the show, you may be punished, so please understand in advance.

31
00:01:59,314 --> 00:02:11,314
Also, except for photographers, cameras, video cameras, mobile phones, recording devices, recordings, recordings will only be prohibited.

32
00:02:11,314 --> 00:02:21,314
If you have a smartphone or mobile phone, please turn it off or put it in "manner mode" and use it in the park.

33
00:02:22,314 --> 00:02:29,314
In addition, during the show, we will cancel the evacuation guide.

34
00:02:30,314 --> 00:02:37,314
We will prevent the evacuation, but we ask you to confirm the evacuation route in advance.

35
00:02:38,314 --> 00:02:45,314
If evacuation is required, please follow the guidelines of the staff and act calmly.

36
00:02:46,314 --> 00:02:49,314
Please note the noise and noise in the audience.

37
00:02:50,314 --> 00:02:54,314
Please manage your luggage and valuables yourself.

38
00:02:56,336 --> 00:03:06,336
Also, the concert "夜公演" will be live-streamed on the family theater's promotion "スカパー" program.

39
00:03:06,336 --> 00:03:15,336
From October, it will be broadcast on the family theater with a special video, so please check it out.

40
00:03:15,336 --> 00:03:20,102
Everyone, we've made a lot of calls and are ready to call our names!

41
00:03:32,666 --> 00:03:36,666
We're AKB48's 17th generation. We hope you'll support us!

42
00:03:56,418 --> 00:03:58,418
Yes, you are right.

43
00:03:58,418 --> 00:04:06,418
I'm glad that we could hold this event in a situation where we don't know whether we can come or not.

44
00:04:06,418 --> 00:04:12,418
I hope we can finish this event safely in Kanagawa Park.

45
00:04:13,418 --> 00:04:16,418
Today is the last day in the region.

46
00:04:16,418 --> 00:04:17,418
It's fun.

47
00:04:17,418 --> 00:04:18,418
Yes, it is.

48
00:04:18,418 --> 00:04:19,418
I don't like it.

49
00:04:19,418 --> 00:04:21,418
I still want to go somewhere.

50
00:04:21,418 --> 00:04:23,418
I've been wanting to go for my whole life.

51
00:04:23,418 --> 00:04:24,418
You haven't been to Hokkaido yet, have you?

52
00:04:25,418 --> 00:04:26,418
I haven't been there yesterday, either.

53
00:04:26,418 --> 00:04:27,418
I haven't been there yesterday, either.

54
00:04:27,418 --> 00:04:28,418
I haven't been there, either.

55
00:04:28,418 --> 00:04:29,418
I haven't been there, either.

56
00:04:29,418 --> 00:04:30,418
No, you haven't.

57
00:04:30,418 --> 00:04:31,418
I'm going to go there.

58
00:04:31,418 --> 00:04:33,418
I'm waiting for additional support.

59
00:04:33,418 --> 00:04:35,418
I haven't been to Saitama, either.

60
00:04:35,418 --> 00:04:38,418
Well, it doesn't change where you are.

61
00:04:40,418 --> 00:04:42,418
I don't want it to end.

62
00:04:47,394 --> 00:04:48,394
What is it?

63
00:04:51,394 --> 00:04:53,394
Was it number one?

64
00:04:53,394 --> 00:04:55,394
Is it usually number one?

65
00:05:00,394 --> 00:05:05,394
This aisle is not as wide as usual,

66
00:05:05,394 --> 00:05:09,394
but I can see a lot of customers, so it's fun to watch.

67
00:05:14,426 --> 00:05:16,426
Thank you for your support.

68
00:05:29,426 --> 00:05:31,426
I want to eat it.

69
00:05:50,882 --> 00:05:52,882
Don't call me.

70
00:05:54,882 --> 00:05:56,882
Love love love.

71
00:05:57,882 --> 00:05:59,882
Tell her, Chan.

72
00:05:59,882 --> 00:06:03,882
Lovely, lovely.

73
00:06:03,882 --> 00:06:05,882
We can see it with our own hearts.

74
00:06:05,882 --> 00:06:07,882
Lovely, lovely.

75
00:06:12,882 --> 00:06:14,882
I like that, lovely.

76
00:06:14,882 --> 00:06:16,882
What should we do?

77
00:06:20,882 --> 00:06:21,882
The last one.

78
00:06:22,882 --> 00:06:23,882
Last one.

79
00:06:33,594 --> 00:06:37,510
AKB48!

80
00:13:29,372 --> 00:13:31,372
AKB48!

81
00:13:31,372 --> 00:13:32,234
AKB48!

82
00:22:00,382 --> 00:22:03,382
And here is an announcement!

83
00:22:03,382 --> 00:22:12,382
Now, thanks to the support of Family Theater, we are able to broadcast "Yoru Kouen" on SCAPA programs.

84
00:22:12,382 --> 00:22:20,490
But for the most popular program, "Kanagawa Kouen", which will be broadcasted on September 29th, will be broadcasted by Family Theater.

85
00:22:27,006 --> 00:22:30,570
For more information, please check out the Family Theater's official homepage.

86
00:22:35,102 --> 00:22:40,902
Once again, to show you more of us, we have a photographer with us.

87
00:22:40,902 --> 00:22:44,330
We'd like you to take a group photo with us!

88
00:23:08,134 --> 00:23:11,134
Are you ready?

89
00:23:11,134 --> 00:23:13,134
We'll start in five seconds!

90
00:23:17,502 --> 00:23:19,502
Time's up!

91
00:23:25,214 --> 00:23:33,354
Yes! #akb48greetings Please spread it more and more! Thank you in advance! Thank you in advance!

92
00:24:00,030 --> 00:24:02,030
Thank you in advance!

93
00:24:02,030 --> 00:24:04,030
Thank you in advance!

94
00:24:15,358 --> 00:24:22,578
Let's introduce ourselves one by one with Hakata-ben.

95
00:24:22,578 --> 00:24:24,578
Let's start with Mizumi-san.

96
00:24:24,578 --> 00:24:29,178
Hi, I'm Mizumi Ika and Mizushima Miyu of AKB48 17th generation.

97
00:24:34,398 --> 00:24:36,398
I want you to like me.

98
00:24:44,102 --> 00:24:45,102
Thank you.

99
00:24:49,102 --> 00:24:52,102
I'm Yuki-tan, 17 years old. Nice to meet you.

100
00:24:52,102 --> 00:24:54,102
Yuki-tan!

101
00:24:56,102 --> 00:25:00,102
This is the last performance in the local park.

102
00:25:00,102 --> 00:25:04,102
Please have fun!

103
00:25:04,102 --> 00:25:05,482
I'm looking forward to it!

104
00:25:09,374 --> 00:25:11,374
I'm Kairi Tokumoto, a member of AKB48!

105
00:25:16,030 --> 00:25:22,030
I want to shine your hearts like the shining sun in the sky!

106
00:25:42,334 --> 00:25:44,334
I'm Hiroto Yuki and I'm 17 years old!

107
00:25:45,334 --> 00:25:46,334
Yuki!

108
00:25:47,334 --> 00:25:49,334
Get ready to have fun tonight!

109
00:25:56,766 --> 00:25:57,766
So cute!

110
00:25:57,766 --> 00:25:58,766
Go for it!

111
00:25:58,766 --> 00:25:59,146
Go for it!

112
00:26:19,774 --> 00:26:21,774
I'd like to make fun memories with you.

113
00:26:21,774 --> 00:26:22,570
I'd like to make fun memories with you.

114
00:27:04,418 --> 00:27:11,086
As Yuki Ota said, this is the last time we'll be performing in a local park. Let's have fun!

115
00:28:13,218 --> 00:28:15,218
Hello, I'm Kudo Kasumi and I'm a first-year research student.

116
00:28:15,218 --> 00:28:17,218
Kasumi!

117
00:28:19,218 --> 00:28:23,218
If you don't get excited, I'll turn you into mentaiko.

118
00:28:23,218 --> 00:28:24,218
Huh?

119
00:28:24,218 --> 00:28:25,218
Let's...

120
00:28:25,218 --> 00:28:26,218
Huh?

121
00:28:26,218 --> 00:28:27,566
Let's get excited!

122
00:28:59,426 --> 00:29:02,062
Today, I'm going to lie about everyone one by one!

123
00:29:31,842 --> 00:29:35,342
I'm Eri-chan aka Eriko Hashimoto. Nice to meet you!

124
00:29:35,342 --> 00:29:37,342
Eri-chan!

125
00:29:37,342 --> 00:29:45,342
Today, I'm going to lie about everyone one by one!

126
00:29:45,342 --> 00:29:47,342
Nice to meet you!

127
00:29:50,530 --> 00:29:54,530
I'm 19th grade research student, SALLY, also known as SIROTOLI SALLY!

128
00:29:54,530 --> 00:29:56,530
SALLY-chan!

129
00:29:57,530 --> 00:30:00,530
This is my first time in Fukuoka,

130
00:30:00,530 --> 00:30:05,530
so I want to make a lot of fun memories with you and make you all smile!

131
00:30:05,530 --> 00:30:06,734
Jan! Nice to meet you!

132
00:30:10,210 --> 00:30:14,574
I'm Yamaguchi Yuichiko, 18 years old, research student. Nice to meet you!

133
00:30:18,178 --> 00:30:22,178
Are you ready to have fun?

134
00:30:30,050 --> 00:30:41,410
If you want me to beat you up, I won't forgive you if you don't do your best, okay?

135
00:30:41,410 --> 00:30:43,410
Wow!

136
00:30:43,410 --> 00:30:46,606
Let's have fun together until the end of the show!

137
00:31:09,410 --> 00:31:17,410
I hit my shoe in the afternoon park, but it was too fun and I blew up in the afternoon park.

138
00:31:43,170 --> 00:31:45,170
Anyway, thank you so much!

139
00:32:22,722 --> 00:32:25,422
I love you!

140
00:32:49,474 --> 00:32:54,474
I love you, but do you like me?

141
00:33:02,338 --> 00:33:06,338
Today is the last day of school performance. Chikappa, are you lonely?

142
00:33:10,946 --> 00:33:12,946
So I want to liven up a lot! Thank you!

143
00:33:52,962 --> 00:33:54,962
I love you so much!

144
00:34:16,002 --> 00:34:21,002
Next is a unit corner exclusive to Fukuoka Park.

145
00:34:21,002 --> 00:34:23,002
Here is the first song!

146
00:34:44,802 --> 00:34:46,802
君だけに... (kimi dake ni...) (for you only...)

147
00:49:30,662 --> 00:49:36,662
Thank you for watching Fukuoka Park Unit!

148
00:49:43,726 --> 00:49:49,726
Now, let's talk about the unit, not the announcement.

149
00:49:50,726 --> 00:49:57,726
Four of you are in charge of "Chu Chu Chu" and we did "Kimia Pegasus".

150
00:49:58,726 --> 00:49:59,726
How was it, Sora-chan?

151
00:50:00,726 --> 00:50:01,726
How was it?

152
00:50:02,726 --> 00:50:04,726
The costumes are really cool.

153
00:50:05,726 --> 00:50:10,726
I'm wondering if cool members will be chosen for the setlist.

154
00:50:10,726 --> 00:50:15,726
I know Sora-chan is cool and so is Koshi-chan.

155
00:50:16,726 --> 00:50:22,726
But I didn't think me and Bebe were cool.

156
00:50:23,726 --> 00:50:24,726
I was surprised.

157
00:50:25,726 --> 00:50:26,726
How was it, everyone?

158
00:50:27,726 --> 00:50:28,726
I like the gap.

159
00:50:29,726 --> 00:50:34,726
Recently, we did "Yabana Q&A" in Osaka Park.

160
00:50:35,726 --> 00:50:38,726
So I secretly asked for handsome members.

161
00:50:39,726 --> 00:50:40,726
That's good!

162
00:50:42,726 --> 00:50:43,726
You were handsome just now.

163
00:50:44,726 --> 00:50:45,726
Really?

164
00:50:46,726 --> 00:50:50,726
When you were wearing "Kimia Pegasus" costume, you went to get my earring for me.

165
00:50:51,726 --> 00:50:52,726
That's right.

166
00:50:53,726 --> 00:50:57,726
I asked you if you went to get it if it wasn't for this costume and you said yes.

167
00:50:58,726 --> 00:51:00,726
If you wear that costume, you will be handsome.

168
00:51:01,726 --> 00:51:02,726
That's right.

169
00:51:03,726 --> 00:51:08,726
But when you say I'm handsome, I want to brag about it.

170
00:51:09,726 --> 00:51:10,726
You're handsome!

171
00:51:12,726 --> 00:51:13,726
Thank you.

172
00:51:14,726 --> 00:51:16,726
How was it for you four?

173
00:51:17,726 --> 00:51:18,726
Did you win, Yuki-chan?

174
00:51:19,726 --> 00:51:25,726
After the afternoon performance, Yuki-san kept saying "I won't lose to Sae-chan!"

175
00:51:25,726 --> 00:51:31,726
That's right. Before the afternoon performance, we were competing for jumps like this.

176
00:51:32,726 --> 00:51:33,726
That's right.

177
00:51:34,726 --> 00:51:36,726
For the afternoon performance, we were conscious of flying high.

178
00:51:37,726 --> 00:51:39,726
For the evening performance, we were conscious of flying beautifully.

179
00:51:41,172 --> 00:51:43,172
The place of consciousness has changed.

180
00:51:44,172 --> 00:51:50,172
Asai-chan is tall and beautiful, so I thought I should learn from you.

181
00:51:51,172 --> 00:51:55,172
Until we left the stage, we practiced flying like this.

182
00:51:56,172 --> 00:51:59,172
We practiced flying in a very small space.

183
00:51:59,172 --> 00:52:01,172
You are too serious about flying.

184
00:52:01,172 --> 00:52:07,172
Before the evening performance started, we talked about how we had to put on some kind of dress to fly.

185
00:52:07,172 --> 00:52:11,172
You tried so hard to beat Asai-chan.

186
00:52:11,172 --> 00:52:13,172
You even came up with a plan.

187
00:52:13,172 --> 00:52:16,172
It's amazing how high you can fly.

188
00:52:16,172 --> 00:52:18,172
Please do your best again.

189
00:52:18,172 --> 00:52:21,172
If you have a chance to fly again.

190
00:52:21,172 --> 00:52:24,172
Please give us an announcement.

191
00:52:24,172 --> 00:52:30,738
For the evening performance of this tour, we will broadcast it live on Family Theater.

192
00:52:34,950 --> 00:52:39,950
From October, it will be broadcast on Family Theater with special footage.

193
00:52:39,950 --> 00:52:45,950
Everyone who came to the venue, please definitely watch it!

194
00:52:45,950 --> 00:52:48,950
Please!

195
00:52:48,950 --> 00:52:50,950
Yes, that's right.

196
00:52:50,950 --> 00:52:57,950
There are some people who have a little bit of it from here, but I'd like to do a corner using it.

197
00:52:57,950 --> 00:52:58,950
Please!

198
00:53:05,894 --> 00:53:16,894
There's a lot of members in the box, but I'd like to draw one and spread the talk of the members who drew it.

199
00:53:16,894 --> 00:53:17,894
Yes!

200
00:53:17,894 --> 00:53:18,894
Who will go first?

201
00:53:18,894 --> 00:53:19,894
Who will go first?

202
00:53:19,894 --> 00:53:20,894
Yes!

203
00:53:34,150 --> 00:53:36,150
It was at 9 o'clock, wasn't it?

204
00:53:36,150 --> 00:53:38,150
Yes, it was.

205
00:53:38,150 --> 00:53:40,150
Have you met us before?

206
00:53:40,150 --> 00:53:42,150
Yes, we have.

207
00:53:42,150 --> 00:53:44,150
We did it before we closed.

208
00:53:44,150 --> 00:53:46,150
We did it at 9 o'clock.

209
00:53:46,150 --> 00:53:48,150
Non-sama came out many times.

210
00:53:48,150 --> 00:53:50,150
Non-sama!

211
00:53:50,150 --> 00:53:51,186
Non-sama!

212
00:53:55,534 --> 00:53:56,534
I was thinking of doing it.

213
00:53:56,534 --> 00:53:57,534
You were thinking of doing it?

214
00:53:57,534 --> 00:53:59,534
She's already into it.

215
00:53:59,534 --> 00:54:01,534
She's already into lottery.

216
00:54:01,534 --> 00:54:03,534
Non-chan, you're amazing.

217
00:54:03,534 --> 00:54:05,534
I also won a lottery.

218
00:54:05,534 --> 00:54:13,534
Last time at Nagoya Park, I won Kudo Kasumi-chan's autographed tapestry.

219
00:54:13,534 --> 00:54:20,534
But this time, I won Mei Mei's crown badge and Sari-chan's...

220
00:54:20,534 --> 00:54:21,534
Area limited...

221
00:54:21,534 --> 00:54:23,534
Yes, I won an area limited one.

222
00:54:23,534 --> 00:54:26,534
But I haven't met Non-chan yet.

223
00:54:26,534 --> 00:54:27,534
It's nice.

224
00:54:27,534 --> 00:54:30,534
It's like a fateful encounter.

225
00:54:30,534 --> 00:54:31,534
Oh, is that so?

226
00:54:31,534 --> 00:54:33,534
I've fallen in love with her.

227
00:54:33,534 --> 00:54:34,534
Wow!

228
00:54:34,534 --> 00:54:38,534
I haven't seen you two together much, so...

229
00:54:38,534 --> 00:54:39,534
That's right.

230
00:54:39,534 --> 00:54:40,534
Your style is really good.

231
00:54:40,534 --> 00:54:41,534
It's a good combination.

232
00:54:41,534 --> 00:54:42,534
Thank you.

233
00:54:42,534 --> 00:54:44,534
I think Non-sama will be happy, too.

234
00:54:44,534 --> 00:54:45,534
Thank you.

235
00:54:45,534 --> 00:54:46,534
Oh!

236
00:54:46,534 --> 00:54:47,534
Oh!

237
00:54:47,534 --> 00:54:51,534
Now that we're ready, we'll return to our performance.

238
00:54:51,534 --> 00:54:53,462
Everyone, please stand up!

239
00:55:15,242 --> 00:55:19,242
Now, please listen. "恋つんじゃった"

240
01:04:22,442 --> 01:04:24,442
Thank you very much.

241
01:04:24,442 --> 01:04:25,442
Thank you very much.

242
01:04:25,442 --> 01:04:31,442
Yes, we 19th grade researchers will be hosting this show.

243
01:04:31,442 --> 01:04:32,442
Huh?

244
01:05:01,898 --> 01:05:03,898
You're in a good mood today, aren't you?

245
01:05:03,898 --> 01:05:05,398
That's right!

246
01:05:05,398 --> 01:05:15,414
I love mentaiko and I love Fukuoka, so I'm really happy to be able to eat mentaiko in Fukuoka!

247
01:05:26,858 --> 01:05:29,910
Please look forward to seeing who will be dressing up in Fukuoka!

248
01:05:33,738 --> 01:05:39,238
In this show, I've drawn a portrait of someone next to me.

249
01:05:58,922 --> 01:06:01,232
I've drawn a portrait of someone next to me!

250
01:06:05,962 --> 01:06:09,462
Please check it out in the archive stream as well!

251
01:06:11,462 --> 01:06:12,962
Okay, next up is Kaomura Yui-chan!

252
01:06:12,962 --> 01:06:14,962
Okay, I'm Kaomura Yui and...

253
01:06:14,962 --> 01:06:16,962
Itou Momoka-chan, how may I help you?

254
01:06:16,962 --> 01:06:17,686
Oh, how cute!

255
01:06:21,802 --> 01:06:23,802
- How cute! - You're good at it.

256
01:06:23,802 --> 01:06:26,802
Okay, we'll talk about the details when we're all done.

257
01:06:26,802 --> 01:06:27,702
- Okay! - Okay!

258
01:06:33,162 --> 01:06:38,602
- She's pretty good at it. - I drew a picture of Sari-chan in Seattle! - Ta-da!

259
01:06:38,602 --> 01:06:38,678
- I drew a picture of Sari-chan in Seattle! - Ta-da!

260
01:06:46,954 --> 01:06:49,750
- Yes! I'm a flower bird in Seattle!

261
01:07:06,122 --> 01:07:11,122
- Let's ask for points. Kumoto and Kairi, what are your points? - I wrote a lot of things!

262
01:07:11,122 --> 01:07:18,122
- First of all, Kawamura Yui-chan is this cute bob, so I wrote it with a round bob.

263
01:07:18,122 --> 01:07:26,122
- Also, Kawamura Yui-chan is a strawberry, right? So she's writing "strawberry" next to me, and she's shouting "strawberry!"

264
01:07:26,122 --> 01:07:33,122
- Yui-chan also shouted "strawberry"! If you look closely, she's shouting "strawberry", so that's Yui-chan's biggest point for me.

265
01:07:33,122 --> 01:07:37,122
- Oh, "strawberry" is your point! - I see!

266
01:07:37,122 --> 01:07:45,122
- Okay, then, Kumamoto Yui-chan. - For me, Ito Momo-chan is really sparkly, isn't she?

267
01:07:45,122 --> 01:07:49,122
- Sparkly visuals. - Wow, I'm happy!

268
01:07:49,122 --> 01:07:55,122
- So I made her sparkle and drew her big eyes. - Wow, I'm happy!

269
01:07:55,122 --> 01:08:00,122
- She also did one of her ears covered. - That's right, she's really cute.

270
01:08:00,122 --> 01:08:08,122
- Okay, then, my point is... Sari-chan is the youngest member of AKB48, and her eyes are really sparkly.

271
01:08:08,122 --> 01:08:15,122
- She always looks at me with sparkly eyes, so I wanted to make her look cute, so I drew her eyes like a girl's manga!

272
01:08:15,122 --> 01:08:21,122
- I drew her eyes sparkly, so I think I drew her eyes sparkly.

273
01:08:21,122 --> 01:08:25,122
- Also, I emphasized her glossy hair. - Oh, glossy hair...

274
01:08:25,122 --> 01:08:29,122
- That's really nice! - Yeah, your eyes are sparkly. - Thank you!

275
01:08:29,122 --> 01:08:33,122
- Okay, then... Oh!

276
01:08:33,122 --> 01:08:38,122
- Oh, then, Mentaiko-san, do you have anything you want to say at the end? - That's right...

277
01:08:38,122 --> 01:08:43,122
- After all, I want to eat 1.2kg of mentaiko before I go home.

278
01:08:43,122 --> 01:08:45,122
- I want to eat it before I go home. - Oh, really? - Yeah!

279
01:08:45,122 --> 01:08:47,930
- I'll do my best not to be a burden on my goal!

280
01:08:52,302 --> 01:08:56,302
So, I'd like to throw the moon cake to you and end the show!

281
01:08:56,302 --> 01:08:58,302
Okay, please step aside.

282
01:34:46,742 --> 01:34:49,282
Thank you very much!

283
01:34:53,782 --> 01:35:00,782
We'd like to make you even more excited, but the next song is really, really the last song!

284
01:35:01,782 --> 01:35:12,782
Yes, everyone, since this is the last day of the tour and of "Chihou," let's give it our all and make you even more excited!

285
01:38:44,822 --> 01:39:05,562
We didn't know if we could hold it or not, but I was very happy to be able to enjoy it with you like this, and the members said it many times, but this tour "Chihou" is the last in this Fukuoka park, so only two days of Kanagawa park will be left, so I hope you don't regret it!

286
01:39:11,670 --> 01:39:16,670
So it's the last one, so I'd like to say my last greetings with everyone!

287
01:40:22,326 --> 01:40:25,326
Let's say our last greetings with everyone!

288
01:45:33,526 --> 01:45:35,526
It's the last day of next month's training camp.

289
01:45:35,526 --> 01:45:37,526
That's right. It's early in the morning.

290
01:45:37,526 --> 01:45:41,378
It's only just begun, but it's already July.

291
01:45:52,094 --> 01:45:53,094
I'm looking forward to it.

292
01:45:53,094 --> 01:45:54,094
We'll do our best!

293
01:45:54,094 --> 01:45:55,094
Thank you for your hard work.

294
01:45:55,094 --> 01:45:56,094
Thank you for your hard work.

295
01:45:57,094 --> 01:46:03,094
It's my first time in Japan, and I'm happy that fans from Fukuoka are coming.

296
01:46:03,094 --> 01:46:06,914
Even though the weather is a little unstable, I'm happy that fans are coming.

297
01:46:11,710 --> 01:46:16,710
It was a very cute event, but I was very happy that fans were very happy.

298
01:46:17,710 --> 01:46:20,710
What do you want people to see at the end of next month?

299
01:46:21,710 --> 01:46:30,710
Since Kanagawa Prefecture is my hometown, I think I was able to show various sides of me.

300
01:46:31,710 --> 01:46:32,710
Thank you very much. I'm looking forward to it.

301
01:46:33,710 --> 01:46:34,710
Thank you very much.

302
01:46:35,710 --> 01:46:36,710
Thank you very much.

303
01:46:43,126 --> 01:46:58,126
I feel like I'm getting more and more powerful since my predecessors, and I think I'm getting more and more communication with my fans, so I'm very happy.

304
01:46:58,126 --> 01:47:08,126
Finally, it's finally the end of the month, but how do you spend it?

305
01:47:08,126 --> 01:47:34,126
That's right. In August, there were three performances in Osaka and Fukuoka, so I think everyone was able to do it without cutting off their feelings, but since it's been a month and the set list changes, we practiced together tightly for a month so that we wouldn't become one character again and grow again from where we grew up.

306
01:47:34,126 --> 01:47:46,126
As a 17-year-old, I won last year at the same set in Yokohama, so I hope I can show everyone how much I've grown.

307
01:47:46,126 --> 01:47:49,126
Thank you. Thank you for your hard work.

