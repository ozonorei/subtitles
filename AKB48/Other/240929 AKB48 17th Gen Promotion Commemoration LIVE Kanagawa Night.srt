1
00:00:09,122 --> 00:00:11,122
We are going to take a picture.

2
00:00:11,122 --> 00:00:13,122
Checking.

3
00:00:13,122 --> 00:00:15,122
Checking.

4
00:00:20,122 --> 00:00:22,122
Let's do it together.

5
00:00:22,122 --> 00:00:24,122
Let's do it together.

6
00:00:25,122 --> 00:00:27,122
Let's do it together.

7
00:00:27,122 --> 00:00:29,122
Ready, go.

8
00:00:30,122 --> 00:00:32,122
Let's do it together.

9
00:00:32,122 --> 00:00:34,122
Ready, go.

10
00:00:34,122 --> 00:00:36,122
Let's do it together.

11
00:00:36,122 --> 00:00:38,122
Ready, go.

12
00:01:36,442 --> 00:01:48,442
I've been doing lessons with Yuki-san and going on excursions. I don't know if that day will ever come. I'm so lonely.

13
00:01:48,442 --> 00:01:50,442
I'm lonely.

14
00:01:50,442 --> 00:01:51,442
I'm lonely.

15
00:01:51,442 --> 00:01:52,442
But I won't cry.

16
00:01:52,442 --> 00:01:53,442
But you were about to cry earlier.

17
00:01:53,442 --> 00:01:55,442
I'm really about to cry.

18
00:01:55,442 --> 00:01:56,442
Right?

19
00:01:56,442 --> 00:01:57,442
Right?

20
00:01:57,442 --> 00:01:59,442
I won't cry, I won't cry.

21
00:01:59,442 --> 00:02:07,442
The fans who came today, I won't be alone and I will definitely make you cry. Thank you.

22
00:02:07,442 --> 00:02:09,442
How will you make me cry?

23
00:02:09,442 --> 00:02:19,442
I think it will be my performance. I will show you my crazy performance and make you cry.

24
00:02:19,442 --> 00:02:23,442
Let's go to Sato Airi-san.

25
00:02:23,442 --> 00:02:26,442
What's with you today?

26
00:02:26,442 --> 00:02:36,442
Since yesterday's concert, there has been a lot of enthusiasm from the fans.

27
00:02:36,442 --> 00:02:45,442
Today, we will do our best so that we can get excited and the fans can say "AKB48 is okay from now on".

28
00:02:47,442 --> 00:02:51,442
I'm going to cry.

29
00:02:51,442 --> 00:02:53,442
You looked like you were about to cry.

30
00:02:53,442 --> 00:02:54,442
I'm looking forward to it.

31
00:02:54,442 --> 00:02:55,442
I'll say something.

32
00:02:55,442 --> 00:02:57,442
I'm looking forward to it.

33
00:02:57,442 --> 00:03:00,442
It was fun every time.

34
00:03:02,442 --> 00:03:04,442
I'm nervous.

35
00:03:04,442 --> 00:03:09,442
I'm nervous and all my nerves are going up now.

36
00:03:09,442 --> 00:03:10,442
Do you have nerves?

37
00:03:10,442 --> 00:03:11,442
I have nerves.

38
00:03:11,442 --> 00:03:16,442
The nerves of my thumb and so on are amazing.

39
00:03:16,442 --> 00:03:19,442
I'm nervous.

40
00:03:19,442 --> 00:03:21,442
But it's the last one.

41
00:03:21,442 --> 00:03:24,442
I have to do my best.

42
00:03:24,442 --> 00:03:28,442
How was the sound system?

43
00:03:28,442 --> 00:03:29,442
The engine?

44
00:03:29,442 --> 00:03:32,442
I was very impressed with the engine.

45
00:03:35,080 --> 00:03:40,080
In the end, Sarichan from Shiratori told me something very moving.

46
00:03:40,080 --> 00:03:43,080
Sarichan, you have been working hard for three years.

47
00:03:43,080 --> 00:03:45,080
Yes, I have.

48
00:03:45,080 --> 00:03:53,080
This period I spent with you was very important for me.

49
00:03:53,080 --> 00:03:54,080
Let's go!

50
00:03:54,080 --> 00:04:02,080
If you need evacuation, please follow the guidance of others and act calmly.

51
00:04:02,080 --> 00:04:06,080
Please pay attention to the noise in the seats.

52
00:04:13,080 --> 00:04:15,080
This is the last one.

53
00:04:20,080 --> 00:04:21,080
How do you feel?

54
00:04:23,080 --> 00:04:30,080
Now I am full of energy to show you what I have done so far.

55
00:04:30,080 --> 00:04:32,080
But I am worried about the second half.

56
00:04:32,080 --> 00:04:33,080
I might cry.

57
00:04:36,080 --> 00:04:40,080
Sarichan was about to cry in the engine.

58
00:04:40,080 --> 00:04:42,080
But I will do my best.

59
00:04:44,080 --> 00:04:45,080
I won't cry.

60
00:04:45,080 --> 00:04:47,080
I will be in the video.

61
00:04:47,080 --> 00:04:51,080
Let's have a great time with us.

62
00:04:51,080 --> 00:04:54,080
What should I do?

63
00:04:54,080 --> 00:04:56,080
It will be over soon.

64
00:04:56,080 --> 00:04:58,080
Please wait a little longer.

65
00:04:59,080 --> 00:05:00,080
I am about to cry.

66
00:05:00,080 --> 00:05:06,080
I have been about to cry since I was in the engine.

67
00:05:06,080 --> 00:05:18,080
But I will do my best to show you what I have done so far.

68
00:05:19,080 --> 00:05:20,080
I am looking forward to it.

69
00:05:20,080 --> 00:05:21,080
Thank you.

70
00:05:21,080 --> 00:05:22,080
I will do my best.

71
00:05:28,080 --> 00:05:30,080
Please pay attention to the noise in the seats.

72
00:05:30,080 --> 00:05:31,080
Thank you.

73
00:05:42,690 --> 00:05:44,690
This is a limited time remote corner.

74
00:05:44,690 --> 00:05:45,690
Hello.

75
00:05:45,690 --> 00:05:46,690
Good evening.

76
00:05:46,690 --> 00:05:49,690
How are you?

77
00:05:49,690 --> 00:05:51,690
This is the last day of the 19th season.

78
00:05:51,690 --> 00:05:53,690
Time flies.

79
00:05:53,690 --> 00:05:55,690
I was surprised.

80
00:05:55,690 --> 00:05:56,690
Thank you.

81
00:05:56,690 --> 00:06:01,690
You had the last day with your seniors of the 17th and 18th seasons.

82
00:06:01,690 --> 00:06:02,690
Yes.

83
00:06:02,690 --> 00:06:03,690
How was it?

84
00:06:03,690 --> 00:06:04,690
Did you learn a lot?

85
00:06:04,690 --> 00:06:11,690
Through this tour, I was able to get along with my seniors.

86
00:06:11,690 --> 00:06:17,690
I was able to gain experience and learn a lot from my seniors.

87
00:06:17,690 --> 00:06:23,690
I hope I can use this experience in various places in the future.

88
00:06:23,690 --> 00:06:25,690
How about you?

89
00:06:25,690 --> 00:06:32,690
The tour started as soon as I joined.

90
00:06:32,690 --> 00:06:38,690
I feel like I'm getting a rare experience.

91
00:06:38,690 --> 00:06:45,690
I hope I can use this experience in various places in the future.

92
00:06:45,690 --> 00:06:46,690
I'm happy.

93
00:06:46,690 --> 00:06:47,690
Are you feeling rock?

94
00:06:47,690 --> 00:06:48,690
I'm feeling rock.

95
00:06:48,690 --> 00:06:50,690
Rock is life.

96
00:06:50,690 --> 00:06:52,690
Life is rock.

97
00:06:52,690 --> 00:06:55,690
I'm feeling rock.

98
00:06:55,690 --> 00:06:59,690
I'm feeling rock.

99
00:06:59,690 --> 00:07:01,690
Did you hit your head last time?

100
00:07:01,690 --> 00:07:02,690
What?

101
00:07:02,690 --> 00:07:03,690
Did you hit your head last time?

102
00:07:03,690 --> 00:07:04,690
I remember it.

103
00:07:04,690 --> 00:07:06,690
It was captured by the camera.

104
00:07:06,690 --> 00:07:11,690
Where was it?

105
00:07:11,690 --> 00:07:12,690
Fukuoka.

106
00:07:12,690 --> 00:07:13,690
Fukuoka.

107
00:07:13,690 --> 00:07:14,690
It was low.

108
00:07:14,690 --> 00:07:15,690
Fukuoka.

109
00:07:15,690 --> 00:07:16,690
It's not low.

110
00:07:16,690 --> 00:07:19,690
There is a back road.

111
00:07:19,690 --> 00:07:24,690
That road is low.

112
00:07:24,690 --> 00:07:26,690
I thought it wasn't.

113
00:07:26,690 --> 00:07:28,690
It was captured by the camera.

114
00:07:28,690 --> 00:07:29,690
It's low.

115
00:07:29,690 --> 00:07:30,690
It's low.

116
00:07:30,690 --> 00:07:31,690
It's low.

117
00:07:31,690 --> 00:07:32,690
It's low.

118
00:07:32,690 --> 00:07:33,690
I'm sorry.

119
00:23:17,826 --> 00:23:22,670
We will succeed in this performance and we will do our best to move on to the next step.

120
00:23:33,762 --> 00:23:38,762
Once again, we would like you to see us in many different ways, so we have given you the photographer's seat.

121
00:23:38,762 --> 00:23:40,558
We would like you to take a group photo with us.

122
00:23:52,354 --> 00:23:54,734
We would like you to call out our names.

123
00:24:18,178 --> 00:24:24,622
The picture you just took is #AKB48. Please call out our names and call out our names more and more.

124
00:24:33,730 --> 00:24:38,010
Yes, with today's enthusiasm, I would like to introduce myself.

125
00:24:38,010 --> 00:24:40,130
Please start with Sari Shiratori.

126
00:24:40,130 --> 00:24:44,730
Yes, I am 19th grade research student Sari, also known as Sari Shiratori.

127
00:25:29,730 --> 00:25:33,198
Everyone, are you ready to have fun on the stage after this?

128
00:26:23,906 --> 00:26:26,382
So I'll do my best, too!

129
00:26:36,002 --> 00:26:39,002
I'm 17 years old, Eri-chan and Eriko Hashimoto!

130
00:26:44,098 --> 00:26:52,598
This is my last video, so I hope I can share a lot of happy tears with you and make you smile a lot!

131
00:26:52,598 --> 00:26:56,270
Thank you for having me today! Let's have some fun!

132
00:27:01,282 --> 00:27:03,282
I'm 20-year-old cute girl Yui Kawamura!

133
00:27:08,290 --> 00:27:13,038
Let's have a lot of fun together!

134
00:27:21,826 --> 00:27:24,654
I'm ITO MOMOKO and ITO MOMOKA from AKB48. Nice to meet you!

135
00:27:33,346 --> 00:27:39,986
I hope I can share with you what I've learned so far in this concert.

136
00:27:39,986 --> 00:27:42,414
I hope you can look up at the sky and say, "AKB48, thank you for everything you've done for us."

137
00:27:49,666 --> 00:27:51,666
I'm Kubo Hinano, a research student in 2018!

138
00:27:58,338 --> 00:28:04,238
I'm really sad, but I'm many times happier than I was when I was welcomed here today, so I hope you feel the same way!

139
00:29:09,218 --> 00:29:23,918
I don't know at all that today is the last day, but it's really the last day, so I'll remember each and every one of your faces and go home, so I'd be very happy if you could come and tell me your impressions again!

140
00:29:23,918 --> 00:29:26,894
Let's make it a memorable time for everyone! Thank you!

141
00:29:30,018 --> 00:29:35,018
I'm Yumemi and Asako Yumemi, and I'm an 18-year-old research student. Nice to meet you!

142
00:29:35,018 --> 00:29:37,018
Yumemi!

143
00:29:37,018 --> 00:29:49,018
Thank you! Today is the last day of the tour, and I'm really sad about it, but I'll make every minute and second count and have the best time with you! Thank you!

144
00:29:54,018 --> 00:29:56,018
I'm 17-year-old Hiroto Yuki!

145
00:29:56,018 --> 00:29:58,018
Yuki-chan!

146
00:30:00,018 --> 00:30:10,018
Today is the last day of the tour, and we want to make it a stage that we will never forget, so please shout out a lot so that you will never forget it!

147
00:30:10,018 --> 00:30:20,878
Let's have fun together so that today will be an unforgettable day for us and for you! Thank you!

148
00:30:25,794 --> 00:30:29,794
I'm 17 years old, Yuki-tan aka Yuki Ohto. Nice to meet you!

149
00:30:33,858 --> 00:30:42,858
I'm really happy to be able to make it to the final of the tour in my hometown of Kanagawa Prefecture, which is going to be one of my best memories of this summer.

150
00:30:42,858 --> 00:30:55,858
I'd be really happy if you could give it all you've got and come up with a great performance. Thank you!

151
00:31:03,906 --> 00:31:07,106
Hi! I'm 10th grade transfer student Kairiko and Kairi...

152
00:31:07,106 --> 00:31:09,406
I'm Kairiko and Kairi's little brother, Kairi!

153
00:31:09,406 --> 00:31:10,906
Nice to meet you!

154
00:31:15,042 --> 00:31:21,042
Finally, the day has come. I can really feel it now.

155
00:31:21,042 --> 00:31:27,042
Really, even now, I feel like I'm about to burst into tears.

156
00:31:27,042 --> 00:31:31,042
But, really, like...

157
00:31:31,042 --> 00:31:33,042
I'm in trouble.

158
00:31:33,042 --> 00:31:37,042
I'm really nervous, but I'm glad I could welcome you today.

159
00:31:37,042 --> 00:31:41,042
I'll do my best today to have fun with you all! Thank you!

160
00:31:41,042 --> 00:31:44,042
Yeah!

161
00:31:44,042 --> 00:31:48,042
I'm Kasumi of Kasuinkotalk and I'm a first-year research student!

162
00:31:52,386 --> 00:32:06,386
I'm sad that I can't continue my lessons with 17-year-olds like you, but I'll do my best to make today the best memory for you!

163
00:32:33,538 --> 00:32:47,538
When I started this tour, my goal was to say proudly that we are AKB48, which is the title of the tour, and that's why I decided to do this tour, but since it's the final today, I'd like to show you the culmination of it!

164
00:32:47,538 --> 00:32:50,538
From the front of the first floor to the back of the second floor!

165
00:32:58,562 --> 00:33:00,562
I'm a research student at AKB48, Narita Kohina!

166
00:33:00,562 --> 00:33:02,562
Kohiii!

167
00:33:03,562 --> 00:33:09,562
I've been thinking a lot about what I want to say at the end of the show, but I've already said everything I wanted to say.

168
00:33:09,562 --> 00:33:17,562
I laughed a little, but today is really the last time, so let's get together and get excited! Thank you!

169
00:33:42,066 --> 00:33:44,066
Yaaay!

170
00:33:46,066 --> 00:33:52,066
I'm Hanada Mei, and I'm a senior research student. Thank you for having me!

171
00:33:52,066 --> 00:33:54,066
Mei Mei!

172
00:33:56,066 --> 00:34:07,066
I was a little nervous and choked up, but I'm so happy to be able to be in this space with you and spend time with you through this stream!

173
00:34:07,066 --> 00:34:14,066
I want to make more and more precious memories with you in the future, so please continue to support me!

174
00:34:14,066 --> 00:34:16,066
Yaaay!

175
00:34:18,066 --> 00:34:21,066
Okay, let's get back to the performance!

176
00:34:21,066 --> 00:34:26,066
From here is a unit segment exclusive to Kanagawa Prefecture Park Day 2!

177
00:34:27,066 --> 00:34:29,066
Let's start with this song!

178
00:34:29,066 --> 00:34:30,066
One, two...

179
00:34:30,066 --> 00:34:32,066
Here you go!

180
00:50:01,066 --> 00:50:03,926
We've been taking lots of wonderful photos!

181
00:50:03,926 --> 00:50:04,630
And that's where...

182
00:50:15,306 --> 00:50:21,590
We're going to show you one of the favorite photos that each member chose from the photos posted on Twitter! Let's go!

183
00:50:32,570 --> 00:50:33,570
Cute!

184
00:50:33,570 --> 00:50:34,570
Nice!

185
00:50:34,570 --> 00:50:36,570
What's your favorite part of the photo?

186
00:50:36,570 --> 00:50:45,570
Yes, we received a lot of photos like this.

187
00:50:45,570 --> 00:50:47,570
All of them are precious to us.

188
00:50:47,570 --> 00:50:56,570
When the stars don't go away, we always wink at this part of the video.

189
00:50:56,570 --> 00:50:59,570
They captured that part perfectly.

190
00:50:59,570 --> 00:51:03,570
The light looks good, too.

191
00:51:03,570 --> 00:51:07,570
The light is shining and I look like I'm shining, too.

192
00:51:07,570 --> 00:51:11,570
I thought it was one of my favorites, so I chose it.

193
00:51:11,570 --> 00:51:20,570
I don't know if it's your favorite or not, but there's even a musical note at the end of the comment.

194
00:51:20,570 --> 00:51:22,570
Why do you say that?

195
00:51:22,570 --> 00:51:24,570
I thought it was your favorite.

196
00:51:24,570 --> 00:51:25,570
I don't like it.

197
00:51:25,570 --> 00:51:28,570
That's why you can do such a beautiful wink!

198
00:51:28,570 --> 00:51:29,570
You're a professional!

199
00:51:29,570 --> 00:51:32,570
You should try it, Yuichi!

200
00:51:32,570 --> 00:51:33,570
Who?

201
00:51:33,570 --> 00:51:34,570
The one in the middle.

202
00:51:34,570 --> 00:51:35,570
Can I do it?

203
00:51:35,570 --> 00:51:36,570
Yes, you can!

204
00:51:36,570 --> 00:51:37,570
Maybe I'm better than Airi.

205
00:51:37,570 --> 00:51:38,570
Yes, you can!

206
00:51:38,570 --> 00:51:39,570
Go for it!

207
00:51:42,570 --> 00:51:43,570
Nice!

208
00:51:43,570 --> 00:51:44,570
Cute!

209
00:51:44,570 --> 00:51:47,570
Shall we move on?

210
00:51:47,570 --> 00:51:52,570
The next one is chosen by Kawamara Yui!

211
00:51:52,570 --> 00:51:53,310
Here it is!

212
00:51:59,194 --> 00:52:01,194
This one is not good.

213
00:52:03,194 --> 00:52:05,194
Why did you choose this one?

214
00:52:10,194 --> 00:52:17,194
This picture was taken during the song "オオカミとプライド"

215
00:52:17,194 --> 00:52:25,194
Meihanado and I performed this song together.

216
00:52:25,194 --> 00:52:35,194
This picture was taken during "思い出に残ってる人の中で"

217
00:52:35,194 --> 00:52:38,194
That's why I chose this one.

218
00:52:38,194 --> 00:52:40,194
That's not fair.

219
00:52:40,194 --> 00:52:44,194
Because everyone likes Kawaii-chan.

220
00:52:44,194 --> 00:52:46,194
She's really cute!

221
00:52:47,194 --> 00:52:50,194
The next one is chosen by Yamaguchi Yui!

222
00:52:50,194 --> 00:52:52,194
Here it is!

223
00:52:54,194 --> 00:52:57,194
This one is also dynamic.

224
00:52:57,194 --> 00:53:00,194
Her hair is long.

225
00:53:00,194 --> 00:53:02,194
Everyone, look at this.

226
00:53:02,194 --> 00:53:07,194
I cut my hair, so it's not interesting.

227
00:53:07,194 --> 00:53:11,194
But I always had long hair.

228
00:53:11,194 --> 00:53:14,194
The reason I chose this picture is...

229
00:53:14,194 --> 00:53:23,194
If you look at my ponytail, you can see how dynamic it is.

230
00:53:23,194 --> 00:53:25,194
Can you see it, everyone?

231
00:53:25,194 --> 00:53:28,194
I'm sure you can see it.

232
00:53:28,194 --> 00:53:31,194
It's like climbing a love tree.

233
00:53:31,194 --> 00:53:33,194
Like a love tree?

234
00:53:33,194 --> 00:53:35,194
I don't know what you're talking about.

235
00:53:35,194 --> 00:53:37,194
I don't know what you're talking about, but it's a love tree.

236
00:53:37,194 --> 00:53:38,194
It's red, isn't it?

237
00:53:38,194 --> 00:53:40,194
Yes, it's red.

238
00:53:41,194 --> 00:53:46,194
The next one is chosen by me!

239
00:53:46,194 --> 00:53:47,934
Here it is!

240
00:53:56,690 --> 00:54:01,430
It's hard to describe, but it's kind of funny.

241
00:54:01,430 --> 00:54:03,690
You're next to me, Jiwan.

242
00:54:03,690 --> 00:54:07,530
It's like me holding myself.

243
00:54:07,530 --> 00:54:13,570
It's something I do every time I do #好きなんだ.

244
00:54:13,570 --> 00:54:19,650
I got a lot of pictures of me in every concert, and I chose one of them.

245
00:54:19,650 --> 00:54:25,450
Please look for me holding myself.

246
00:54:25,450 --> 00:54:28,910
It's like you're an adult holding yourself.

247
00:54:28,910 --> 00:54:33,050
It's like me, holding myself.

248
00:54:33,050 --> 00:54:38,130
The next one is chosen by Hinata Yuki.

249
00:54:38,130 --> 00:54:40,490
Wow!

250
00:54:40,490 --> 00:54:43,930
First of all, you have such a cute face.

251
00:54:43,930 --> 00:54:49,130
The reason for this is funny, and it's because you're the cutest!

252
00:54:49,130 --> 00:54:50,470
That's it!

253
00:54:50,470 --> 00:54:53,610
I said it because you're the cutest.

254
00:54:53,670 --> 00:54:55,670
You're really cute.

255
00:54:55,670 --> 00:54:57,670
What is it? A rabbit?

256
00:54:57,670 --> 00:54:59,670
I think it's a rabbit.

257
00:54:59,670 --> 00:55:01,670
It's really cute.

258
00:55:01,670 --> 00:55:03,670
Doesn't it have a puffy face?

259
00:55:03,670 --> 00:55:05,670
I don't usually do that.

260
00:55:05,670 --> 00:55:07,670
That's true.

261
00:55:07,670 --> 00:55:09,670
It's kind of funny.

262
00:55:09,670 --> 00:55:13,290
The last one is a picture of Sakuri Mimi.

263
00:55:13,290 --> 00:55:15,290
Here it is!

264
00:55:15,290 --> 00:55:17,290
Wow!

265
00:55:17,290 --> 00:55:19,290
Look over there!

266
00:55:19,290 --> 00:55:21,290
Isn't she too cute?

267
00:55:21,290 --> 00:55:23,290
She's so cute!

268
00:55:23,290 --> 00:55:25,290
The reason for this is...

269
00:55:25,290 --> 00:55:29,290
I ran into Haruna Kojima when I was doing "Oni Suppin".

270
00:55:29,290 --> 00:55:35,290
She asked me if I had any leaked photos, and I showed her the photos she took of me.

271
00:55:35,290 --> 00:55:37,290
I think it's a memory for me.

272
00:55:37,290 --> 00:55:39,290
That's an amazing reason!

273
00:55:39,290 --> 00:55:41,290
Wow!

274
00:55:41,290 --> 00:55:43,290
I think it's a chime.

275
00:55:43,290 --> 00:55:45,290
I think we're ready.

276
00:55:45,290 --> 00:55:50,290
Other than what we announced today, there were a lot of really nice photos.

277
00:55:50,290 --> 00:55:52,290
Photographers, thank you!

278
00:55:52,290 --> 00:55:54,046
Thank you!

279
00:55:54,418 --> 00:55:59,998
Okay, let's get back to the performance. Everyone, please stand up!

280
01:12:11,514 --> 01:12:13,514
Second one!

281
01:12:45,242 --> 01:12:47,242
How do you like it?

282
01:12:48,242 --> 01:12:50,242
It's fun!

283
01:12:50,242 --> 01:12:51,242
It's fun!

284
01:12:51,242 --> 01:12:52,242
It's really fun!

285
01:12:57,826 --> 01:12:59,826
This has evolved.

286
01:12:59,826 --> 01:13:02,826
It's true! It's changed a little.

287
01:13:02,826 --> 01:13:04,826
It was cardboard but it's colored now.

288
01:13:04,826 --> 01:13:06,470
I see!

289
01:13:10,562 --> 01:13:12,562
We have an announcement to make!

290
01:13:12,562 --> 01:13:16,198
We, 19th grade researchers, will be opening an Instagram account!

291
01:13:20,314 --> 01:13:23,174
We'll be updating a lot, so please follow us!

292
01:13:23,174 --> 01:13:25,174
Thank you!

293
01:13:30,650 --> 01:13:39,450
As MCs here, we've written down the most memorable memories from this tour, so we'd like to announce them!

294
01:13:39,450 --> 01:13:41,450
Starting with Kairi Okumoto!

295
01:13:41,450 --> 01:13:41,990
Yes, thank you!

296
01:13:45,498 --> 01:13:49,498
Kairi Okumoto's most memorable memory is...

297
01:13:49,498 --> 01:13:53,498
Goto Uchigurume!

298
01:13:53,498 --> 01:13:57,498
We'll tell you more about it once everyone's announced their memories!

299
01:13:57,498 --> 01:13:59,498
Next up is Yui Kawamura!

300
01:13:59,498 --> 01:14:01,498
My most memorable memory with Yui Kawamura is...

301
01:14:01,498 --> 01:14:05,498
Her two-shot with our seniors!

302
01:14:05,498 --> 01:14:07,498
She's so cute!

303
01:14:07,498 --> 01:14:09,498
She's really good at drawing!

304
01:14:09,498 --> 01:14:11,498
She's really good!

305
01:14:11,498 --> 01:14:13,498
My most memorable memory with Momoka Ito is...

306
01:14:13,498 --> 01:14:14,342
Ta-da!

307
01:14:23,578 --> 01:14:27,078
For me, Shiratori Sari's tour memories are...

308
01:14:27,078 --> 01:14:29,078
Units from each region!

309
01:14:34,330 --> 01:14:37,330
For me, Hanada Mei's best memory is...

310
01:14:48,514 --> 01:14:50,514
Okumoto Kairi-chan, why did you choose that?

311
01:14:50,514 --> 01:14:54,514
I think I've written a few poems before.

312
01:14:54,514 --> 01:14:56,514
And I think I wrote "rice" many times.

313
01:14:56,514 --> 01:14:59,514
I love eating that much.

314
01:14:59,514 --> 01:15:00,514
Especially...

315
01:15:01,514 --> 01:15:02,514
Oops, my nose is...

316
01:15:02,514 --> 01:15:03,514
Give me your position.

317
01:15:03,514 --> 01:15:06,514
And among them, my best memories are...

318
01:15:06,514 --> 01:15:09,514
"tebasaki" and "zundao mochi".

319
01:15:09,514 --> 01:15:10,514
How can I say...

320
01:15:10,514 --> 01:15:12,514
I love eating.

321
01:15:12,514 --> 01:15:14,514
That's why I chose this poem.

322
01:15:14,514 --> 01:15:16,514
That's nice.

323
01:15:16,514 --> 01:15:18,514
I'm glad you chose "zundao mochi".

324
01:15:18,514 --> 01:15:20,514
"himatsubushi" was good, too.

325
01:15:20,514 --> 01:15:21,514
"himatsubushi"?

326
01:15:21,514 --> 01:15:23,514
"hitsuma", "hitsuma"!

327
01:15:23,514 --> 01:15:24,514
"hitsuma" and "hitsuma"!

328
01:15:24,514 --> 01:15:25,514
"hitsuma" and "hitsuma"!

329
01:15:25,514 --> 01:15:26,514
Okay, then...

330
01:15:26,514 --> 01:15:27,514
Kawaii-chan.

331
01:15:27,514 --> 01:15:28,514
Okay.

332
01:15:28,514 --> 01:15:31,514
At first, my goal for the tour was...

333
01:15:31,514 --> 01:15:37,514
to take a picture with all of you.

334
01:15:37,514 --> 01:15:38,514
But...

335
01:15:38,514 --> 01:15:39,514
Oh!

336
01:15:39,514 --> 01:15:40,514
I was in a pinch.

337
01:15:40,514 --> 01:15:41,514
Pinch!

338
01:15:41,514 --> 01:15:42,514
Pinch!

339
01:15:42,514 --> 01:15:51,514
I was thinking of not telling you that we should take a picture together.

340
01:15:51,514 --> 01:15:52,514
So I chose this.

341
01:15:52,514 --> 01:15:54,514
Kawaii!

342
01:15:54,514 --> 01:15:56,514
I hope you can do it.

343
01:15:56,514 --> 01:15:57,514
Let's do it!

344
01:15:57,514 --> 01:15:58,514
Still...

345
01:15:58,514 --> 01:15:59,514
Still...

346
01:15:59,514 --> 01:16:00,514
Still...

347
01:16:00,514 --> 01:16:01,514
Still...

348
01:16:01,514 --> 01:16:02,514
Still...

349
01:16:02,514 --> 01:16:03,514
Still...

350
01:16:03,514 --> 01:16:04,514
Still...

351
01:16:04,514 --> 01:16:05,514
Still...

352
01:16:05,514 --> 01:16:06,514
Still...

353
01:16:06,514 --> 01:16:07,514
Still...

354
01:16:07,514 --> 01:16:08,514
Still...

355
01:16:08,514 --> 01:16:09,514
Still...

356
01:16:09,514 --> 01:16:10,514
Still...

357
01:16:10,514 --> 01:16:11,514
Still...

358
01:16:11,514 --> 01:16:12,514
Still...

359
01:16:12,514 --> 01:16:13,514
Still...

360
01:16:13,514 --> 01:16:14,514
Still...

361
01:16:14,514 --> 01:16:15,514
Still...

362
01:16:15,514 --> 01:16:16,514
Still...

363
01:16:16,514 --> 01:16:17,514
Still...

364
01:16:17,514 --> 01:16:18,514
Still...

365
01:16:18,514 --> 01:16:19,514
Still...

366
01:16:19,514 --> 01:16:20,514
Still...

367
01:16:20,514 --> 01:16:21,514
Still...

368
01:16:21,514 --> 01:16:22,514
Still...

369
01:16:22,514 --> 01:16:23,514
Still...

370
01:16:23,514 --> 01:16:24,514
Still...

371
01:16:24,514 --> 01:16:25,514
Still...

372
01:16:25,514 --> 01:16:26,514
Still...

373
01:16:26,514 --> 01:16:27,514
Still...

374
01:16:27,514 --> 01:16:28,514
Still...

375
01:16:28,514 --> 01:16:29,514
Still...

376
01:16:29,514 --> 01:16:30,514
Still...

377
01:16:30,514 --> 01:16:31,514
Still...

378
01:16:31,514 --> 01:16:32,514
Still...

379
01:16:32,514 --> 01:16:33,514
Still...

380
01:16:33,514 --> 01:16:34,514
Still...

381
01:16:34,514 --> 01:16:35,514
Still...

382
01:16:35,514 --> 01:16:36,514
Still...

383
01:16:36,514 --> 01:16:37,514
Still...

384
01:16:37,514 --> 01:16:38,514
Still...

385
01:16:38,514 --> 01:16:39,514
Still...

386
01:16:39,514 --> 01:16:40,514
Still...

387
01:16:40,514 --> 01:16:45,514
I was so happy to be able to do this on this tour, so I wrote this.

388
01:16:46,968 --> 01:16:47,968
It's an idol's dream, isn't it?

389
01:16:47,968 --> 01:16:48,968
Dream!

390
01:16:48,968 --> 01:16:49,968
I know, I know.

391
01:16:49,968 --> 01:16:51,968
Okay, what about you, Mei Mei?

392
01:16:51,968 --> 01:17:01,968
For me, Hanada Mei, it was one of my goals to meet fans one by one through this tour.

393
01:17:01,968 --> 01:17:06,968
And since it came true, I wrote this picture.

394
01:17:06,968 --> 01:17:09,968
I'm so happy! Thank you so much!

395
01:17:10,968 --> 01:17:13,968
Okay, we're ready for you now!

396
01:17:13,968 --> 01:17:16,968
I'm going to throw my sketchbook at you and end the show!

397
01:17:17,968 --> 01:17:18,968
Okay, spread out.

398
01:17:19,968 --> 01:17:21,968
I'm looking for flowers at the store.

399
01:17:27,994 --> 01:17:29,994
I found one!

400
01:17:29,994 --> 01:17:32,994
Don't throw flowers, Dora!

401
01:17:32,994 --> 01:17:34,994
I'll throw this one.

402
01:17:34,994 --> 01:17:35,994
Did it spread?

403
01:17:35,994 --> 01:17:37,994
No, it didn't.

404
01:17:37,994 --> 01:17:38,994
Here we go!

405
01:17:38,994 --> 01:17:39,558
Ready, set...

406
01:17:48,698 --> 01:17:52,038
Here we go with the next song! Ready, set...

407
01:48:15,914 --> 01:48:21,398
It's still exciting, but the next song is really, really the last song.

408
01:48:29,130 --> 01:48:33,630
It's really sad that it's already over, but are you all sad too?

409
01:48:33,630 --> 01:48:35,630
We're sad!

410
01:48:35,630 --> 01:48:39,630
If Eriko plays a song, we'll move on to the next song.

411
01:48:43,978 --> 01:48:45,978
Then you don't have to pretend you know the song.

412
01:49:01,714 --> 01:49:05,714
I hope we can have a tour with 17th, 18th, and 19th generation someday.

413
01:49:05,714 --> 01:49:07,714
I hope we can do it again.

414
01:49:07,714 --> 01:49:09,714
Maybe we have more people now.

415
01:49:09,714 --> 01:49:14,714
I hope we can have a tour together again.

416
01:49:14,714 --> 01:49:21,714
Today, let's have fun with a smile and give it all we've got.

417
01:49:24,714 --> 01:49:27,714
Then please listen to the last song.

418
01:49:27,714 --> 01:49:29,714
Girls!

419
01:49:29,714 --> 01:49:30,550
Yeah!

420
01:54:43,434 --> 01:54:48,534
I will do my best to show you my growth and growth again.

421
01:55:09,066 --> 01:55:12,066
Let's do our best to become more and more famous!

422
01:57:57,818 --> 01:58:00,818
When we were still in college, we couldn't really do it like we do now.

423
01:58:00,818 --> 01:58:06,818
So we're going to look back on it and try to do it better.

424
01:58:15,818 --> 01:58:19,818
Everyone! Let's do our best to become more and more famous!

425
01:58:19,818 --> 01:58:21,502
Cheers!

426
01:59:01,114 --> 01:59:03,114
It was really fun to do it.

427
01:59:03,114 --> 01:59:09,114
I hope we can gradually make better things together.

428
01:59:09,114 --> 01:59:17,114
Even though it was our first tour, we had a lot of fans coming and giving us a lot of voices.

429
01:59:17,114 --> 01:59:20,114
It was really fun to get excited together.

430
01:59:20,114 --> 01:59:28,114
I think we're the only ones who can show our hard work and freshness now.

431
01:59:28,114 --> 01:59:33,114
I don't have any worries, but I want to become more famous and do my best.

432
02:02:59,378 --> 02:03:07,178
I think I was able to run until the last day thanks to the staff and the fans who support me.

433
02:03:07,178 --> 02:03:14,938
I think we were able to show you a little bit of how we have grown through this greeting circle.

434
02:03:14,938 --> 02:03:29,338
We are still immature, but we will do our best to aim higher and have a bigger concert at a larger venue, so please support us as much as you can.

435
02:03:29,338 --> 02:03:41,338
And we want more and more people to know about AKB48, and we think that AKB48 is really the best right now!

436
02:03:41,338 --> 02:03:43,966
Thank you so much for the new title!

437
02:03:47,506 --> 02:04:10,326
Yes, the newly renewed AKB48 theater will be renewed, but in order for us AKB48 to be more and more excited, we 17th generation, 18th generation researchers, and 19th generation researchers will do our best to create a new era!

438
02:07:00,466 --> 02:07:05,022
In addition, photos of AKB48 will be on sale, so as a commemoration of today, we will take a commemorative photo.

439
02:08:06,234 --> 02:08:07,234
Yamaguchi-chan.

440
02:08:07,234 --> 02:08:08,234
Saepi.

441
02:08:08,234 --> 02:08:09,234
Thank you for your hard work.

442
02:08:09,234 --> 02:08:10,234
You cut your hair, didn't you?

443
02:08:10,234 --> 02:08:11,234
Shiko cut her hair.

444
02:08:11,234 --> 02:08:12,234
Cute.

445
02:08:12,234 --> 02:08:13,234
It's over.

446
02:08:13,234 --> 02:08:14,234
What?

447
02:08:14,234 --> 02:08:16,234
It's over. It's over.

448
02:08:16,234 --> 02:08:17,234
It's over.

449
02:08:17,234 --> 02:08:18,234
It's over.

450
02:08:18,234 --> 02:08:19,234
Why did it end?

451
02:08:19,234 --> 02:08:20,234
What? There's more.

452
02:08:20,234 --> 02:08:23,234
I didn't want it to end, but...

453
02:08:23,234 --> 02:08:25,234
But it's really a sense of accomplishment.

454
02:08:25,234 --> 02:08:26,234
It's amazing, isn't it?

455
02:08:26,234 --> 02:08:27,234
Let's go!

456
02:08:27,234 --> 02:08:28,234
To the top of the second floor.

457
02:08:28,234 --> 02:08:29,234
I saw it!

458
02:08:29,234 --> 02:08:30,234
I saw it!

459
02:08:30,234 --> 02:08:31,234
I saw it!

460
02:08:31,234 --> 02:08:32,234
I didn't want it to end.

461
02:08:32,234 --> 02:08:33,234
It was really fun.

462
02:08:33,234 --> 02:08:34,234
Good memories.

463
02:08:35,234 --> 02:08:36,234
Junanaki-san, thank you very much.

464
02:08:36,234 --> 02:08:37,234
Thank you.

465
02:08:37,234 --> 02:08:38,234
Congratulations.

466
02:08:38,234 --> 02:08:39,234
Congratulations.

467
02:08:39,234 --> 02:08:40,234
Thank you very much.

468
02:08:40,234 --> 02:08:41,234
Thank you very much.

469
02:08:41,234 --> 02:08:42,234
How was it today?

470
02:08:42,234 --> 02:08:43,234
What?

471
02:08:43,234 --> 02:08:44,234
What?

472
02:08:44,234 --> 02:08:45,234
You just said it, didn't you?

473
02:08:45,234 --> 02:08:46,234
You just said it, didn't you?

474
02:08:48,234 --> 02:08:49,234
Cheers!

475
02:08:50,234 --> 02:08:51,234
Bye-bye.

476
02:08:51,234 --> 02:08:52,234
Thank you very much.

477
02:08:53,234 --> 02:09:00,234
I got to know Junanaki-san, Juhachiki-san and Isedouki-san better, and I think I was able to scale up more.

478
02:09:00,234 --> 02:09:12,234
I've seen my seniors shine so brightly up close, so I want to work harder than I usually do so that I can be like them.

479
02:09:12,234 --> 02:09:17,234
I want to work hard so that I can tour again someday.

480
02:09:17,234 --> 02:09:24,234
I think I was able to make good memories not only for them but for me as well.

481
02:09:24,234 --> 02:09:28,234
I want to work harder so that I can get closer to my seniors in the future.

482
02:09:28,234 --> 02:09:33,234
I want to work harder so that I can get closer to my seniors in the future.

483
02:09:33,234 --> 02:09:40,234
I've been watching Junanaki-san shine from behind, and I've been thinking about how nice it is to shine.

484
02:09:40,234 --> 02:09:45,234
Our goal is to shine someday, and we're still working on it.

485
02:09:45,234 --> 02:09:49,234
I hope we all shine and tour again.

486
02:09:49,234 --> 02:09:50,234
I'll do my best.

487
02:09:50,234 --> 02:09:59,234
Every venue was so passionate, and I was able to grow a lot by performing in 12 different venues.

488
02:09:59,234 --> 02:10:04,234
I think it's a great venue where we all grew up.

489
02:10:05,456 --> 02:10:16,456
It's sad that the tour is over, but it's not all over, so I'd like to do my best to grow up more and more with this experience.

490
02:10:17,456 --> 02:10:24,456
As we did it more and more, we wanted to liven up with Juhatsuki and Jukyuki-chan.

491
02:10:25,456 --> 02:10:32,456
I think we were able to give a performance suitable for the name of the tour, "We are AKB48". It was really fun.

492
02:10:32,456 --> 02:10:44,456
I'm full of lonely feelings, but we'll do our best every day to create more opportunities like this and promise you we'll see you again.

493
02:10:45,456 --> 02:10:56,456
I'd like you to see our stage with your fans like this again, and I'd like to do my best to keep AKB48 going.

494
02:10:56,456 --> 02:11:07,456
Looking back, I think it was a very brief moment, and I'd like to do my best to do our first anniversary tour in the same way in the future.

495
02:11:08,456 --> 02:11:25,456
It was very short and long, but it's over in no time, and I'm full of lonely feelings that this time will be gone, but I'm full of feelings that we did our best.

496
02:11:26,456 --> 02:11:27,456
It was really fun.

497
02:11:28,456 --> 02:11:34,456
I thought it would be nice if I could grow up with everyone.

498
02:11:35,456 --> 02:11:45,456
I think I'll remember this moment until I die, and I'm sure I'll remember it when I die. I'm crying.

499
02:11:45,456 --> 02:11:57,456
I thought it would be nice if I could have this opportunity to connect with you next time, so I hope I can continue to be with you and your fans.

500
02:11:57,544 --> 02:12:00,824
I hope I can continue to be a person who can excite you.

501
02:12:00,824 --> 02:12:04,824
I hope I can improve if I repeat anything.

502
02:12:04,824 --> 02:12:09,824
I hope I can have more things that I think are fun.

503
02:12:09,824 --> 02:12:15,824
I hope I can repeat a lot of things I'm not good at.

504
02:12:15,824 --> 02:12:18,824
September is over in a flash.

505
02:12:18,824 --> 02:12:23,824
I'm really sad and happy that the tour is over.

506
02:12:24,104 --> 02:12:29,104
It's complicated, but I'm happy.

507
02:12:29,104 --> 02:12:35,104
I want to grow more and more and be liked by more and more people.

508
02:12:35,104 --> 02:12:40,104
I've never done a cool song, but it's my first time doing it on this tour.

509
02:12:40,104 --> 02:12:52,104
I've been able to do a lot of songs, so I hope people who don't know AKB will see more and more performances like that.

510
02:12:52,384 --> 02:13:01,384
I hope next year and this summer will be as fun as this year.

511
02:13:01,384 --> 02:13:03,384
I want to tour again.

512
02:13:03,384 --> 02:13:07,384
I've been looking forward to these last two days.

513
02:13:07,384 --> 02:13:19,384
It's over and I feel lonely, but I want to show you that we're going to make you feel better.

514
02:13:19,664 --> 02:13:25,664
There were fans who didn't know us, but there were fans who wondered if we were okay.

515
02:13:25,664 --> 02:13:38,664
I started with a lot of feelings, but I was able to have fun with you until the end and spend a wonderful time with you.

516
02:13:38,664 --> 02:13:44,664
It was a really good experience and I think it helped me grow.

517
02:13:44,664 --> 02:13:47,664
It was a lot of fun.

518
02:13:47,944 --> 02:13:50,944
I had a lot of fun on this tour.

519
02:13:50,944 --> 02:13:56,944
I was able to meet the fans and only go to Kanagawa Park.

520
02:13:58,320 --> 02:14:03,320
I was able to walk around and get closer to the fans, so it was a lot of fun.

521
02:14:04,320 --> 02:14:11,320
The whole performance ended today, and I think it was a great concert thanks to the fans.

522
02:14:12,320 --> 02:14:23,320
I was able to tell the fans that AKB48 exists in rural areas and many other places, so I was very happy.

523
02:14:23,320 --> 02:14:40,320
We will continue to hold concerts in large venues and let more people know about AKB48, so please continue to support us!

524
02:14:41,320 --> 02:14:42,320
See you!

