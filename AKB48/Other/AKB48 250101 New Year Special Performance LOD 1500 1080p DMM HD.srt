1
00:00:15,554 --> 00:00:19,554
Hello everyone! I'm AKB48's Yagi Azuki!

2
00:00:22,554 --> 00:00:28,554
Thank you for coming to AKB48's Gekijou Ganjitsu Kouen today!

3
00:00:29,554 --> 00:00:32,554
I would like to make a few requests before we start.

4
00:00:32,554 --> 00:00:38,554
Please do not stand on chairs or move seats during the show,

5
00:00:38,554 --> 00:00:43,554
as it is dangerous and will be a nuisance for other guests.

6
00:00:44,554 --> 00:00:49,554
It is prohibited to bring cameras and recording devices with you.

7
00:00:49,554 --> 00:00:52,554
If you have one, please leave it with us.

8
00:00:53,554 --> 00:00:57,554
Also, please make sure you put your mobile phones in manual mode.

9
00:00:58,554 --> 00:01:00,554
The show will be held indoors.

10
00:01:00,554 --> 00:01:04,554
Due to the circumstances of today's show, we will remove the emergency lights in the audience seats.

11
00:01:04,554 --> 00:01:08,554
When the show starts, we will direct you to the shops and staff,

12
00:01:08,554 --> 00:01:11,554
but please make sure you check the exits in advance.

13
00:01:16,546 --> 00:01:24,546
This is our first time performing in a theater. We will do our best, so please have fun!

14
00:01:26,546 --> 00:01:30,546
This has been AKB48's Yagiazuki!

15
00:09:20,333 --> 00:09:24,601
This year, our research students will be hosting the first theater performance of the year.

16
00:09:38,893 --> 00:09:42,841
Not only us, but 20th generation has also joined in!

17
00:10:03,085 --> 00:10:05,085
16 members have joined in!

18
00:10:05,085 --> 00:10:07,085
Oh, but speaking of which...

19
00:10:07,085 --> 00:10:09,085
This year we have...

20
00:10:09,085 --> 00:10:09,273
Heavy Metal!

21
00:10:14,189 --> 00:10:17,189
As you can see, we AKB48 are moving fast...

22
00:10:18,189 --> 00:10:20,189
What did you say? We are moving fast!

23
00:10:24,877 --> 00:10:33,877
We will move fast and aim for the top!

24
00:10:33,877 --> 00:10:39,877
I will introduce myself along with my self-introduction!

25
00:11:09,101 --> 00:11:17,101
Last year, I turned 20 years old and I wanted to be an independent woman, but this year I want to have fun.

26
00:11:17,101 --> 00:11:25,101
I have the image of laughing a lot, but when I'm in a good mood, I smile a lot.

27
00:11:25,101 --> 00:11:31,101
I hope fans will see me like that! Let's have fun today! Thank you!

28
00:11:31,101 --> 00:11:45,101
Can you introduce me? Please look forward to it!

29
00:11:45,101 --> 00:11:51,101
I'm Yui Yamaguchi and I'm 18 years old.

30
00:12:14,029 --> 00:12:27,029
I want to do my best and show my growth in front of my fans.

31
00:12:27,029 --> 00:12:32,029
And I want you to feel my love for you.

32
00:12:54,797 --> 00:12:57,401
My writing is like this.

33
00:13:12,941 --> 00:13:21,941
This word has a meaning of cooperating with each other and creating an unfavorable situation or going through a new year.

34
00:13:21,941 --> 00:13:33,941
This year is AKB48's 20th anniversary year, so I made this four-lettered language with the hope of working with all members and fans and moving forward for a year.

35
00:13:33,941 --> 00:13:36,889
Thank you for your cooperation this year as well!

36
00:13:53,485 --> 00:13:56,249
Yes! I will go with my writing! One, two!

37
00:14:00,469 --> 00:14:02,469
I wrote "strong will".

38
00:14:02,469 --> 00:14:05,469
Thank you.

39
00:14:07,469 --> 00:14:13,469
I think my will was weak in 2020.

40
00:14:13,469 --> 00:14:14,469
Really?

41
00:14:14,469 --> 00:14:23,469
I heard that I will be 20 years old in 2025.

42
00:14:25,469 --> 00:14:28,469
But I was born in February.

43
00:14:28,469 --> 00:14:30,469
It's next year!

44
00:14:30,469 --> 00:14:32,469
It's not yet!

45
00:14:39,093 --> 00:14:41,093
I'm already 20 years old.

46
00:14:41,093 --> 00:14:43,093
Isn't your will weak? Are you OK?

47
00:14:47,093 --> 00:14:55,093
We made this word "strong will" so that we can make a lot of wonderful goals for you.

48
00:14:55,093 --> 00:14:58,093
We will do our best this year as well. Thank you.

49
00:15:00,093 --> 00:15:02,093
OK, let's go.

50
00:15:02,093 --> 00:15:03,093
Azu.

51
00:15:03,093 --> 00:15:04,093
Azu.

52
00:15:04,093 --> 00:15:06,093
Azu's order is...

53
00:15:06,093 --> 00:15:07,093
This one?

54
00:15:07,093 --> 00:15:09,093
Azu, what is it?

55
00:15:09,093 --> 00:15:11,093
Azu's order is...

56
00:15:17,933 --> 00:15:20,345
18th grade research student Azu.

57
00:15:24,821 --> 00:15:25,821
Thank you very much.

58
00:15:25,821 --> 00:15:28,821
My ambition for this year is...

59
00:15:39,743 --> 00:15:42,743
I don't mind if it's a bad song, but in that sense...

60
00:15:42,743 --> 00:15:44,743
It's difficult, isn't it?

61
00:15:44,743 --> 00:15:46,743
It has that meaning, but...

62
00:15:46,743 --> 00:15:49,743
I want to show it in various places...

63
00:15:49,743 --> 00:15:52,743
"Wow, she's funny. She has this kind of expression."

64
00:15:52,743 --> 00:15:56,743
I want to make this year a year where people will think like that!

65
00:15:56,743 --> 00:15:59,276
Everyone, please smile a lot!

66
00:16:02,656 --> 00:16:04,656
I'm glad you could laugh it off.

67
00:16:04,656 --> 00:16:05,656
I'm glad!

68
00:16:05,656 --> 00:16:07,656
Everyone is so kind!

69
00:16:08,656 --> 00:16:10,656
Then I will also go.

70
00:16:11,656 --> 00:16:14,656
I'm Judge Researcher Akiyama Ena.

71
00:16:14,656 --> 00:16:17,656
This is what I wrote.

72
00:16:17,656 --> 00:16:18,656
Ta-dah!

73
00:16:28,072 --> 00:16:36,072
It says "the strongest smile". I want to do my best with the strongest smile I always say.

74
00:16:36,072 --> 00:16:41,072
Actually, I wrote some difficult words last year.

75
00:16:41,072 --> 00:16:45,072
But I think this is the most important for me.

76
00:16:45,072 --> 00:16:50,072
If I don't say this, you may think I'm really tired.

77
00:16:52,072 --> 00:16:55,072
I'm a person who laughs forever.

78
00:16:55,072 --> 00:16:59,072
So I want to laugh a lot this year and make good memories with you. Thank you.

79
00:17:23,807 --> 00:17:41,308
And I found out that "kinotomi no toshi" and "kinotomi" means to work hard one by one no matter what happens.

80
00:17:41,308 --> 00:17:50,708
So I wrote this word because I want to work hard one by one this year and make good memories with you.

81
00:17:50,708 --> 00:17:52,708
I'll do my best. Thank you.

82
00:17:56,800 --> 00:18:01,800
Wow, you're different from funny people.

83
00:18:01,800 --> 00:18:05,800
I thought about it very seriously.

84
00:18:05,800 --> 00:18:07,800
Yes, and...

85
00:18:13,888 --> 00:18:15,888
My goal for this year is...

86
00:18:23,264 --> 00:18:27,264
I couldn't do it last year, so I want to bungee jump this year.

87
00:18:27,264 --> 00:18:36,264
But before I do it, I want to break my shell and grow up like a snake.

88
00:18:36,264 --> 00:18:38,264
Yes, yes, yes.

89
00:18:38,264 --> 00:18:48,264
So I want to bungee jump and break my shell and grow up more and more.

90
00:18:48,264 --> 00:18:54,264
I want to bungee jump with my seniors one day.

91
00:19:06,816 --> 00:19:08,816
It's cute. Let's drop it and use it.

92
00:19:26,847 --> 00:19:28,847
This year, I dyed my hair orange.

93
00:19:37,504 --> 00:19:47,468
I have the least physical strength and muscle strength among AKB48 members, so I'm going to train as much as I die this year.

94
00:20:14,760 --> 00:20:15,760
Thank you for having me!

95
00:20:18,760 --> 00:20:24,760
My resolution for this year is...

96
00:20:25,760 --> 00:20:26,760
Ta-dah!

97
00:20:39,840 --> 00:20:42,840
The four of us have something special in mind.

98
00:20:42,840 --> 00:20:45,840
First of all, why did we choose the word "evolve"?

99
00:20:45,840 --> 00:20:49,840
I still have a lot of room for improvement,

100
00:20:49,840 --> 00:20:53,840
so I wanted to share with you what I've improved this year.

101
00:20:53,840 --> 00:20:55,840
That's why we chose the word "evolve".

102
00:20:55,840 --> 00:20:57,840
And what this "invisible human" is...

103
00:20:57,840 --> 00:21:00,840
You know, monkeys.

104
00:21:00,840 --> 00:21:03,840
Invisible, human, monkey, human, monkey...

105
00:21:03,840 --> 00:21:05,840
They become like this and become like this and become human.

106
00:21:05,840 --> 00:21:06,840
Do you know what I mean?

107
00:21:06,840 --> 00:21:10,840
That's why we chose "evolve" and "invisible human".

108
00:21:31,680 --> 00:21:34,956
We are SALLY from AKB48 and SIROTOLI SALLY from AKB48.

109
00:21:38,048 --> 00:21:43,660
My goal for this year is to be cheerful and energetic.

110
00:21:46,912 --> 00:21:57,912
Just like these words, I want to say with you that it was fun to spend a year cheerfully and energetically. Thank you for your support this year as well.

111
00:22:09,312 --> 00:22:11,312
I'm Kawamura Yui, and I'm a postgraduate student.

112
00:22:11,312 --> 00:22:13,312
You're cute!

113
00:22:13,312 --> 00:22:15,312
Thank you!

114
00:22:15,312 --> 00:22:17,312
My goal for this year is...

115
00:22:17,312 --> 00:22:19,312
to be healthy!

116
00:22:24,800 --> 00:22:39,800
I thought about it for a long time, but I thought that nothing would start unless you are healthy, so I hope you and I can stay healthy for a year and stay healthy together. Thank you for your support this year as well!

117
00:22:43,104 --> 00:22:45,104
Wow, I'm getting goosebumps.

118
00:22:45,104 --> 00:22:46,956
Here we go!

119
00:22:52,320 --> 00:22:55,820
Hello, I'm Kuro Okasumi and I'm an 18-year-old researcher.

120
00:22:55,820 --> 00:22:59,320
My persimmon dye is...

121
00:22:59,320 --> 00:23:01,320
Thank you. My persimmon dye is...

122
00:23:01,320 --> 00:23:02,827
Ta-da!

123
00:23:09,786 --> 00:23:11,786
I'm an invincible idol!

124
00:23:12,786 --> 00:23:13,786
Thank you.

125
00:23:14,786 --> 00:23:17,786
After all, invincible is invincible.

126
00:23:18,786 --> 00:23:24,786
So I want this year to be a better year than 2024.

127
00:23:24,786 --> 00:23:26,786
No enemies.

128
00:23:27,786 --> 00:23:30,786
I want to make a lot of fun memories with you.

129
00:23:30,786 --> 00:23:34,786
Please support me, Kasumi Kudo, again this year.

130
00:23:34,786 --> 00:23:36,786
Thank you very much.

131
00:23:36,786 --> 00:23:38,786
Kasumi, you said "enemy" wrongly.

132
00:23:38,786 --> 00:23:39,786
What?

133
00:23:39,786 --> 00:23:40,786
Are you okay with that?

134
00:23:40,786 --> 00:23:42,786
You said "invincible" wrongly.

135
00:23:42,786 --> 00:23:43,786
Oh, you're right!

136
00:23:43,786 --> 00:23:44,786
What?

137
00:23:44,786 --> 00:23:46,786
You said "enemy" wrongly.

138
00:23:46,786 --> 00:23:47,786
Am I wrong?

139
00:23:47,786 --> 00:23:49,786
You're not bad in real life.

140
00:23:49,786 --> 00:23:51,786
I'm okay with that "invincible" part.

141
00:23:53,786 --> 00:23:54,786
That's good.

142
00:23:54,786 --> 00:23:55,786
Calculation?

143
00:23:55,786 --> 00:23:57,786
Wait, I really made a mistake.

144
00:23:59,786 --> 00:24:00,786
Okay, next.

145
00:24:00,786 --> 00:24:03,786
I'm Yumemiko and Yumi Sakoyumi, and I'm a first-year research student.

146
00:24:03,786 --> 00:24:04,414
Yumemiko and Yumi Sakoyumi!

147
00:24:15,570 --> 00:24:18,570
I'll do my best posting on social media!

148
00:24:46,458 --> 00:24:49,458
Hi, I'm Maruyama Hinata and I'm 19 years old.

149
00:24:53,458 --> 00:24:58,458
My goal for this year is "do my best (always)".

150
00:25:03,458 --> 00:25:06,458
At first, I wasn't sure if I should try it or not.

151
00:25:06,458 --> 00:25:16,458
I had a lot of things I wanted to do, like dancing and singing, food reports and so on.

152
00:25:16,458 --> 00:25:20,458
But first of all, I thought I should do my best no matter what I do.

153
00:25:20,458 --> 00:25:23,458
So I posted "do my best (always)".

154
00:25:46,706 --> 00:25:48,706
I'm Saki Kondo! Nice to meet you!

155
00:25:53,010 --> 00:25:55,966
My goal for this year is...

156
00:26:03,186 --> 00:26:11,186
I want to try many things this year and do my best to make you smile. Thank you!

157
00:26:16,018 --> 00:26:27,018
We drew this and we would like to give it to you as a gift!

158
00:35:19,685 --> 00:35:22,033
A song for you!

159
00:35:47,429 --> 00:35:51,429
So in exchange for this year's amulet, please try to get it!

160
00:36:53,997 --> 00:36:54,997
You don't know about this?

161
00:36:54,997 --> 00:36:56,721
You don't know about this history?

162
00:37:15,557 --> 00:37:17,557
Our legs are always swollen.

163
00:37:52,773 --> 00:37:54,773
So, here we are!

164
00:38:09,829 --> 00:38:12,689
I'm going to stab more and more swords and this time...

165
00:38:42,656 --> 00:38:46,656
Let's do our best for our first try of the new year!

166
00:38:46,656 --> 00:38:52,656
There will be three rounds in total, so we can get up to three people.

167
00:38:52,656 --> 00:38:54,656
Let's get it for sure!

168
00:38:54,656 --> 00:38:55,656
By the way...

169
00:38:55,656 --> 00:38:57,656
You want to get it for sure, right?

170
00:38:57,656 --> 00:39:03,656
By the way, even if everyone stabs, there is a possibility that no one will fly.

171
00:39:03,656 --> 00:39:08,656
If no one flies in the first round, it will be a carryover.

172
00:39:08,656 --> 00:39:09,656
What is it?

173
00:39:09,656 --> 00:39:11,656
Yes, it's a carryover.

174
00:39:11,656 --> 00:39:15,588
The number of people who fly out in the second round will also be added.

175
00:39:18,840 --> 00:39:20,840
There is also a double chance.

176
00:39:20,840 --> 00:39:22,840
Please come out.

177
00:39:22,840 --> 00:39:24,840
Then I would like to do it right away.

178
00:39:24,840 --> 00:39:26,840
Yes.

179
00:39:26,840 --> 00:39:28,548
Who will go first?

180
00:39:33,560 --> 00:39:35,560
We might get it right in one go.

181
00:39:35,560 --> 00:39:36,676
That's right!

182
00:40:01,880 --> 00:40:03,880
"Invincible Idols"

183
00:46:19,082 --> 00:46:21,082
"Invincible Idols"

184
00:47:13,162 --> 00:47:16,598
Let's hear it! "Tears Surprise"!

185
00:50:15,594 --> 00:50:17,594
I'll hand it over to you. Please come closer.

186
00:54:02,309 --> 00:54:03,309
Thank you.

187
00:54:03,309 --> 00:54:05,309
We're running out of time.

188
00:54:05,309 --> 00:54:07,309
The next song will be the last one.

189
00:54:19,389 --> 00:54:21,389
Let's listen to it.

190
00:54:23,389 --> 00:54:25,389
"Happy End".

191
00:58:29,469 --> 00:58:32,469
Please wait in the audience until then!

192
00:58:32,469 --> 00:58:35,469
And let's say goodbye for the first time this year...

193
00:58:35,469 --> 00:58:40,469
Let's say goodbye for the first time this year...

194
01:00:44,029 --> 01:00:46,029
It was very fun to be able to perform in front of you.

195
01:00:59,837 --> 01:01:11,837
It was my first time doing a lot of songs, so I was very nervous, but I think I was able to perform happily with a lot of help from my seniors.

196
01:01:11,837 --> 01:01:12,617
Thank you very much!

197
01:01:27,901 --> 01:01:34,473
I think it's meaningful, so I will do my best to continue to pull AKB48 with all the researchers in the future.

