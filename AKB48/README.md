# AKB48

AKB48 (pronounced A.K.B. Forty-Eight) is a Japanese idol girl group named after the Akihabara (Akiba for short) area in Tokyo, where the group's theater is located. AKB48's producer, Yasushi Akimoto, wanted to form a girl group with its own theater and performing daily so fans could always see them live (which is not the case with usual pop groups giving occasional concerts and seen on television). This "idols you can meet" concept includes teams which can rotate performances and perform simultaneously at several events and "handshake" events, where fans can meet group members. Akimoto has expanded the AKB48 concept to several girl groups in Mainland China, Japan, Indonesia, Thailand, Taiwan and the Philippines.

AKB48 have been characterized as a social phenomenon. They are among the highest-earning musical acts in Japan, and are the fifth-best-selling girl group worldwide. For example, their 2012 sales from record and DVD/Blu-ray releases reached $226 million, earning the group the top spot in the 2012 artist ranking. As of April 2019, the group has sold over 60 million records, including over 6 million albums. At least 35 AKB48's singles have topped the Oricon Weekly Singles Chart, with at least 30 singles selling over a million copies each, making the group the highest selling musical act in Japan in terms of singles sold. Their highest selling single, "Teacher Teacher", sold over 3 million in 2018 according to Billboard / Soundscan. Between 2010 and 2020, AKB48's singles have occupied at least the top two spots of the Oricon Yearly Singles Chart.

# Concept

AKB48 was founded as "idols you can meet". Japanese idols are entertainers/performers who appeal directly to fans for support. Close interactions between fans and idols allow idols to cultivate and maintain loyal fan followings. The group's chief producer, Yasushi Akimoto, said that his goal was to create a unique idol group which, unlike other idol groups which perform occasional concerts and appear primarily on television, would perform regularly in its own theater. The AKB48 Theater is in the Don Quijote store in Akihabara, Tokyo.

The group is split into several teams, reducing its members' workload (since the theater's near-daily performance is by only one team at a time) and enabling AKB48 to perform simultaneously in several places. According to former member Misaki Iwasa, each team has its own theme. Team A represents freedom; Team B is idol-like, with cute costumes, and Team K has a strong, powerful image. According to an early press release, the group was intended to have 16 members on each of three teams, for a total membership of 48; but its membership has varied over time, and has exceeded 120 people. New members are called trainees (研究生, kenkyūsei) who are understudies for the group, performing occasionally in the theater as a team.

The group members' ages range from their early teens to over 30, and they are selected from regular auditions. Members are not allowed to date, and must be well-behaved; any violation of these restrictions is to be punished, possibly by expulsion from the group. AKB48 has a system that allows members to "graduate" from the group when they are older and are replaced by trainees who are promoted. Monica Hesse of The Washington Post described the AKB48 audition process as "rolling American Idol-esque".

# Active TV Shows

* OUT OF 48
* AKB48, Saikin Kiita！～ issho ni KYOUSOU shimasen ka ？～
* Atsumare! Marumaru AKB
* AKB48 17 kenkyuujo!
* AKB48 Chousatai!
* AKB48 Nemousu TV