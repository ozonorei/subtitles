# Suiensaa (すイエんサー)

Sueinsaa is a scientific variety show in Japan. The show has featured numerous members of the Japanese group AKB48.

![Show poster](fanart.jpg)

# Program Information

* Broadcast on: NHK Educational TV
* Air On : Saturday, 10:30~ (JST)
* Started: 2009-03-31
* Ended  : 2023-03-27
* Status : Ended
* Genres: Variety
* Links: [NHK](https://www.nhk.jp/p/suiensaa/ts/K8Q468L9RR/).
  
# Notes

## 2024-02-16

We are only doing the episodes that feature AKB48 members. The show has featured many other guests and topics.