# Sakamichi no Mukou ni wa Aozora ga Hirogatteita (坂道の向こうには青空が広がっていた。)

This show is the first weekly variety show of Boku ga Mitakatta Aozora group. This show is filmed in front of a live studio audience. Comedy duo Chocolate Planet will serve as MCs and help members grow as idols. Narration is provided by Harada Aoi (ex-Sakurazaka46).

![Boku ga Mitakatta Aozora members](fanart.jpg)

# MC

* Shun "Chocolate Planet" Matsuo. (Comedian)
* Shohei "Chocolate Planet" Osada. (Comedian)
* Harada "ex-Sakurazaka46" Aoi. (Narration)

# Program Information

* Broadcast on: Fuji TV, TVer
* Air On: Tuesday, 24:25- (JST)
* Started: 2023-10-17
* Status: Continuing
* Genre: Variety, Comedy
* Links: [Fuji TV](https://www.fujitv.co.jp/bokua/), [TVer](https://tver.jp/series/sr7dnwynr0)
