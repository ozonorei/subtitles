# Yabababambi (#ヤバババンビ)

This is the first show for the 7-member idol group "#BabaBaBambi." In order to become an idol suitable for Budokan, they take on various “dangerous” projects!

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: U-NEXT
* Air On: Friday once or twice a month.
* Started: 2022-04-15
* Status: Ended
* Genres: Variety, Comedy, Games
* Links: [U-NEXT](https://www.video.unext.jp/title/SID0089374/)

# Notes

## 2024-06-04

Season 02 ended on 2023-03-29.