# Utsubora

One day, a beautiful woman named Fujino Aki died mysteriously. A woman, Sakura, who claims to be Aki's twin sister, appears in front of popular novelist Mizorogi Shun and turns out Mizorogi had plagiarised Aki's novel "Utsubora". Sakura, who has the manuscript of "Utsubora", makes a proposal to Mizorogi but this drives him into deep darkness. Meanwhile, detectives from Koharu Police Station are pursuing the truth behind Aki's death.

![Utsubora - Maeda Atsuko](fanart.jpg)

# Airing Status

* Channel: WOWOW Streaming Service
* Started: 2023-03-24
* Ended: 2023-05-12
* Status: Ended