# Hello! Pro no Oshigoto Challenge!

Hello Pro no Oshigoto Challenge (ハロプロのお仕事チャレンジ; Hello! Project's Job Challenge) is a variety show starring Hello! Project. Through the show, Morning Musume '19, ANGERME, Juice=Juice, Country Girls, Kobushi Factory, Tsubaki Factory, and BEYOOOOONDS challenged various real-world jobs in order to improve their skills as performers. The first season began airing on April 27, 2019 on the dTV Channel's Hikari TV Channel+.[1]
All 13 episodes of the first season were compiled in the Hello! Project DVD Magazine Vol.64, which was released on December 31, 2019, and have been re-edited with unreleased footage.

On February 13, 2020, the show's second season began airing under the name Hello Pro no Oshigoto Challenge! 2 (ハロプロのお仕事チャレンジ！2).[2][3]

On August 6 and 13, 2020, a two part special featuring select Hello! Project members reflecting on the past two seasons aired ahead of season 3 under the title Hello Pro no Oshigoto Challenge! 3 e no Michi ~Kinkyuu Kikaku Kaigi~ (ハロプロのお仕事チャレンジ！3への道 ～緊急企画会議～).[4]


# Airing Status

* Source: Hikari TV, dTV channel
* Started: 2019-04-27
* Status: Ended
