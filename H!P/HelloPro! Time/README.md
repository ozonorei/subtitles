# HelloPro! Time (ハロプロ!TIME)

Each week different Hello! Project members would be given a camera and would film a variety of topics such as music video shoots, backstage concert footage, their pets, visits to restaurants, days out at amusement parks, shopping or trips abroad such as Hawaii, Seattle and Korea plus much more.

The groups featured were Morning Musume, Berryz Koubou, C-ute and S/mileage plus soloist Mano Erina.


![HelloPro! Time Logo](fanart.jpg)

# Airing Status

* Channel: TV Tokyo
* Started: 2011-04-21
* Ended  : 2012-05-31
* Status : Ended
* Genre  : Variety
