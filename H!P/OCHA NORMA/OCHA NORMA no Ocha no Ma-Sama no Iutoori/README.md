# OCHA NORMA no Ocha no Ma-Sama no Iutoori (OCHA NORMAの お茶の間さまの言うとおり)

Hello! Project's idol group "OCHA NORMA", which made its major debut in July 2022, will challenge on location live variety show! Choose the one you want to try the most from among the multiple projects presented in advance within the BSJapanext app! The content of the location will be determined by the amount of enthusiasm. Your thoughts will change OCHA NORMA's growth and future!

![OCHA NORMA Members](fanart.jpg)

# Airing Status

* Channel: BSJapanext
* Started: 2022-03-31
* Ended  : 2023-09-26
* Status : Ended
* Genre  : Variety
