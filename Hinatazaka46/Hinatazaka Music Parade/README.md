# Hinatazaka Music Parade (日向坂ミュージックパレード)

About a year and a half after joining, the 4th generation members of Hinatazaka46, who continue to grow day by day, will welcome the super popular trio of "3 o'clock heroine," as the MCs of the program, and will be singing, dancing, talking, and playing games.

![Cast Members](fanart.jpg)

# Airing Status

* Broadcast on: NTV, Hulu, TVer
* Air On: Tuesday, 24:59-25:29 (JST)
* Started: 2024-04-30
* Status: Continuing
* Genre: Music, Variety, Talk Show.
* Links: [NTV](https://www.ntv.co.jp/hinapare/), [Hulu](https://www.hulu.jp/hinatazaka-music-parade), [TVer](https://tver.jp/series/sre7xcpldd)