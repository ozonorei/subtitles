# Hinatazaka de Aimashou (日向坂で会いましょう)

A Japanese variety show starring Hinatazaka46 and is hosted by Japanese comedy duo Audrey. Originally it was `Hiragana Oshi` but due to Hiragana Keyakizaka46 becoming Hinatazaka46, the program has now been renamed.

![Hinatazaka de Aimashou cast](fanart.jpg)

# MCs

* Masayasu "Audrey" Wakabayashi. `(Comedian)`.
* Toshiaki "Audrey" Kasuga. `(Comedian)`

# Airing Status

* Channel: TV Tokyo
* Air On: Monday 01:05 AM (JST)
* Started: 2019-04-08
* Status: Continuing
* Genre: Variety, Comedy, Talk Show
* Links: [TV Tokyo](https://www.tv-tokyo.co.jp/hinatazaka/), [Team-HinataSubs](https://hinatasubs.wordpress.com/)

# Notes

## 2024-05-20

This show is mainly subbed by <strong>Team HinataSubs</strong>. You can find their subs on their [website](https://hinatasubs.wordpress.com/).