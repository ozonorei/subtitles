# Momoclo-Dan (ももクロDan)

Momoclo Dan is a two minute program that aired on TBS since January of 2012, allowing fans to see yet another side of the charismatic and charming idol group, Momoiro Clover Z. "Momoclo Dan" revolves around the group as they secretly plot to take over the world.

![Momoclo-Dan Cast](fanart.jpg)

# Program Information

* Broadcast on: TBS
* Air On: Monday, ?? (JST)
* Started: 2012-01-16
* Status: Ended
* Genres: Variety, Comedy.
* Links: [TBS](https://www.tbs.co.jp/momoclo-dan/)