# Idol Shikkaku (アイドル失格)

Depicts the forbidden romance between an idol and the otaku who has serious feelings for her. The main character is Onodera Mimika, a second-year high school student who is the center member of the increasingly popular idol group Tetra but has concerns regarding her future. One day, after the words of her fan Keita resonate deeply with her, she heads towards where he works at his part-time job and thus, crosses the barrier between idol and fan.

![Show Poster](fanart.jpg)

# Airing Status

* Channel: DMM TV, TVer
* Air On: Saturday, 17:00~ (JST)
* Started: 2024-01-13
* Status: Continuing
* Genre: Drama, Music, Romance

