# HamAsuka Housou-bu (ハマスカ放送部)

Okamoto Hama, the highly esteemed bassist of OKAMOTO'S and renowned radio DJ, is teaming up with Saito Asuka former Nogizaka46 member. Asuka, known for her immense popularity, shrouds her real face in mystery. Together, they will be delivering a talk show filled with music from an unauthorized broadcasting room within the TV Asahi station.

This unique program will delve into various musical aspects, from exploring tunes that enhance productivity during work and study to discovering school songs from across the country. Periodically, the duo will engage in musical sessions, showcasing their instrumental talents as members of "HamAsuka" The goal is to broadcast a soft and inviting atmosphere, fueled by the excellent compatibility between these two musical talents. This show promises to be captivating, ensuring that viewers become hooked on the delightful ambiance created by Okamoto Hama and Saito Asuka.

![Fanart](fanart.jpg)

# MC

* Saito "ex-Nogizaka46" Asuka. `Idol`
* Okamoto "Okamoto's" Hama. `Musician`

# Program Information

* Broadcast on: TV Asahi, TVer
* Air on: Monday, 24:15 ~ (JST)
* Started: 2021-10-07
* Status : Continuing
* Genre  : Variety, Music
* Links: [TV Asahi](https://www.tv-asahi.co.jp/hamasukahousoubu/), [TVer](https://tver.jp/series/srryfaoaht)
