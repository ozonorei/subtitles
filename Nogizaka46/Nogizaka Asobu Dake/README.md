# Nogizaka Asobu Dake (乃木坂あそぶだけ)

A mini-series of Nogizaka46 idol group. The members do what they want such as play games, toys, and sports!

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: NogiDoga
* Air On: Last Friday of the month, Undetermined time.
* Started: 2021-05-18
* Status: Continuing
* Genres: Variety, Comedy, Games
* Links: [NogiDoga](https://nogidoga.com/series/63)