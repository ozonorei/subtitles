# Nogizaka Otameshichuu (乃木坂お試し中)

Nogizaka Otameshichuu is a new variety show on TBS Channel 1 that replaced Nogi Eigo, and members try out various stuff.

![Show Poster](fanart.jpg)

# MC
* Suzuki Taku (Comedian)
* Ito "Nogizaka46" Riria (Idol).

# Program Information

* Broadcast on: TBS
* Air On: Last Saturday of every month, 23:00~ (JST)
* Started: 2021-02-27
* Status: Continuing
* Genres: Variety, Comedy, Games
* Links: [TBS](https://www.tbs.co.jp/tbs-ch/item/v3090/)