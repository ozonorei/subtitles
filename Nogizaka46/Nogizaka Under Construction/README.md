# Nogizaka Under Construction (乃木中)

Nogichuu (乃木中?), is a variety program that succeeded Nogizakatte, Doko? as Nogizaka46's main program. 

![Fanart](fanart.jpg)

# Airing Status

* Broadcast on: TV Tokyo,TV Aichi, YouTube
* Air on: Every Sunday 24:15~ (JST)
* Started: 2015-04-19 
* Status : Continuing
* Genre  : Variety, Music, Comedy
* Links  : [TV Aichi](https://tv-aichi.co.jp/nogi-kou/), [YouTube](https://www.youtube.com/playlist?list=PL-tVKL3zVywavzveUa688lwEMfRNajMpi)
# Notes

## 2023-12-06

Sadly, our dear GEES-sama, has graduated from NUC, and will no longer provide real subs, as such we will be generating weekly subs from now on. until someone else takes the torch.