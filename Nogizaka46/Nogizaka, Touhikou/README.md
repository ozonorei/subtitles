# Nogizaka, Touhikou (乃木坂、逃避行。)

The members of Nogizaka46 escape from their busy daily lives for a little while. Two members from a certain pair choose a place they absolutely want to go to right now and plan a two-day, one-night trip for themselves. This documentary variety show will allow viewers to see the real faces of the members and their relationship through their two-person trip! 

![Show Poster/fanart](fanart.jpg)

# Cast

* Nogizaka46 Members.

# Program Information

* Broadcast on: LEMINO
* Air On : Friday, 25:30~ (JST), 
* Started: 2024-07-05
* Status : Continuing
* Genres : Variety, Travel
* Links  : [LEMINO](https://lemino.docomo.ne.jp/contents/Y3JpZDovL3BsYWxhLmlwdHZmLmpwL2dyb3VwL2IxMDFmYTA=?pit_git_type=SERIES)

# Notes

## 2024-07-06

The show is quite new, there is not lot of information about it. 