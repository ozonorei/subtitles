# Nogizaka46 - Hitori de Dekirumon (ひとりでできるもん)

A variety show focused on the 5th generation members of Nogizaka46. The show is mainly about things that the members can do alone, and the show is hosted by the 5th generation members themselves.

![Sakura Meets Cast](fanart.jpg)

# Program Information

* Broadcast on: nogidoga
* Air On: Friday Twice a month?
* Started: 2023-01-01
* Status: Continuing
* Genres: Variety, Travel
* Links: [nogidoga](https://nogidoga.com/series/89)