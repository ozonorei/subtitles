# Tokyo PC Club (東京パソコンクラブ ～乃木坂46×ゲームプログラミング～)

A Variety show which focuses on programming a three members of Nogizaka46 learn how to do game programming from scratch and work hard with the goal of creating an original game! This is a programming documentary program. Will they be able to become full-fledged female programmers? We look forward to seeing it!

![Fanart](fanart.jpg)

# Cast

* Yoshida Ayano Christie. (Regular)
* Yumiki Nao. (Regular)
* Hayashi Runa. (Regular)
* Sato Rika. (Narration)

# Airing Status

* Channel: BS TV Tokyo, TVer
* Started: 2022-04-15
* Status : Continuing
* Genre  : Variety, Educational
