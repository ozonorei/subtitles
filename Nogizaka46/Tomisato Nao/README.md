# (♥) Tomisato Nao (Kanji: 冨里奈央) (Kana: とみさと なお)

Tomisato Nao is a member of Nogizaka46's 5th Generation.

![Tomisato Nao](poster.jpg)

<div class="video-container">
<iframe src="https://www.youtube-nocookie.com/embed/j--WnfLHCMc?si=u6KkHZCe2IMfMrOD" 
title="YouTube video player" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

# Information

* Nicknames: Naonao (なおなお?)
* Former Stage Name: Aosaka Nana (青坂奈々?)
* Birthday: 2006-09-18
* Birthplace: Chiba, Japan
* Blood type: O
* Zodiac Sign: ♍︎ Virgo
* Height: 164cm
* Group: Sakurazaka46
* Agency: [N46Div](http://nogizaka46.com/)
* Activity Status: 2022-present
* Links: [Official Blog](https://www.nogizaka46.com/s/n46/diary/MEMBER/list?ima=4520&ct=55393), [Official Showroom](https://www.showroom-live.com/r/46_tomisatonao), [Stage48](http://stage48.net/wiki/index.php/Tomisato_Nao), [AKB48 Fandom](https://akb48.fandom.com/wiki/Tomisato_Nao).

# Trivia

* Fifth Nogizaka46 5th Generation member to be announced
* Lightstick Colors: Turquoise and Turquoise
* Favorite color: Pink, white
* Favorite food: Cookies, cocoa, strawberries, sandwiches, sweets
* Favorite animal: Dog, cat
* Favorite Nogizaka46 songs: Girl's Rule, Kaerimichi wa Toomawari Shitaku Naru, Sekkachi na Katatsumuri
* Pros: Treats others in a way that she'd like to be treated
* Cons: "Can't get up by myself in the morning zzz", does things at her own pace
* Hobbies: Watching animal videos, raising animals in smartphone games.
* Future ambitions: To become a regular cast member of "Sakagami Animal Kingdom", an animal variety show; run a cat cafe
* In her introductory video, is holding a see-through lily (Lilium maculatum)
* Prior to joining Nogizaka46, was an idol under Ever Green Entertainment, working under the name Aosaka Nana (青坂奈々)
* Is a former member of girl group Ichikawa Otomoe (市川乙女)
* Family raises jellyfish and crayfish at home
* Dad enjoys fishing, and the family often goes crayfishing together
* Father is a Nogizaka46 fan, Told Nao that if she made it to the final round of the Nogizaka46 audition, he'd buy her an iPhone. Did so after she passed.
* Would like to visit America and eat a big hamburger.
* Would like to try danso (cross-dressing cosplay) someday.
* Wants to travel to Korea and Hawaii someday.
* Has seen zashiki-warashi (座敷童子, a protective yokai associated with good fortune but can be childlike and mischievous) in her family home, per Showroom.
* Tested positive for COVID-19 on 28 Aug 2022
