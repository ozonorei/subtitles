# Japanese idols variety shows AI translation project.

The aim of this project is to generate english subtitles for japanese idols groups variety shows. I usually generate
subs for shows that has no active sub groups. **When there are subs made by groups please use them, they are going to be
most likely better as human did the review and subbing**. These subtitles are meant be used as last resort.

For Subbing groups, feel free to use the subtitles provided here as starting point for your work. 

--------------

# Update

We have created new website for the project, you can find it [https://subs.j-idols.net/](https://subs.j-idols.net/). The website is exact mirror of this repository, so you can use it as alternative to this repository for easier navigation. However, The website sync once every 15min.

We also have new repository for the shows that didn't get the v3.4 upgrade. But due to naming difference they are on different repository, you can find it [here](https://subs.j-idols.net/plexified_subs/).

--------------

About the Subtitle Generation
=============================

The AI model i use was mainly based on `Whisper AI` project, that's no longer the case right now i use privately trained model which was trained on more japanese audio. And the pipeline include more steps than just audio to text. However, due to the automation nature of the project, it bound to have some problems. Here are some of the problems that you might encounter as of `AISubs v3.6`:

* I/You, He/She.
* Names & Homophones.
* Multiple people speaking at the same time.
* Music sometimes are translated, sometimes get's the karaoke treatment, and often times ignored.
* Obviously it does not translate on screen text.
* Long period of silence lead the model to hallucinate, showing random text or older/future subbed lines.

Even accounting for those problems, the final subtitles are very watchable.

--------------

# Shows we are actively generating subs for:-  

#### Note: All these subtitles are generated based on content found in A!O unless otherwise stated.

##### Saturday

* [Nogizaka Asobu Dake](https://nogidoga.com/series/63) - `(Bi-weekly)`
* [Nogizaka Otameshichu](https://www.tbs.co.jp/tbs-ch/item/v3057/) - `(Last week of each month)`
* [Kimi wa =LOVE wo Aiseru ka!!!](https://otn.fujitv.co.jp/equal-love/) - `(Last Saturday of each month)`
* [AKB48, Second trip after a long time](https://tver.jp/series/sruh5975ky) - `(Saturday, ~~)`
  
##### Sunday

* [Hinatazaka Anime-bu](https://www.ktv.jp/hinatazakaanimebu/)
* [Nogizaka Under Construction](https://tv-aichi.co.jp/nogi-kou/) - `(Due to GEES-sama graduating from NUC)`
* [Soko Magattara, Sakurazaka?](https://www.tv-tokyo.co.jp/keyaki/) - `(Due to sub group no longer sharing soft-subs)`
* [SKE48 no Mikanzen](https://tv-aichi.co.jp/mikanzenTV/) - `(2nd and 4th Sundays of every month)`

##### Monday

* [HamAsuka Housou-bu](https://www.tv-asahi.co.jp/hamasukahousoubu/)
* [Chou Nogizaka Star Tanjou!](https://www.ntv.co.jp/newnogistar/)
* [Ikuze! Tsubaki Factory](https://tv.spaceshower.jp/p/ikuzetsubak) - `(Bi-weekly)`

##### Tuesday

* [Sakamichi no Mukou ni wa Aozora ga Hirogatteita](https://www.fujitv.co.jp/sakamichinomukou/)
* [ano-chan no Den Den Denpa](https://www.tv-tokyo.co.jp/dendendenpa/)
* [Hinatazaka Music Parade](https://www.ntv.co.jp/hinapare/)
* [#STU48 no KuraCon](https://www.tss-tv.co.jp/stu48_kuracon/) - `(Last week of each month)`
* [NMB48 Sports Kingdom](https://www.nicovideo.jp/series/317579) - `(once a month)`

##### Wednesday

* [Hinatazaka ni Narimashou](https://lemino.docomo.ne.jp/search/word/%E6%97%A5%E5%90%91%E5%9D%82%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%BE%E3%81%97%E3%82%87%E3%81%86)
* [Atsumare! Marumaru AKB](https://douga.tv-asahi.co.jp/program/40736-40735/46753)
* [Flying Joy](https://www.hulu.jp/flying-joy) - `(Unknown, possibly once month)`

##### Thursday

* [Sakura Meets](https://www.tv-asahi.co.jp/barabara/)
* [AKB Nemousu TV](https://www.fami-geki.com/nemousu/) - `(Status of of the uploader is unknown)`
* [STU48 Imousu TV](http://www.fami-geki.com/imousu/) - `(Status of of the uploader is unknown)`
* [NMB48 no #EmoSto](https://www.mbs.jp/nmb48emosuto/) - `(Status of of the uploader is unknown)`
* [OCHA NORMA no Ocha no Ma-Sama no Iutoori](www.bsjapanext.co.jp/program/iutoori/) - `(Regular show is finished, only special episodes released)`

##### Friday

* [I =LOVE! Gaming](https://www.tv-asahi.co.jp/ilovegaming/)
* [Tokyo PC Club](https://www.bs-tvtokyo.co.jp/nogi_paso/)
* [Chou Tokimeki Sendenbu - Tokimeki♡Precious](https://douga.tv-asahi.co.jp/program/42731-42730/46624)
* [ABEMA BOATRACE SPACE Kuro-chan to Crew-chan 2nd](https://abema.tv/video/title/221-226?lang=en)
* [≠ME Senpai, Motto Oshiete Kudasai!!](https://www.hulu.jp/not-equal-me-please-tell-me-senpai) - `(Bi-weekly)`

#### No strict release time

* [Nogizaka46 Hitori de Dekirumon](https://nogidoga.com/series/89) - `(No strict release time)`
* [Nogizaka Burari](https://nogidoga.com/series/52) - `(No strict release time)`
* [AKB48 Chousatai!](https://akb48chousatai.com/) - `(Unknown)`

--------------

## YouTube Channels
#### Note: The following content subs are generated based on content from direct YouTube links.

* [Nogizaka Channel.](https://www.youtube.com/@nogizakahaishinchu)
* [NMB48 Channel.](https://www.youtube.com/@YNNNMB48CHANNEL)
* [01familia 01TV Channel.](https://www.youtube.com/@user-xp5zv8my8n)
* [Hello Project - Study Abroad Channel.](https://www.youtube.com/@hellostudyabroad4249)
* [#2i2 Channel.](https://www.youtube.com/@2i2)
* [Babababambi Channel.](https://www.youtube.com/@babababambi)
* [iKonoJoy Channel.](https://www.youtube.com/@ikonoijoy)
* [SKE48 Channel](https://www.youtube.com/@SKE48)
* [Ozono Momoko (ex-Nogizaka46) Channel.](https://www.youtube.com/@-momoko)
* [Okada Nana (ex-AKB48) Channel.](https://www.youtube.com/@okadanana1107)
* [HKT48 Totto Channel.](https://www.youtube.com/@HKT48_toto-to)
* [Sakurazaka46 SMEJ Channel.](https://www.youtube.com/@sakurazaka46SMEJ)
* [Shiraishi Mai (ex-Nogizaka46) Channel.](https://www.youtube.com/@maishiraishi)
* [Morning Musume Channel.](https://www.youtube.com/@morningmusume)
* [YuuNaaMogiOn YouTube Channel](https://www.youtube.com/channel/UC0X6o-aELH_Rt3ZXLoKxe-w)
* [NGT48 YouTube Channel](https://www.youtube.com/@NGT48)
* [Hinatazaka YouTube Channel](https://www.youtube.com/@hinatazakachannel)
* [AKB48 Live Channel](https://www.youtube.com/@akb48officiallivech)
* [Sakurazaka Channel](https://www.youtube.com/@sakurazakachannel)
* [Cho Tokimeki♡Sendenbu Official Channel](https://www.youtube.com/@TOKISEN)

--------------

## NicoNico Channels
#### Note: The following content subs are generated based on content from direct NicoNico links.

* [AKB48 17 kenkyuujo!](https://nicochannel.jp/akb48-17ken/) - `(Sat)`
* [Nogizaka46 Nama no Idol ga Suki](https://twitter.com/namaidol) - `(Usually last week of the month)`
* [STU48 Channel](https://ch.nicovideo.jp/stu48) - `(Unknown)`
* [SKE48 Unofficial Channel](https://ch.nicovideo.jp/ske48-thetv) - `(Unknown)`

--------------

## TVer
#### Note: The following content subs are generated based on content from direct TVer links.

##### Saturday

* [The GOD Tongue](https://tver.jp/series/srhlozl9ob) - `(Sat)`
* [Geinoujin ga Honki de Kangaeta! Dokkiri GP](https://tver.jp/series/sr8y9zb5qo) - `(Sat)`

##### Sunday

* [Bananaman Special Gourmet](https://tver.jp/series/sr9x22wtdy) - `(Sun)`
* [Bananaman Gourmet Food!!](https://tver.jp/series/sr0y8a45mk) - `(Sun)`

##### Monday

* [Yobidashi Teacher Tanaka](https://tver.jp/series/sr11ylksh6) - `(Mon)`

##### Tuesday

* [AKB48, Saikin Kiita！～ issho ni KYOUSOU shimasen ka ？～](https://tver.jp/series/srbp1t6umv) - `(Tue)`

##### Wednesday

* None

##### Thursday

* [OUT OF 48](https://tver.jp/series/srn72cwlv5) - `(Thu)`
* [Sakurai & Ariyoshi The evening party](https://tver.jp/series/sro9q0gb6r) - `(Thu)`

##### Friday
* [All Night Fujiko](https://tver.jp/series/srrw69kkz7) - `(Fri)`

--------------

FAQ
==============

**Why you are doing this?**

Mainly for myself, I do not speak Japanese, i do understand very little to piece together what is happening. However,
using these subs helps me a lot. I am of the opinion that good enough subs are better than no subs at all.

**Why are you not doing X show?**

Most likely because the show has an active subgroup, However there might be an odd episode here and there that never
get subbed. For example, `Nogizakatte, Doko - ep83`, or shows that only have hard-subs for example `Soko Magattara, Sakurazaka?` I'll make exceptions for that.

**Where X subs located in the repository?**

For daily/weekly shows, I usually follow simple method of sorting subs, for example if the show mainly consist of one group for example `NOGIBINGO`, it will be in group directory in this case `Nogizaka46`, if the show consist of multiple groups, it will be in the group which has highest air time. For Concerts, one time episodes or things that doesn't have a directory it follows the same idea, it most likely will be placed `Group/Other` directory.

For Shows that idols appears but not being actively subbed, it will be placed in `Other` directory. If there is demand for the content or i find it interesting, i'll make a directory for it. and added to to my list of automation. This process is really costly and time consuming, so i'll only do it if there is demand for it.

Now, As we covered the majority of the non-translated idols shows, We are expanding to other variety shows which will be placed in `variety_shows` directory.

**Why the file naming in the repository is different than the one in the releases?**

Due to variety of reasons, If i have to re-generate the subs as i don't keep the original file names in my home server, i.e. i use `Plex/Jellyfin` to watch the content, i have to rename them into something that is parsable by them, which most likely means it wont be in the standard `YYMMDD series - title` format. I try to be consist but it's hard sometimes as release groups sometimes release files with different naming conventions. However, Here is the mapping for the most common ones.

For youtube content as there is no standard i use `YYYYMMDD - title [youtube-(id)]` format, Which you can use the `date` or `youtube-(id)` to match. For `A!O` it most likely exact name except with tags [720p|1080i] etc removed, The reason for removing the tags are the sub usually works for both so it doesn't make sense to have the tag there. For newer shows that we pick we try to keep same name as release group.

**Why there is Ozono Rei, Tomisato Nao directories?**

They are my favorite members in their respective groups, and i like to keep their content separate.

**Do you accept requests?**

Depends on how available i am, and how much workload i have queued, I usually prioritize shows that i like or i find interesting. However, if you have a request, feel free to contact me at one of the channels listed below.

**Note:** There is no guarantee that I'll do it, but i will try my best if the content fits the criteria. and i am remotely interested in it.

**Do you accept donations?**

No, I don't accept donations, I do this for fun and for myself, I don't want to be in a position where i have to do something i don't want to do. However, if you want to support me, you can do so by joining the subbing community, or by supporting the subbing groups.


--------------

Contact Channels
==============

You can reach me at one of the following places.

* Discord: [wowkise@Eito-Cord #aisub](https://discord.gg/bdz7qZbrwu).
* Forum: [Kise@Stage46](http://stage48.net/forum/index.php?members/kise.77644/).
* Tracker: `wowkise@A!O`