# SKE48 Gakuen DVD Box 3

This dvd contains the the following episodes:

* 100403 ep07. Let’s play instruments! (1)
* 100501 ep08. Let's play instruments! (2)
* 100601 ep09. Let's make a girls band! (1)
* Bonus Content.

==================

The subtitle generation was done on the DVD collection which can be found at A!O, makemkv was used to split the DVD into separate files.
