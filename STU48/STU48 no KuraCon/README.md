# #STU48 no KuraCon (＃ＳＴＵ４８のくらコン)

"#STU48 no KuraCon" is a variety show of STU48 2nd generation kenkyuusei broadcast every last Tuesday of the month (KuraCon = Kudaranai Content = stupid/worthless content) The show replaced "STU48 no GanbarimaSU!" and is based on various short VTRs with members.

![STU48 Members](fanart.jpg)

# MCs

* Nakashima Naoki. `(Entertainer/Reporter)`
* Katou Masaya. `(TV Shin Hiroshima Announcer)`

# Program Information

* Broadcast on: TV Shin Hiroshima (TSS), TVer, FOD
* Air On: Last Tuesday of the month, 26:02~ (02:02 AM) (JST)
* Started: 2020-04-28
* Status: Continuing
* Genres: Variety, Comedy, Music
* Links: [TV Shin Hiroshima](https://www.tss-tv.co.jp/stu48_kuracon/), [TVer](https://tver.jp/series/srpnancutd), [FOD](https://fod.fujitv.co.jp/title/2695/)

