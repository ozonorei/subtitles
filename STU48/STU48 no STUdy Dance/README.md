# STU48 no STUdy Dance (STU48のSTUでぃダンス)

A variety show about STU48 members learning various dance styles and also presenting charms of Setouchi in location segments. There should be six episode in total, one episode every two weeks.


![STU48 Members](fanart.jpg)

# Program Information

* Channel: Dance Channel (Premium service only channel)
* Air On: Thursday, 14:00-14:30~ (JST)
* Started: 2023-11-23
* Finished: 2024-02-08
* Status: **Ended**
* Genre: Variety, Dance, Music
* Links: [Dance Channel](https://www.dance-ch.jp/all/stu48.html)