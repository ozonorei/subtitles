# Chokosaku (ちょこさく)

A Spin-off the main show `Soko Magattara, Sakurazaka?` where the original members of Sakurazaka46 are the MC of the show, they present stuffs that were not shown in the main show, and also talk about their personal life and other stuffs.

![Original Sakurazaka Members](fanart.jpg)

# Airing Status

* Broadcast on: Lemino
* Air On: Monday ??:??~ (JST)
* Started: 2024-04-08
* Status: Continuing
* Genre: Variety, Comedy, Talk Show
* Links: [Lemino](https://lemino.docomo.ne.jp/?crid=Y3JpZDovL3BsYWxhLmlwdHZmLmpwL2dyb3VwL2IxMDFiNWY%3D)