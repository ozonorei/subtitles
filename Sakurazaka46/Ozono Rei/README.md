# (♥) Ozono Rei (Kanji: 大園 玲) (Kana: おおぞの れい)

Ozono Rei (大園 玲) is a member of Sakurazaka46's 2nd Generation and a former member of Keyakizaka46 and Sakamichi Kenshusei.

![Ozono Rei](fanart.jpg)

# Information

* Nicknames: Zono (ゾノッ), Rei (れい?), Reirei (れいれい?), Shiichan (しいちゃん?)
* Birthday: 2000-04-18
* Birthplace: Kagoshima, Japan
* Blood type: A
* Zodiac Sign: ♈︎ Aries
* Height: 163cm
* Group: Sakurazaka46
* Agency: [Seed & Flower](https://sakurazaka46.com/)
* Activity Status: 2019-present
* Links: [Stage48](http://stage48.net/wiki/index.php/Ozono_Rei), [AKB48 Fandom](https://akb48.fandom.com/wiki/Ozono_Rei)

# Trivia

* Her audition SHOWROOM number was #41. Her nickname during the auditions was Shiichan (しいちゃん)
* Admired Members: Sugai Yuuka, Kobayashi Yui, Watanabe Risa, Shida Manaka, Kosaka Nao, Nishino Nanase.
* Favorite **Keyakizaka46** song: **Eccentric**, She explained that it helped her change her negative outlook on life.
* Favorite food: Okonomiyaki
* She has been studying psychology at university since 2019.
* She is known for her "Guigui (グイグイ)" character. She is vigorously optimistic and non-hesitant.
* She was crowned the first "quick-witted" queen of the group on Soko Magattara, Sakurazaka?
