# Sakura Meets! (サクラミーツ)

Sakurazaka46 members will perform "original skits" that can only be seen here with popular comedians, and enjoy "backstage games" devised by comedians...!

![Sakura Meets Cast](fanart.jpg)

# Program Information

* Broadcast on: TV Asahi, TELASA, TVer
* Air On: Thursday, 26:02~ (02:02 AM) (JST)
* Started: 2023-04-06
* Status: Continuing
* Genres: Variety, Comedy, Music
* Links: [TV Asahi](https://www.tv-asahi.co.jp/barabara/), [TELASA](https://www.telasa.jp/series/13451), [TVer](https://tver.jp/series/srx97ftk3w)