# Soko Magattara, Sakurazaka? (そこ曲がったら、櫻坂？)

The main variety show of the idol group Sakurazaka46 hosted by Tsuchida Teruyuki and Sawabe Yuu (Haraichi). It is the direct continuation of Keyakitte, Kakenai?, which had the same cast, format, and time slot, but named after the former name of Sakurazaka46, Keyakizaka46.

This is a variety show where the girls take on various projects in order to make further progress.


![Original Sakurazaka Members](fanart.jpg)

# MCs

* Tsuchida Teruyuki (Comedian).
* Sawabe "Haraichi" Yuu (Comedian).

# Airing Status

* Channel: TV Tokyo
* Air On: Sunday 24:35~ (JST)
* Started: 2020-10-18
* Status: Continuing
* Genre: Variety, Comedy, Talk Show
* Links: [TV Tokyo](https://www.tv-tokyo.co.jp/keyaki/)