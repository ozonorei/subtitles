# Tokimeki♡Precious (ときめき♡プレシャス)

This new variety show “Tokimeki♡Precious” by the idol unit “Chou Tokimeki Sendenbu” which has gained support from many girls, they pursue fashionable and cute things that resonate with the girls of Generation Z!

![Tokimeki♡Precious Poster](fanart.jpg)

# Airing Status

* Channel: TV Asahi
* Started: 2023-09-15
* Status: Continuing