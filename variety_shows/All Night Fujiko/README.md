# All Night Fujiko (オールナイトフジコ)

The legendary live broadcast show "All Night Fuji" has been revived after 40 years as "All Night Fuji Co"! Produced by Akimoto Yasushi. Every Friday late night, a two-hour live broadcast where anything can happen!

![Poster](fanart.jpg)

# MC
* Ito "Oswald" Shunsuke. (Comedian).
* Morita Tetsuya. (Comedian).
* Sakuma Nobuhisa.

# Program Information

* Broadcast on: Fuji TV, TVer
* Air On: Friday, 24:55~ (JST)
* Started: 2023-04-15
* Status: Continuing
* Genres: Variety, Comedy
* Links: [Fuji TV](https://www.fujitv.co.jp/allnight-fujiko/), [TVer](https://tver.jp/series/srrw69kkz7)