# Bananaman's Gourmet (バナナマンのせっかくグルメ!! & バナナマンの早起きせっかくグルメ!!)

Gourmet variety show where Bananaman introduces delicious "local food" from all over Japan. There are exquisite gourmet foods that local people want visitors to try to enjoy, and travelers travel to each region and eat the exquisite local gourmet foods recommended by the locals.

This repository contains both `Bananaman's Gourmet Food!` and `Bananaman's Special Gourmet!!` series.

![Poster](fanart.jpg)

# MC

* Shitara "Bananaman" Osamu. (Comedian)
* Himura "Bananaman" Yuki. (Comedian)

# Program Information

* Broadcast on: TBS, TVer, U-NEXT
* Air On: Sunday, 06:00~ 07:00~ (JST)
* Started: 2014-04-28
* Status: Continuing
* Genres: Variety, Food, Travel
* Links: [TBS](https://www.tbs.co.jp/sekkaku-h/), [TVer](https://tver.jp/series/sr0y8a45mk), [TVer2](https://tver.jp/series/sr9x22wtdy), [U-NEXT](https://video.unext.jp/title/SID0088702)