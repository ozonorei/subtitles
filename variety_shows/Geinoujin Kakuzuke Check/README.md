# Kakuzuke Check (芸能人格付けチェック)

A Variety show in which contestants try to distinguish between "High-End" and "Cheap" products, and are ranked according to the number of correct answers.

![Fanart](fanart.jpg)

# Airing Status

* Channel: TV Asahi
* Started: 1999-03-25
* Status : Continuing
* Genre  : Variety
