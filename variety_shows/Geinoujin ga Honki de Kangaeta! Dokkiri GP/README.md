# Geinoujin ga Honki de Kangaeta! Dokkiri GP (芸能人が本気で考えた！ドッキリGP)

The history of "hidden camera pranks" at Fuji TV began with the popular show "Star Dokkiri Maruhi Hokoku," which gained fame in the late 1970s to 1980s. Afterward, entertainers were typically on the receiving end of pranks. However, a new twist emerged with celebrities now actively involved in planning pranks, marking a unique concept for a hidden camera show. Hosted by Yuki Tsunenori and Eiko Koike, with prank creators Toshiaki Megumi, Fuma Kikuchi (Sexy Zone), and Koji Mukai (Snow Man) as regular participants.

![Poster](fanart.jpg)

# MC
* Tsunenori Yuki.
* Koike Eiko.

# Program Information

* Broadcast on: Fuji TV, TVer, FOD
* Air On: Saturday, 19:00~ (JST)
* Started: 2016-10-15
* Status: Continuing
* Genres: Variety, Comedy
* Links: [Fuji TV](https://www.fujitv.co.jp/dokkirigp/), [TVer](https://tver.jp/series/sr8y9zb5qo), [FOD](https://fod.fujitv.co.jp/title/2701/)