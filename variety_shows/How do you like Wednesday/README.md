# How do you like Wednesday? (水曜どうでしょう)

It is a Japanese television variety series that aired on HTB in Hokkaido, Japan, and on other regional television stations in Japan. The program debuted on HTB on October 9, 1996. The series was one of the first local variety programs to be produced on Hokkaido; prior to this series' launch, local variety programs in Hokkaidō were virtually non-existent.

The show is mostly about the MCs traveling and doing various things, such as visiting tourist spots, eating local cuisine, and doing various activities. The show is also known for its frequent use of the word "dōdeshō" (どうでしょう), meaning "How is it?" or "How about it?", which is often used by the MCs to ask each other how they feel about the places they visit.

![Program fanart](fanart.jpg)

# MCs

* Suzui Takayuki.
* Oizumi Hiroshi.

# Program Information

* Broadcast On: HTB, Tokyo MX
* Started: 1998-10-09
* Ended: 2002-09-25
* Status: Ended
* Genre: Variety, Comedy, Travel.
* Links: [HTB](https://www.htb.co.jp/suidou/), [Tokyo MX](https://s.mxtv.jp/variety/suidou/).
* Source used: Nyaa.
