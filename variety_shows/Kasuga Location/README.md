# Kasuga Location (春日ロケーション)

When he was a student, Audrey Kasuga actually wanted to become a TV producer. Kasuga is the producer of this travel show! Kasuga, who is a money-grubber, chooses only what is truly valuable and spends money, sometimes boldly, sometimes severely, in order to produce the best possible filming on a limited budget! Watch out for Kasuga's unique way of spending money! Traveling with him will be Haruhi's best friend, Mitsuharu Sato (Dokidoki Camp), and Yoshika Matsuda (Hinatazaka46), who looks up to Sato as a mentor.

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: NTV, Hulu
* Air On : Friday, 18:00~ (JST)
* Started: 2023-09-14
* Status : Ended
* Genres : Variety, Comedy, Travel
* Links  : [NTV](https://www.ntv.co.jp/kasuga-location/), [Hulu](https://www.hulu.jp/kasuga-location-kasuga-producers-travel-program)