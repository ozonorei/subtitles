# Lion no Mirai Monster (ライオンのミライ☆モンスター)

![Poster](fanart.jpg)

# MC
* Tsutomu Sekine.
* Oguri "AKB48" Yui. `(Idol)`
* Yuki Hirako.

# Former MCs

* Okabe "AKB48" Rin. `(Idol)` - `Former`
* Okada "AKB48" Nana. `(Idol)` - `Former`

# Program Information

* Broadcast on: Fuji TV
* Air On: Sunday, 11:15~ (JST)
* Started: 2022-04-03
* Status: Continuing
* Genres: Variety, Documentary
* Links: [Fuji TV](http://www.fujitv.co.jp/miraimonster/)
