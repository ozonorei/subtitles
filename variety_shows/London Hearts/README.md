# London Hearts (ロンドンハーツ)

The show focuses on creating comedy by taking Japanese comedians and television personalities out of their normal broadcast environments, often by ranking them from best to worst in a specific category or in expensive, elaborate candid camera pranks. In the past it featured ordinary members of the public being set up in edgy stunts, often using actors and hidden cameras.

![Show Poster](fanart.jpg)

# MC

* Tamura "London Boots" Atsushi. `(Comedian)`.
* Tamura "London Boots" Ryo. `(Comedian)`.

# Program Information

* Broadcast on: TV Asahi, TVer
* Air On : Tuesday, 11:15~ (JST)
* Started: 1999-04-18
* Status : Continuing
* Genres : Variety, Comedy, Explicit
* Links  : [TV Asahi](https://www.ntv.co.jp/NiziU/), [TVer](https://tver.jp/series/sr2tk8lazj).
* Source : The Silent Library,

# Notes

## 2024-02-26

We so far only have the recording from `2011+`, if someone is able to provide the earlier episodes, i.e `1999-2010`, We will be able to transcribe it as well. Please feel free to contact us on the channels we have on the main page.

Depending on the source, we may switch to TVer as primary source. if there is a need for it.