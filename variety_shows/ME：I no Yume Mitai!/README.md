# ME:I no Yume Mitai! (ME:Iの夢みたい！)

ME:I is a group born from the survival audition program "PRODUCE 101 JAPAN THE GIRLS". They will debut with the single "MIRAI" on April 17th. The program will feature a reward project for the members to make their wishes come true, each member's debut pledge, and will also feature recordings, music video shoots, and more.

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: Lemino
* Air On : Thursday, 21:00 ~ (JST)
* Started: 2024-03-07
* Status : Ended
* Genres : Variety
* Links  : [Lemino](https://lemino.docomo.ne.jp/ft/0000026/).

# Notes

## 2024-04-12

The show ended with episode number 06.