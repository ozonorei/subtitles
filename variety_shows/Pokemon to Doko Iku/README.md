# Pokemon to Doko Iku!? (ポケモンとどこいく！？)

A weekly show about everything related to Pokémon! This show is the successor to Pokemon no Uchi Atsumaru?

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: TV Tokyo, TVer
* Air On: Sunday, 08:000~ (JST)
* Started: 2022-04-03
* Status: Continuing
* Genres: Variety, Games
* Links: [TV Tokyo](https://www.tv-tokyo.co.jp/anime/pokedoko/), [TVer](https://tver.jp/series/srhmllxvnh).

# Notes

## 2024-02-19

We are only doing idols related episodes.
