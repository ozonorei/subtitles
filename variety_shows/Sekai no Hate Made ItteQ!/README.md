# Sekai no Hate Made ItteQ! (世界の果てまでイッテQ!)

Sekai no Hate Made ItteQ! is also known as "Let's Go to the End of the World", is about Adventures to unknown, uncharted territories, amazing activities that the cast members put their bodies on the line to try, and discovering the culture of unknown countries that are not known in Japan... This is an adventure variety show that is full of thrills and excitement for children and adults alike!.

![Show fanart](fanart.jpg)

# Subtitles sources

The subtitles are generated from the theSilentLibrary for older content, otherwise new episodes with `[TVer]` are from TVer. which we started using as source from `2024-08-12`.

# Program Information

* Broadcast on: TBS, TVer, U-NEXT
* Air On: Sunday, 18:58~ - 19:58~ (JST)
* Started: 2007-07-30
* Status: Continuing
* Genres: Action, Thriller, Comedy
* Links: [NTV](https://www.ntv.co.jp/q/), [TVer](https://tver.jp/series/sr9gfdf2ex), [Hulu](https://www.hulu.jp/the-quest-2007).


# Notes

### 2024-08-14

You can find subtitles for old content from The silent library in their folder. Future content in Subs directory is based on TVer releases and follow the standard
`YYMMDD series - title` format.

Disclaimer, we know some content has incorrect date and/or incorrect broadcast date in the TSL content, However i chose to stick with whatever they have listed, which 
alot of poeple most likely will have as well. 


