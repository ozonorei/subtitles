# Task have Fun Diary

"Task have Fun Diary" is a new show by the three-member idol unit "Task have Fun" (commonly known as Task), which has been together for seven years. The members take on various tasks and record their growth like a diary. From the overwhelming live performances that captivate fans, to off-shots of the members that you don't usually get to see, to scenes of them working hard in variety shows.

The comedian Shinomiya "Ojin Ozbourne" Akira will join as Task's show MC and will write a new page together with Task. What kind of diary will they create?

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: TV-Asahi (Douga)
* Air On : Friday, 17:00 ~ JST
* Started: 2023-04-11
* Status : Continuing
* Genres : Variety, Comedy.
* Links  : [Douga](https://douga.tv-asahi.co.jp/program/39579-39578).