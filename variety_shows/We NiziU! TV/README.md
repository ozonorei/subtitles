# We NiziU! TV

NiziU! first variety series. NiziU members are assigned "loose" missions and have to clear them.

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: NTV, Hulu
* Air On : Thursday, Undetermined time.
* Started: 2020-12-06
* Status : Continuing
* Genres : Variety, Comedy, Music, Food, Travel
* Links  : [NTV](https://www.ntv.co.jp/NiziU/), [Hulu](https://www.hulu.jp/we-niziu-tv).