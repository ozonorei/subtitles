# Yobidashi Teacher Tanaka (呼び出し先生タナカ)

A fusion of “study” and “laughter”! Comedy education “simultaneous test” variety show opens! Various guests from various genres such as actors, artists, athletes, idols, cultural figures, comedians, etc. are called as students by Mr. Tanaka and are made to take on a "simultaneous test" related to the school! We have tests not only on the five subjects of Japanese, social studies, mathematics, science, and English, but also on all kinds of subjects that all citizens must study at school! Shame on you if you don't know! “Proverbs & idioms”, “jumping box” and “reverse rise” that seem impossible. 

A variety of academic and practical skills tests will be held all at once, ranging from making "mackerel miso stew" and "kakiage bowl" that will test your common sense in everyday life to "drawing" that will test your sense of style! OfFusion of "study" and "laughter"! A comedy educational "Simultaneous Test" variety show opens! Various guests from various genres such as actors, artists, athletes, idols, cultural figures, and comedians are summoned by Mr. Tanaka as students and asked to take "simultaneous tests" related to school!

![Poster](fanart.jpg)

# MC

* Tanaka "UnGiRLS" Takushi. (Comedian)
 
# Program Information

* Broadcast on: Fuji TV, TVer
* Air On: Monday, 20:00~ (JST)
* Started: 2022-04-24
* Status: Continuing
* Genres: Educational, Variety, Comedy
* Sources: 240129 and onwards are from TVer, earlier episodes are from `jptv.club`.
* Links: [Fuji TV](https://www.fujitv.co.jp/yobidashisensei-tanaka/index.html), [TVer](hhttps://tver.jp/series/sr11ylksh6).