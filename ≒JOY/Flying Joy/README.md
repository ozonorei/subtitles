# Flying Joy (フライングジョイ)

The idol group ≒JOY (Nearly Equal Joy), produced by Sashihara Rino, was unveiled on March 29, 2022, and is now preparing for their long-awaited major debut on January 17, 2024. This is the first step for the girls to "spread their wings" to the world, and before their debut, they will "fling" their charms to the world earlier than anywhere else. The program will feature Minamikawa, a senior comedian in the entertainment world, as the MC, and will delve deeply into the unknown aspects of the members and their personalities through standard variety comedy. The content of the program is filled with the fresh charm of the girls as they are now.

The show will mainly be consist of three parts:

* "Flying Self-Introduction," in which the participants will introduce themselves with a lot of information that has never been shown before.
* "Flying Question," in which they will get to know their personal side.
* "Situation Confession Contest," in which they will confess their feelings according to a theme drawn by lottery. 

At first, the members were a bit nervous because they were not used to being on a variety show, but Minamikawa's relentless criticisms helped them to relax, and in the latter half of the session, the members even teased Minamikawa to get him going. In the latter half of the recording, the members even teased Minamikawa to get him going. If they can't complete it, they will have to face the punishment that only a variety show can offer...?

![Cast](fanart.jpg)

# MCs

* Minamikawa (comedian)

# Airing Status

* Channel: Hulu
* Air On: Every? Wednesday, 19:00~ (JST)
* Started: 2024-01-10
* Status: Continuing
