# ≠ME Senpai, Motto Oshiete Kudasai!! (≠ME　先輩、も〜っと教えてください！！)

The main variety program of the idol group ≠ME, produced by Sashihara Rino (ex AKB48, HKT48) to improve their skills as idols, they invite their seniors to this show to teach them about the entertainment world.

![Show Poster](fanart.jpg)

# Program Information

* Broadcast on: Hulu, Sky PerfecTV!, Fuji U-NEXT
* Air On: Friday, 23:00~ (JST)
* Started: 2021-04-16
* Status: Continuing
* Genres: Variety, Comedy, Talk Show
* Links: [Hulu](https://www.hulu.jp/not-equal-me-please-tell-me-senpai), [Fuji U-NEXT](https://otn.fujitv.co.jp/not-equal-me/), [Sky PerfecTV!](https://streaming.skyperfectv.co.jp/tv-799816).